<?php
	session_start();
	require(dirname(__FILE__). '/global_conn.php');
	require (dirname(__FILE__). '/jpgraph/jpgraph.php');
	require (dirname(__FILE__). '/jpgraph/jpgraph_line.php');
?>
<?php 
header("Expires: " . gmdate("D, d M Y H:i:s", time() + (0*60)) . " GMT"); 
ob_start();
ob_clean(); 
?>
<?php 

$id=!empty($_REQUEST["id"])?$_REQUEST["id"]:"";
if ($_SESSION['id']!="" && $_SESSION['id']!=$id && $id!="505"){
	$id=$_SESSION['id'];
} 
if ($id==""){
  $id=$_SESSION['id'];
} 

$ddate=!empty($_REQUEST["ddate"])?$_REQUEST["ddate"]:"";
$fdate=!empty($_REQUEST["fdate"])?$_REQUEST["fdate"]:"";
 $_clics = !empty($_REQUEST["clics"])?$_REQUEST["clics"]:0;
 $_inscriptions = !empty($_REQUEST["inscriptions"])?$_REQUEST["inscriptions"]:0;
 $_hits = !empty($_REQUEST["hits"])?$_REQUEST["hits"]:0;
 $_abonne = !empty($_REQUEST["abonne"])?$_REQUEST["abonne"]:0;
 if(!empty($ddate) && !empty($fdate)){
	$date1 = explode("/",$ddate);
	$date2 = explode("/",$fdate);
	$date2=mktime(0,0,0,$date2[1],$date2[0],$date2[2]);
	$date1=mktime(0,0,0,$date1[1],$date1[0],$date1[2]);
	$d=$date2 - $date1;
	$xdatediff = round((int)($date2 - $date1)/(3600*24));
	//$wcount = $xdatediff;
 }else{
	 //$wcount=0;
 }
 
if ($_inscriptions==1){

  $Title=_t("Inscriptions ces")." ".$xdatediff." "._t("derniers jours / du")." ".$ddate." "._t("et")." ".$fdate;
}else{
	if ($_abonne ==1){
	$Title=_t("Evolution de vos Abonnés ces")." ".$xdatediff." "._t("derniers jours / du")." ".$ddate." "._t("et")." ".$fdate;
	}else{
		$Title=_t("Visiteurs Unique ces")." ".$xdatediff." "._t("derniers jours / du")." ".$ddate." "._t("et")." ".$fdate;
	} 
} 

///**** Begining
//dump($xdatediff,false);
$rs = query("CALL p_af_eric_select_ca_new(?,?)",array(
			array('value'=>$id,"type"=>PDO::PARAM_STR,"length"=>255),
			array('value'=>$xdatediff+30,"type"=>PDO::PARAM_STR,"length"=>255)
		),false);
//dump($rs,true);
// Setup the graph
$graph = new Graph(520,230);
if(!empty($rs)){
	//$graph->SetScale("textlin");
	$graph->SetScale("int");
	$graph->yaxis->scale->SetAutoMin(0);
}else{
	$graph->SetScale('int',0,1,0,1);
}

$graph->img->SetAntiAliasing(false);

$graph->title->Set($Title);
$graph->title->SetColor("#000000");
// $graph->title->SetFont(FF_ARIAL,12); 
$graph->title->SetFont(FF_ARIAL,FS_NORMAL,8);
$graph->title->SetPos('left');
$graph->SetBox(false);
$graph->img->SetImgFormat("png");
// Setup X-scale
//$graph->xaxis->SetPos('min');
//$graph->xaxis->SetTextLabelInterval(1);
//$graph->xaxis->SetTextTickInterval(5,-1);
$graph->xaxis->SetLabelAngle(90);
$graph->ygrid->SetLineStyle("dotted");
//$graph->xaxis->scale->ticks->SetSize(8,3);
$graph->ygrid->SetColor('#464637');
		
 $clics =  array();
 $ins =  array();
 $ximp = array();
 $xab =  array();
 
 if(!empty($rs)){
	 foreach($rs as $item){
		if ($_clics == 1){
			//array_push($clics, $item["nb_hit"]);
			array_unshift($clics, $item["nb_hit"]);
		}	
		
		if  ($_inscriptions == 1){
			//array_push($ins, $item["nb_inscription"]);
			array_unshift($ins, $item["nb_inscription"]);
		}

		if  ($_hits == 1){
			//array_push($ximp, $item["nb_impression"]);
			array_unshift($ximp, $item["nb_impression"]);
		}

		if  ($_abonne == 1){
			//array_push($xab, $item["nb_abonne"]);
			array_unshift($xab, $item["nb_abonne"]);
		}
	 }
 }
 //var_dump ($rs);
 //var_dump($xab); exit();
if(!empty($clics)){
	// scale ticks
	if(count($clics) >= 365)
			$graph->xaxis->SetTextTickInterval(25,-1);
        elseif(count($clics) >= 150)
            $graph->xaxis->SetTextTickInterval(20,-1);
        elseif(count($clics) >= 120)
            $graph->xaxis->SetTextTickInterval(15,-1);
        elseif(count($clics) >= 90)
			$graph->xaxis->SetTextTickInterval(10,-1);
        elseif(count($clics) >= 60)
           $graph->xaxis->SetTextTickInterval(7,-1);
        elseif(count($clics) >= 30)
            $graph->xaxis->SetTextTickInterval(5,-1);
	//Create the clics line
	if(count($clics) <= 1){
		array_unshift($clics, 0);
	}
	$p1 = new LinePlot($clics);
	$graph->Add($p1);
	$p1->SetWeight(2); 
	$p1->SetColor("#8000a0");
	$legend = _t("Visiteurs Uniques");
	$p1->SetLegend($legend);
	$p1->SetStyle("solid"); 
}
if(!empty($ins)){
	// scale ticks
	$c_ins = count($ins);
	if(count($ins) >= 365)
			$graph->xaxis->SetTextTickInterval(25,-1);
        elseif(count($ins) >= 150)
            $graph->xaxis->SetTextTickInterval(20,-1);
        elseif(count($ins) >= 120)
            $graph->xaxis->SetTextTickInterval(15,-1);
        elseif(count($ins) >= 90)
			$graph->xaxis->SetTextTickInterval(10,-1);
        elseif(count($ins) >= 60)
           $graph->xaxis->SetTextTickInterval(7,-1);
        elseif(count($ins) >= 30)
            $graph->xaxis->SetTextTickInterval(5,-1);
	//Create the ins line
	if(count($ins) <= 1){
		array_unshift($ins, 0);
	}
	$p2 = new LinePlot($ins);
	$graph->Add($p2);
	$p2->SetWeight(2); 
	$p2->SetColor("#8000a0");
	$legend = _t("Insciptions");
	$p2->SetLegend($legend);
	$p2->SetStyle("solid"); 
	
	$p2->value->SetFormat('%d'); 
	$p2->value->SetMargin(2);
	$p2->value->SetAlign('left'); 
	$p2->value->M_Show(true,($c_ins),null);
}
if(!empty($ximp)){
	// scale ticks
	$c_ximp = count($ximp);
	if(count($ximp) >= 365)
			$graph->xaxis->SetTextTickInterval(25,-1);
        elseif(count($ximp) >= 150)
            $graph->xaxis->SetTextTickInterval(20,-1);
        elseif(count($ximp) >= 120)
            $graph->xaxis->SetTextTickInterval(15,-1);
        elseif(count($ximp) >= 90)
			$graph->xaxis->SetTextTickInterval(10,-1);
        elseif(count($ximp) >= 60)
           $graph->xaxis->SetTextTickInterval(7,-1);
        elseif(count($ximp) >= 30)
            $graph->xaxis->SetTextTickInterval(5,-1);
	//Create the ximp line
	if(count($ximp) <= 1){
		array_unshift($ximp, 0);
	}
	$p3 = new LinePlot($ximp);
	$graph->Add($p3);
	$p3->SetWeight(2); 
	$p3->SetColor("#999999");
	$legend = _t("Impressions");
	$p3->SetLegend($legend);
	$p3->SetStyle("solid"); 
	
	$p3->value->SetFormat('%d'); 
	$p3->value->SetMargin(2);
	$p3->value->SetAlign('left'); 
	$p3->value->M_Show(true,($c_ximp),null);
}
if(!empty($xab)){
	// scale ticks
	$c_xab = count($xab);
	if(count($xab) >= 365)
			$graph->xaxis->SetTextTickInterval(25,-1);
        elseif(count($xab) >= 150)
            $graph->xaxis->SetTextTickInterval(20,-1);
        elseif(count($xab) >= 120)
            $graph->xaxis->SetTextTickInterval(15,-1);
        elseif(count($xab) >= 90)
			$graph->xaxis->SetTextTickInterval(10,-1);
        elseif(count($xab) >= 60)
           $graph->xaxis->SetTextTickInterval(7,-1);
        elseif(count($xab) >= 30)
            $graph->xaxis->SetTextTickInterval(5,-1);
	//Create the xab line
	if(count($xab) <= 1){
		array_unshift($xab, 0);
	}
	$p4 = new LinePlot($xab);
	$graph->Add($p4);
	$p4->SetWeight(2); 
	$p4->SetColor("#8000a0");
	$legend = _t("Abonnés");	
	$p4->SetLegend($legend);
	$p4->SetStyle("solid"); 
	
	//$p4->value->Show();
	//$p4->value->M_Show(true,null,983); // hien thi max
	$p4->value->SetFormat('%d'); 
	$p4->value->SetMargin(2);
	$p4->value->SetAlign('left'); 
	$p4->value->M_Show(true,($c_xab),null); 
	
}
// $graph->legend->SetFrameWeight(0);
$graph->legend->Pos( 0,0,"right","top");
$graph->legend->SetLayout(LEGEND_VERT);
$graph->legend->SetLineWeight(10);
//dump($xab,false);
// Output line
$graph->Stroke(); 


?>


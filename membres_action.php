<?php
if(!isset($_SESSION)){
	session_start();
}
require_once(dirname(__FILE__). '/global_conn.php');  
// Option $explicit;
// ob_start();
//----------------------------
function blocage_temporaire()
{
  // extract($GLOBALS);
//BLOQUAGE temporaire 
  if ($_SESSION['je_debugg']!=1)
  {

//Err.Raise 6,"erreur_volontaire","verif si page demandé 01/03/2006 12:20" 
    print _t("Désolé, le site est indisponible<br> Veuillez revenir plus tard<br><br>merci de votre compréhension");
    exit();

  } 
} 
//----------------------------
//call blocage_temporaire()

$login=!empty($_REQUEST["login"]) ? $_REQUEST["login"] : "";
$pass=!empty($_REQUEST["pass"]) ? $_REQUEST["pass"] : "";
$geo = geo();
$ip=$geo->ip;
$id_operateur=!empty($_SESSION['id_operateur'])?$_SESSION['id_operateur']:"";
$id_ord=!empty($_SESSION['id_ord'])?$_SESSION['id_ord']:"";
//id_ord = request.cookies("id_ord")("id_ord")
//----------------------------------------------------------------------------------------------------------	
function identification_base($login,$pass,$id_operateur,$ip,$id_ord){
  // extract($GLOBALS);
	//CALL p_af_identification('login','pass',11,'213.144.202.61',123,@id_affilie,@retour_conn,@id_af_connexion,@SWP_Ret_Value);
	//$ip = "91.121.13.96"; //test
	
	
	$rs = query("CALL p_af_identification(?,?,?,?,?,@id_affilie,@retour_conn,@id_af_connexion,@SWP_Ret_Value)",array(
		array('value'=>$login, "type"=>PDO::PARAM_STR, "length"=>25),
		array('value'=>$pass, "type"=>PDO::PARAM_STR, "length"=>25),
		array('value'=>$id_operateur, "type"=>PDO::PARAM_INT),
		array('value'=>$ip, "type"=>PDO::PARAM_STR, "length"=>15),
		array('value'=>$id_ord, "type"=>PDO::PARAM_INT)		
	),false,'@id_affilie,@retour_conn,@id_af_connexion,@SWP_Ret_Value');
	//dump($rs);
	$SWP_Ret_Value = $rs['@SWP_Ret_Value'];
	$id_affilie=null;
	$retour_conn=null;
	$id_af_connexion=null;
	if($SWP_Ret_Value == 1){
		$id_affilie=$rs["@id_affilie"];
		$retour_conn=$rs["@retour_conn"];
		$id_af_connexion=$rs["@id_af_connexion"];
		
		if ($id_affilie==23920){
			$id_affilie=0;
		  } 
		  
		  
		$_SESSION['id_affilie']=$id_affilie;
		$_SESSION['id']=$id_affilie;
		$_SESSION['id_af_connexion']=$id_af_connexion;
		$_SESSION['login']=$login;
		$_SESSION['msg_err']="";
		$_SESSION['demo']="";
		header("Location: /indexstats.php");
	}else{
		$_SESSION['demo']=1;
		$_SESSION['msg_err']="<b>"._t("ERREUR !<br>identification incorrecte.")."</b>";
		$_REQUEST['login'] = $login;
		require(dirname(__FILE__)."/membres.php");
		//header("Location: /membres.php");
		exit();
	}

}

//-----------------------------------------------------------------------------

if ($login!="" && $pass!="" ){
	identification_base($login,$pass,$id_operateur,$ip,$id_ord);
	// '".$login."','".$pass."','".$id_operateur."','".$ip."','".$id_ord."'
}else{ 
	$_SESSION['msg_err']= _t("Veuillez Preciser le login et le password");
	//require("/membres.php");
	require(dirname(__FILE__)."/membres.php");
	//header("Location: /membres.php");
}
?>	


<?php
require dirname(__FILE__).'/../global_conn.php';

function je_divise($ca, $par_ca){	/*il y a vait des erreur de division par 0 09/03/2006 15:59*/
	if($par_ca >0){
		$je_divise = $ca/$par_ca;
	}else{
		$je_divise = $ca;
	}
	return $je_divise;
}

$jour = date('d');
$mois = date('m');
$annee = date('Y');

function convert($time){
	return date('d/m/Y',$time);
}

$fdate = $jour."/".$mois."/".$annee;
//$fdate = date('d/m/Y');

$recul = !empty($_REQUEST['recul']) ? $_REQUEST['recul'] : "";

if($recul == "mois"){
	$ddate = "01/".$mois."/".$annee;
	$recul = 15;
}else{
	if(empty($recul)){
		$recul = "30";
		$ddate = date('d',time()-30*86400);
	}else{
		$ddate = convert(date('d',time()-$recul*86400));
	}
}

$pays = "france";
$webmaster 	= !empty($_SESSION['id']) ? $_SESSION['id'] : ""; 
$login 		= !empty($_SESSION['login']) ? $_SESSION['login'] : "";

//if session("demo") = "1" then 
//->if($_SESSION['demo']=="1"){}

// $id = "348";
$id = "22574";
/*else{
	$id = $_SESSION['id'];
	if(empty($id)){
		header("location:/membre.php")
	}	
}*/
//**** Ratio

$rs = query("CALL p_af_eric_select_ratio(?)",array(
			array('value'=>$id,"type"=>PDO::PARAM_STR,"length"=>255)
		),false);
// dump($rs);
if(!empty($rs)){
	$retrib_0_1500 = $rs[0]['retrib_0_1500'];
	$retrib_1500_3000 = $rs[0]['retrib_1500_3000'];
	$retrib_3000_5000 = $rs[0]['retrib_3000_5000'];
	$retrib_5000_10000 = $rs[0]['retrib_5000_10000'];
	$retrib_10000_20000 = $rs[0]['retrib_10000_20000'];
	$retrib_sup_20000 = $rs[0]['retrib_sup_20000'];
}
//**** Totaux 

$sum_inscription = 0;
$sum_hit		 = 0;
$sum_paiement 	 = 0;
$sum_montant_paiement = 0;
$sum_paiement_auto = 0;
$sum_montant_paiement_auto = 0;
$sum_abonne = 0;

//**** Ancienneté du trafic // pour le previsionnel

// $sql = "CALL p_af_eric_anciennete ('{$id}')";


$rs = query("CALL p_af_eric_anciennete(?)",array(
			array('value'=>$id,"type"=>PDO::PARAM_STR,"length"=>255)
		),false);

// dump($rs);
if(!empty($rs)){
	foreach ($rs as $key => $value) {
		$x[] = $value;
	}	
	$anciennete = $x[0]['qte']; //Anciennetée du trafic // utile si le client à une anciennetée inférieure à 30 jours
	$qte_direct = $x[0]['qte_direct']; //Qte de paiements direct
}
//**** Recup du reste des éléments


$rs2 = query("CALL p_af_eric_select_sum(?,?,?)",array(
			array('value'=>$id,"type"=>PDO::PARAM_STR,"length"=>255),
			array('value'=>$ddate,"type"=>PDO::PARAM_STR,"length"=>255),
			array('value'=>$fdate,"type"=>PDO::PARAM_STR,"length"=>255)
		),false);
// dump($rs);
if(!empty($rs)){
	$analyse = (($rs[0]['sum_montant_paiement']+$rs[0]['sum_montant_paiement_auto'])/1.196)/$recul;
	if($analyse > 50){
		$ratio = $retrib_3000_5000;
	}elseif($analyse>100){
		$ratio = $retrib_3000_5000;
	}elseif($analyse>160){
		$ratio = $retrib_5000_10000;
	}elseif($analyse>330){
		$ratio = $retrib_10000_20000;
	}elseif($analyse>650){
		$ratio = $retrib_sup_20000;
	}else{
		$ratio = $retrib_0_1500;
	}

	$sum_hit = $rs[0]['sum_hit'];
	if(is_null($sum_hit)||($sum_hit<2)){
		$sum_hit = 1;
	}

	$sum_inscription = $rs[0]['sum_inscription'];
	if(is_null($sum_inscription)||($sum_inscription<2)){
		$sum_inscription = 1;
	}

	$ca_ht = ((($rs[0]["sum_montant_paiement"]+$rs[0]["sum_montant_paiement_auto"])/1.196)*0.9);
	if(is_null($ca_ht)){
		$ca_ht = 0.1;
	}

	//reversement_ht=(((rs("sum_montant_paiement")+rs("sum_montant_paiement_auto")*0.9)/1.196)/100)*ratio
	$sum_paiement = $rs[0]['sum_paiement'];
	if(is_null($sum_paiement)){
		$sum_paiement = 0.1;
	}

	$sum_montant_paiement = ((($rs[0]["sum_montant_paiement"]/1.196)*0.9)/100)*$ratio; /**** Pour les 10% du paiement éléctronique ****/
	if(is_null($sum_montant_paiement)){
		$sum_montant_paiement = 0.1;
	}

	$sum_paiement_auto = $rs[0]["sum_paiement_auto"];
	if(is_null($sum_paiement_auto)){
		$sum_paiement_auto = 0;
	}

	$sum_montant_paiement_auto = ((($rs[0]["sum_montant_paiement_auto"]/1.196)*0.9)/100)*$ratio;
	if(is_null($sum_montant_paiement_auto)){
		$sum_montant_paiement_auto = 0;
	}

	$sum_abonne = $rs[0]['sum_abonne'];
	if(is_null($sum_abonne)){
		$sum_abonne = 1;
	}

	// Paiements Directs
	$sum_paiement_direct = $rs[0]['sum_paiement_direct'];
	if(is_null($sum_paiement_direct)){
		$sum_paiement_direct = 1;
	}

	$sum_montant_paiement_direct = $rs[0]['sum_montant_paiement_direct'];
	if(is_null($sum_montant_paiement_direct)){
		$sum_montant_paiement_direct = 1;
	}

	$reversement_ht = $sum_montant_paiement + $sum_montant_paiement_auto;

	$tb_01_clics = number_format($sum_hit);
	$tb_01_inscription = number_format($sum_inscription);

	if($ca_ht == 0.1){
		$tb_01_caht = number_format(0,2,".","");
	}else{
		$tb_01_caht = number_format($ca_ht,2,".",",");
	}

	$tb_01_ratio = $ratio;

	if($reversement_ht == 0.1){
		$tb_01_reversement = number_format(0,2,".","");
	}else{
		$tb_01_reversement = number_format($reversement_ht,2,".",",");
	}

	$tb_02_qte_nouveaux = number_format($sum_paiement);

	if($sum_montant_paiement == 0.1){
		$tb_02_ca_nouveaux = number_format(0,2,".","");
	}else{
		$tb_02_ca_nouveaux  = number_format($sum_montant_paiement,2,".",",");
	}

	$tb_02_qte_rebills = number_format($sum_paiement_auto);
	$tb_02_ca_rebills = number_format($sum_montant_paiement_auto,2,".",",");
	$tb_02_qte_transacs = number_format($sum_paiement + $sum_paiement_auto);

	$tb_02_qte_direct = number_format($sum_paiement_direct);
	$tb_02_ca_direct = number_format($sum_montant_paiement_direct);

	$tb_03_par_nouveaux = number_format(je_divise($sum_montant_paiement,$sum_paiement),2,".",",");
	if(($sum_montant_paiement_auto <2)||($sum_paiement_auto<2)){
		$tb_03_par_rebills = 0;
	}else{
		$tb_03_par_rebills = number_format(je_divise($sum_montant_paiement_auto,$sum_paiement_auto),2,".",",");
	}

	$tb_03_par_transacs = number_format(je_divise($reversement_ht, ($sum_paiement_auto + $sum_paiement)),2,".",",");
	$tb_03_par_visiteur = number_format(je_divise($reversement_ht, $sum_hit),3,".",",");
	$tb_03_par_inscription = number_format(je_divise($reversement_ht, $sum_inscription),3,".",",");

	$tb_06_05 = 0;		//Set default value to use at line 211

	$tb_06_01 = "12 730";
	$tb_06_02 = 6.62;
	$tb_06_03 = 15.3;
    $tb_06_04 = 3.12;
    $tb_06_05 = ($tb_06_03*$tb_06_04)+$tb_06_05;
    $tb_06_06 = ($tb_06_03*$tb_06_04)+$tb_06_05;
    $tb_06_07 = 15.15;
    $tb_06_08 = $tb_06_06*$tb_06_07;
    $tb_06_09 = $tb_06_08/1000;
    $tb_06_10 = $ratio;
    $tb_06_11 = number_format(($tb_06_09/100)*$ratio,2,".",",");
    $tb_06_12 = number_format(($tb_06_08/100)*12800,3,".",",");
    $tb_06_13 = number_format(($tb_06_08/100)*$ratio,2,".",",");
    $tb_06_14 = "-";
    $tb_06_15 = "-";
    $tb_06_16 = "-";
    $tb_06_17 = "-";

     $tb_06_01_a = je_divise($tb_01_clics,($tb_01_inscription/1000));/* Visiteurs Unique    (clics / (inscriptions/1000)) */
     $tb_06_01_b=" ( ".$tb_01_clics."/ (".$tb_01_inscription."/1000) \\ visiteurs/(inscriptions/1000)";
     $tb_06_02_a = je_divise($tb_02_qte_direct , ($tb_01_inscription/1000));/*Abonnements directs / 1000 Inscriptions*/
     $tb_06_03_a = (($tb_06_02_a)/$tb_06_02)*$tb_06_03; 	//Nouveaux abonnements à 12 mois
     $tb_06_04_a = $tb_06_04;                     	   		//Rebill moyen à 12 mois
     $tb_06_05_a = $tb_06_03_a*$tb_06_04;                  	// Qte de rebill à 12 mois
     $tb_06_06_a = ($tb_06_03_a) + ($tb_06_05_a);          	//Qte de transactions à 12 mois
     $tb_06_07_a = je_divise($ca_ht,($sum_paiement+$sum_paiement_auto));     //CA HT moyen par transactions 
     $tb_06_08_a = ($tb_06_06_a)*($tb_06_07_a);              //Total de CA HT sur 12 mois   
     $tb_06_09_a = $tb_06_08_a/1000;                        //CA HT par inscription
     $tb_06_10_a = $ratio;                                  //Ratio
     $tb_06_11_a = ($tb_06_09_a/100)*$ratio;                //Votre CA HT par inscription  
     $tb_06_12_a = je_divise((($tb_06_08_a/100)*$ratio),$tb_06_01_a);          // Votre CA HT par visiteur unique    
     $tb_06_13_a = ($tb_06_08_a/100)*$ratio;                //Total de votre CA HT sur 12 mois    
     $tb_06_14_a = $tb_01_clics/30;                       	//Qte de Visiteurs / jours    
     $tb_06_15_a = $tb_01_inscription/30;                 	//Qte de Inscriptions / jours    
     $tb_06_16_a = $tb_06_15_a*$tb_06_11_a;                	//CA HT moyen / jours à 12 mois    
     $tb_06_17_a = $tb_06_16_a*30;                        	//CA HT moyen / Mois à 12mois

     $tb_06_05 = number_format($tb_06_05,2,".",",");
     $tb_06_06 = number_format($tb_06_06,2,".",",");
     $tb_06_08 = number_format($tb_06_08,2,".",",");
     $tb_06_09 = number_format($tb_06_09,2,".",",");
     $tb_06_01_a = number_format($tb_06_01_a);
     $tb_06_02_a = number_format($tb_06_02_a,2,".",",");
     $tb_06_03_a = number_format($tb_06_03_a,2,".",",");
     $tb_06_05_a = number_format($tb_06_05_a,2,".",",");
     $tb_06_06_a = number_format($tb_06_06_a,2,".",",");
     $tb_06_07_a = number_format($tb_06_07_a,2,".",",");
     $tb_06_08_a = number_format($tb_06_08_a,2,".",",");
     $tb_06_09_a = number_format($tb_06_09_a,2,".",",");
     $tb_06_11_a = number_format($tb_06_11_a,2,".",",");
     $tb_06_12_a = number_format($tb_06_12_a,3,".",",");
     $tb_06_13_a = number_format($tb_06_13_a,2,".",",");
     $tb_06_15_a = number_format($tb_06_15_a,2,".",",");
     $tb_06_16_a = number_format($tb_06_16_a,2,".",",");
     $tb_06_17_a = number_format($tb_06_17_a,2,".",",");
}
?>
<html>
	<head>
		<title><?php echo _t("Rencontre Affiliation"); ?></title>
		<META http-equiv=Page-Enter content=blendTrans(Duration=1.0)>
		<STYLE TYPE="text/css"> A {color:#ffffff;} A:hover {color:#cccccc} </STYLE>

			<SCRIPT LANGUAGE="javascript">
			<!--
				function ShowTooltip(fArg)
				{
					var tooltipOBJ = eval("document.all['tt" + fArg + "']");
					var tooltipOffsetTop = tooltipOBJ.scrollHeight + 15;
					var testTop = (document.body.scrollTop + event.clientY) - tooltipOffsetTop;
					var testLeft = event.clientX - 250;
					var tooltipAbsLft = (testLeft < 0) ? 160 : testLeft;
					var tooltipAbsTop = (testTop < document.body.scrollTop) ? document.body.scrollTop + 10 : testTop;
					tooltipOBJ.style.posLeft = tooltipAbsLft;
					tooltipOBJ.style.posTop = tooltipAbsTop;
					tooltipOBJ.style.visibility = "visible";
				}
				function HideTooltip(fArg)
				{
					var tooltipOBJ = eval("document.all['tt" + fArg + "']");
					tooltipOBJ.style.visibility = "hidden";
				}
			//-->
			</SCRIPT>
		<link href="affiliation.css" rel="stylesheet" type="text/css">
		<LINK REL="stylesheet" HREF="overlib.css" TYPE="text/css">
	</head>
	<body>
	  	<DIV ID="overDiv" STYLE="position:absolute; visibility:hide;"></DIV>
	  	<SCRIPT LANGUAGE="JavaScript" SRC="overlib_fr.js"></SCRIPT>
	  	
	  	<SCRIPT TYPE="" LANGUAGE="JavaScript">
	  	<!--
	  		var width = "250";
	  		var border = "3";
	  		var offsetx = 2;
	  		var offsety = 2;
	  		
	  		var fcolor = "#F4EAFF";
	  		var backcolor = "#8020a0";
	  		var textcolor = "#000000";
	  		var capcolor = "#FFFFFF";
	  		var closecolor = "#C68CFF";
	  		
	  	// -->
	  	</SCRIPT>
	<table width="80%" border="0" cellspacing="0" cellpadding="0" align="center">
	  <tr> 
	    <td class="texte1"><?php if(isset($_SESSION['demo'])&&($_SESSION['demo']=="1")){ ?> 
	      <div align="center"> 
	        <table border="1" cellspacing="0" cellpadding="0" align="center" bordercolor="#CCCCCC" width="510">
	          <tr bgcolor="#8020a0"> 
	            <td background="images/background2.gif" bgcolor="#8020a0"> 
	              <div align="left" class="titre">&nbsp;<b><?php echo _t("Espace Démo"); ?> </b></div>
	            </td>
	          </tr>
	        </table>
	      <table width="100%" border="0" cellspacing="0" cellpadding="0" height="3">
	        <tr>
	          <td></td>
	        </tr>
	      </table><?php }?>
	      </div>
	      <table width="510" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#CCCCCC">
	        <tr> 
	          <td width="131" background="images/background2.gif" bgcolor="#8020a0"> 
	            <div align="center"><?php if(isset($_SESSION['demo'])&&($_SESSION['demo']=="1")){?>
				<a href="#" class="navigation" onMouseOver="ShowTooltip(1);" onMouseOut="HideTooltip(1);" ><?php echo _t("Vos Coordonnées"); ?></a></div>
				<div class=reminderTooltip id=tt1 style="WIDTH: 160px;"><?php echo _t("Indisponible en Espace Demo !"); ?></div>
	              <?php }else{ ?>
				<a href="coordonnees.php" class="navigation"><?php echo _t("Vos Coordonées"); ?></a></div>
				<?php }?>
	          </td>
	          <td width="115"> 
	            <div align="center" class="texte1"><i><?php echo _t("Vos Statistiques"); ?></i></div>
	          </td>
	          <td width="153" background="images/background2.gif" bgcolor="#8020a0"> 
	            <div align="center"><a href="bandeaux" class="navigation"><?php echo _t("Bandeaux &amp; Outils"); ?></a></div>
	          </td>
	          <td width="97" background="images/background2.gif" bgcolor="#8020a0"> 
	            <div align="center">
	              <div align="center"><?php if(isset($_SESSION['demo'])&&($_SESSION['demo']=="1")){ ?>
	                <a href="#" class="navigation" onMouseOver="ShowTooltip(2);" onMouseOut="HideTooltip(2);" ><?php echo _t("Facturation"); ?></a></div>
	              <div class=reminderTooltip id=tt2 style="WIDTH: 160px;"><?php echo _t("Indisponible en Espace Demo !"); ?></div>
	                <?php }else{ ?>
	              		<a href="../appel_facture_list.php" class="navigation"><?php echo _t("Facturation"); ?></a>
	              	<?php }?>
	            </div>
			  </td>
	        </tr>
	      </table>

	      <table width="100%" border="0" cellspacing="0" cellpadding="0" height="3">
	        <tr>
	          <td></td>
	        </tr>
	      </table>
	      <?php 
	      $n = !empty($_REQUEST['n']) ? $_REQUEST['n'] : "30";
	      switch($n){
	      	case "racine": ?>
	      		<table border="1" cellspacing="0" cellpadding="0" align="center" bordercolor="#CCCCCC" width="510">
			        <tr> 
			          <td width="36" background="images/background2.gif" bgcolor="#8020a0"><div align="center" class="navigation"><a href="?recul=150&n=150" onMouseOver="drc('  Du <?php echo date('d/m/Y',time()-150*86400);?> au <?php echo date('d/m/Y');?>','&nbsp;&nbsp;Visuel statistique sur 150 jours'); return true;" onMouseOut="nd(); return true;">150</a></div></td>
			          <td width="37" background="images/background2.gif" bgcolor="#8020a0"><div align="center" class="navigation"><a href="?recul=120&n=120" onMouseOver="drc('  Du <?php echo date('d/m/Y',time()-120*86400);?> au <?php echo date('d/m/Y');?>','&nbsp;&nbsp;Visuel statistique sur 120 jours'); return true;" onMouseOut="nd(); return true;">120</a></div></td>
			          <td width="29" background="images/background2.gif" bgcolor="#8020a0"> <div align="center" class="navigation"><a href="?recul=90&n=90"  onMouseOver="drc('  Du <?php echo date('d/m/Y',time()-90*86400);?> au <?php echo date('d/m/Y');?>','&nbsp;&nbsp;Visuel statistique sur 90 jours'); return true;" onMouseOut="nd(); return true;">90</a></div></td>
			          <td width="31" height="22" background="images/background2.gif" bgcolor="#8020a0"> <div align="center" class="navigation"><a href="?recul=60&n=60"  onMouseOver="drc('  Du <?php echo date('d/m/Y',time()-60*86400);?> au <?php echo date('d/m/Y');?>','&nbsp;&nbsp;Visuel statistique sur 60 jours'); return true;" onMouseOut="nd(); return true;">60</a></div></td>
			          <td width="136" height="22" background="images/background2.gif" bgcolor="#8020a0"> <div align="center" class="navigation"><a href="?recul=30&n=30"  onMouseOver="drc('  Du <?php echo date('d/m/Y',time()-30*86400);?> au <?php echo date('d/m/Y');?>','&nbsp;&nbsp;Visuel statistique sur 30 jours'); return true;" onMouseOut="nd(); return true;">30 
			          <?php echo _t("DerniersJours"); ?></a></div></td>
			          <td width="81" height="22" background="images/background2.gif" bgcolor="#8020a0"> <div align="center" class="navigation"><a href="?recul=365&n=annuelles" onMouseOver="drc('  Du <?php echo date('d/m/Y',time()-365*86400);?> au <?php echo date('d/m/Y');?>','&nbsp;&nbsp;Visuel statistique sur 365 jours'); return true;" onMouseOut="nd(); return true;"><?php echo _t("Annuelles"); ?></a></div></td>
			          <td width="74" height="22" background="images/background2.gif" bgcolor="#8020a0"> <div align="center" class="navigation"><a href="?n=analyses"><?php echo _t("Analyses"); ?></a></div></td>
			          <td width="64" height="22"> 
			          <div align="center" class="texte1"><em><?php echo _t("Du Mois"); ?></em> </div></td>
			        </tr>
			      </table>
	      	<?php	break;
	      	case "analyses": ?>
	      		<table border="1" cellspacing="0" cellpadding="0" align="center" bordercolor="#CCCCCC" width="510">
			        <tr bgcolor="#8020a0"> 
			          <td width="36" background="images/background2.gif" bgcolor="#8020a0"><div align="center" class="navigation"><a href="?recul=150&n=150"  onMouseOver="drc('  Du <?php echo date('d/m/Y',time()-150*86400);?> au <?php echo date('d/m/Y');?>','&nbsp;&nbsp;Visuel statistique sur 150 jours'); return true;" onMouseOut="nd(); return true;">150</a></div></td>
			          <td width="37" background="images/background2.gif" bgcolor="#8020a0"><div align="center" class="navigation"><a href="?recul=120&n=120"  onMouseOver="drc('  Du <?php echo date('d/m/Y',time()-120*86400);?> au <?php echo date('d/m/Y');?>','&nbsp;&nbsp;Visuel statistique sur 120 jours'); return true;" onMouseOut="nd(); return true;">120</a></div></td>
			          <td width="29" background="images/background2.gif" bgcolor="#8020a0"> <div align="center" class="navigation"><a href="?recul=90&n=90"  onMouseOver="drc('  Du <?php echo date('d/m/Y',time()-90*86400);?> au <?php echo date('d/m/Y');?>','&nbsp;&nbsp;Visuel statistique sur 90 jours'); return true;" onMouseOut="nd(); return true;">90</a></div></td>
			          <td width="31" height="22" background="images/background2.gif" bgcolor="#8020a0"> <div align="center" class="navigation"><a href="?recul=60&n=60"  onMouseOver="drc('  Du <?php echo date('d/m/Y',time()-60*86400);?> au <?php echo date('d/m/Y');?>','&nbsp;&nbsp;Visuel statistique sur 60 jours'); return true;" onMouseOut="nd(); return true;">60</a></div></td>
			          <td width="136" height="22" background="images/background2.gif" bgcolor="#8020a0"> <div align="center" class="navigation"><a href="?recul=30&n=30"  onMouseOver="drc('  Du <?php echo date('d/m/Y',time()-30*86400);?> au <?php echo date('d/m/Y');?>','&nbsp;&nbsp;Visuel statistique sur 30 jours'); return true;" onMouseOut="nd(); return true;">30 
			          <?php echo _t("DerniersJours"); ?></a></div></td>
			          <td width="81" height="22" background="images/background2.gif" bgcolor="#8020a0"> <div align="center" class="navigation"><a href="?recul=365&n=annuelles" onMouseOver="drc('  Du <?php echo date('d/m/Y',time()-365*86400);?> au <?php echo date('d/m/Y');?>','&nbsp;&nbsp;Visuel statistique sur 365 jours'); return true;" onMouseOut="nd(); return true;"><?php echo _t("Annuelles"); ?></a></div></td>
			          <td width="74" height="22" bgcolor="#FFFFFF"> 
			          <div align="center" class="navigation"><em><?php echo _t("Analyses"); ?></em></div></td>
			          <td width="64" height="22" background="images/background2.gif" bgcolor="#8020a0"> <div align="center" class="texte1"><a href="?n=mois&recul=mois"><?php echo _t("Du Mois"); ?> </a></div></td>
			        </tr>
			      </table>
	      	<?php	break;
	      	case "annuelles": ?>
	      		<table border="1" cellspacing="0" cellpadding="0" align="center" bordercolor="#CCCCCC" width="510">
			        <tr bgcolor="#8020a0"> 
			          <td width="36" background="images/background2.gif" bgcolor="#8020a0"><div align="center" class="navigation"><a href="?recul=150&n=150"  onMouseOver="drc('  Du <?php echo date('d/m/Y',time()-150*86400);?> au <?php echo date('d/m/Y');?>','&nbsp;&nbsp;Visuel statistique sur 150 jours'); return true;" onMouseOut="nd(); return true;">150</a></div></td>
			          <td width="37" background="images/background2.gif" bgcolor="#8020a0"><div align="center" class="navigation"><a href="?recul=120&n=120"  onMouseOver="drc('  Du <?php echo date('d/m/Y',time()-120*86400);?> au <?php echo date('d/m/Y');?>','&nbsp;&nbsp;Visuel statistique sur 120 jours'); return true;" onMouseOut="nd(); return true;">120</a></div></td>
			          <td width="29" background="images/background2.gif"> <div align="center" class="navigation"><a href="?recul=90&n=90"  onMouseOver="drc('  Du <?php echo date('d/m/Y',time()-90*86400);?> au <?php echo date('d/m/Y');?>','&nbsp;&nbsp;Visuel statistique sur 90 jours'); return true;" onMouseOut="nd(); return true;">90</a></div></td>
			          <td width="31" height="22" background="images/background2.gif"> <div align="center" class="navigation"><a href="?recul=60&n=60"  onMouseOver="drc('  Du <?php echo date('d/m/Y',time()-60*86400);?> au <?php echo date('d/m/Y');?>','&nbsp;&nbsp;Visuel statistique sur 60 jours'); return true;" onMouseOut="nd(); return true;">60</a></div></td>
			          <td width="136" height="22" background="images/background2.gif"> <div align="center" class="navigation"><a href="?recul=30&n=30"  onMouseOver="drc('  Du <?php echo date('d/m/Y',time()-30*86400);?> au <?php echo date('d/m/Y');?>','&nbsp;&nbsp;Visuel statistique sur 30 jours'); return true;" onMouseOut="nd(); return true;">30 
			          <?php echo _t("DerniersJours"); ?></a></div></td>
			          <td width="81" height="22" bgcolor="#FFFFFF"> 
			          <div align="center" class="navigation"><em><?php echo _t("Annuelles"); ?></em></div></td>
			          <td width="74" height="22" background="images/background2.gif"> <div align="center" class="navigation"><a href="?n=analyses"><?php echo _t("Analyses"); ?></a></div></td>
			          <td width="64" height="22" background="images/background2.gif"> <div align="center" class="texte1"><a href="?n=mois&recul=mois"><?php echo _t("Du Mois"); ?> </a></div></td>
			        </tr>
			      </table>
	      	<?php	break;
	      	case "30": ?>
	      		<table border="1" cellspacing="0" cellpadding="0" align="center" bordercolor="#CCCCCC" width="510">
			        <tr bgcolor="#8020a0"> 
			          <td width="36" background="images/background2.gif" bgcolor="#8020a0"><div align="center" class="navigation"><a href="?recul=150&n=150"  onMouseOver="drc('  Du <?php echo date('d/m/Y',time()-150*86400);?> au <?php echo date('d/m/Y');?>','&nbsp;&nbsp;Visuel statistique sur 150 jours'); return true;" onMouseOut="nd(); return true;">150</a></div></td>
			          <td width="37" background="images/background2.gif" bgcolor="#8020a0"><div align="center" class="navigation"><a href="?recul=120&n=120"  onMouseOver="drc('  Du <?php echo date('d/m/Y',time()-120*86400);?> au <?php echo date('d/m/Y');?>','&nbsp;&nbsp;Visuel statistique sur 120 jours'); return true;" onMouseOut="nd(); return true;">120</a></div></td>
			          <td width="29" background="images/background2.gif" > <div align="center" class="navigation"><a href="?recul=90&n=90"  onMouseOver="drc('  Du <?php echo date('d/m/Y',time()-90*86400);?> au <?php echo date('d/m/Y');?>','&nbsp;&nbsp;Visuel statistique sur 90 jours'); return true;" onMouseOut="nd(); return true;">90</a></div></td>
			          <td width="31" height="22" background="images/background2.gif" > <div align="center" class="navigation"><a href="?recul=60&n=60"  onMouseOver="drc('  Du <?php echo date('d/m/Y',time()-60*86400);?> au <?php echo date('d/m/Y');?>','&nbsp;&nbsp;Visuel statistique sur 60 jours'); return true;" onMouseOut="nd(); return true;">60</a></div></td>
			          <td width="136" height="22" bgcolor="#FFFFFF"> 
			          <div align="center" class="navigation"><em>30 <?php echo _t("DerniersJours"); ?></em></div></td>
			          <td width="81" height="22" background="images/background2.gif" > <div align="center" class="navigation"><a href="?recul=365&n=annuelles" onMouseOver="drc('  Du <?php echo date('d/m/Y',time()-365*86400);?> au <?php echo date('d/m/Y');?>','&nbsp;&nbsp;Visuel statistique sur 365 jours'); return true;" onMouseOut="nd(); return true;"><?php echo _t("Annuelles"); ?></a></div></td>
			          <td width="74" height="22" background="images/background2.gif" > <div align="center" class="navigation"><a href="?n=analyses"><?php echo _t("Analyses"); ?></a></div></td>
			          <td width="64" height="22" background="images/background2.gif" > <div align="center" class="texte1"><a href="?n=mois&recul=mois"><?php echo _t("Du Mois"); ?> </a></div></td>
			        </tr>
			      </table>
	      	<?php	break;
	      	case "60": ?>
	      		<table border="1" cellspacing="0" cellpadding="0" align="center" bordercolor="#CCCCCC" width="510">
			        <tr bgcolor="#8020a0"> 
			          <td width="36" bgcolor="#8020a0" background="images/background2.gif" ><div align="center" class="navigation"><a href="?recul=150&n=150"  onMouseOver="drc('  Du <?php echo date('d/m/Y',time()-150*86400);?> au <?php echo date('d/m/Y');?>','&nbsp;&nbsp;Visuel statistique sur 150 jours'); return true;" onMouseOut="nd(); return true;">150</a></div></td>
			          <td width="37" bgcolor="#8020a0" background="images/background2.gif" ><div align="center" class="navigation"><a href="?recul=120&n=120"  onMouseOver="drc('  Du <?php echo date('d/m/Y',time()-120*86400);?> au <?php echo date('d/m/Y');?>','&nbsp;&nbsp;Visuel statistique sur 120 jours'); return true;" onMouseOut="nd(); return true;">120</a></div></td>
			          <td width="29" background="images/background2.gif" > <div align="center" class="navigation"><a href="?recul=90&n=90"  onMouseOver="drc('  Du <?php echo date('d/m/Y',time()-90*86400);?> au <?php echo date('d/m/Y');?>','&nbsp;&nbsp;Visuel statistique sur 90 jours'); return true;" onMouseOut="nd(); return true;">90</a></div></td>
			          <td width="31" height="22" bgcolor="#FFFFFF"> 
			          <div align="center" class="navigation"><em>60</em></div></td>
			          <td width="136" height="22" background="images/background2.gif" > <div align="center" class="navigation"><a href="?recul=30&n=30"  onMouseOver="drc('  Du <?php echo date('d/m/Y',time()-30*86400);?> au <?php echo date('d/m/Y');?>','&nbsp;&nbsp;Visuel statistique sur 30 jours'); return true;" onMouseOut="nd(); return true;">30 
			              <?php echo _t("DerniersJours"); ?></a></div></td>
			          <td width="81" height="22" background="images/background2.gif" > <div align="center" class="navigation"><a href="?recul=365&n=annuelles" onMouseOver="drc('  Du <?php echo date('d/m/Y',time()-365*86400);?> au <?php echo date('d/m/Y');?>','&nbsp;&nbsp;Visuel statistique sur 365 jours'); return true;" onMouseOut="nd(); return true;"><?php echo _t("Annuelles"); ?></a></div></td>
			          <td width="74" height="22" background="images/background2.gif" > <div align="center" class="navigation"><a href="?n=analyses"><?php echo _t("Analyses"); ?></a></div></td>
			          <td width="64" height="22" background="images/background2.gif" > <div align="center" class="texte1"><a href="?n=mois&recul=mois"><?php echo _t("Du Mois"); ?> </a></div></td>
			        </tr>
			      </table>
	      	<?php	break;
	      	case "90": ?>
	      		<table border="1" cellspacing="0" cellpadding="0" align="center" bordercolor="#CCCCCC" width="510">
			        <tr bgcolor="#8020a0"> 
			          <td width="36" bgcolor="#8020a0" background="images/background2.gif" ><div align="center" class="navigation"><a href="?recul=150&n=150"  onMouseOver="drc('  Du <?php echo date('d/m/Y',time()-150*86400);?> au <?php echo date('d/m/Y');?>','&nbsp;&nbsp;Visuel statistique sur 150 jours'); return true;" onMouseOut="nd(); return true;">150</a></div></td>
			          <td width="37" bgcolor="#8020a0" background="images/background2.gif" ><div align="center" class="navigation"><a href="?recul=120&n=120"  onMouseOver="drc('  Du <?php echo date('d/m/Y',time()-120*86400);?> au <?php echo date('d/m/Y');?>','&nbsp;&nbsp;Visuel statistique sur 120 jours'); return true;" onMouseOut="nd(); return true;">120</a></div></td>
			          <td width="29" bgcolor="#FFFFFF"> 
			          <div align="center" class="navigation"><em>90</em></div></td>
			          <td width="31" height="22" background="images/background2.gif" > <div align="center" class="navigation"><a href="?recul=60&n=60"  onMouseOver="drc('  Du <?php echo date('d/m/Y',time()-60*86400);?> au <?php echo date('d/m/Y');?>','&nbsp;&nbsp;Visuel statistique sur 60 jours'); return true;" onMouseOut="nd(); return true;">60</a></div></td>
			          <td width="136" height="22" background="images/background2.gif" > <div align="center" class="navigation"><a href="?recul=30&n=30"  onMouseOver="drc('  Du <?php echo date('d/m/Y',time()-30*86400);?> au <?php echo date('d/m/Y');?>','&nbsp;&nbsp;Visuel statistique sur 30 jours'); return true;" onMouseOut="nd(); return true;">30 
			              <?php echo _t("DerniersJours"); ?></a></div></td>
			          <td width="81" height="22" background="images/background2.gif" > <div align="center" class="navigation"><a href="?recul=365&n=annuelles" onMouseOver="drc('  Du <?php echo date('d/m/Y',time()-365*86400);?> au <?php echo date('d/m/Y');?>','&nbsp;&nbsp;Visuel statistique sur 365 jours'); return true;" onMouseOut="nd(); return true;"><?php echo _t("Annuelles"); ?></a></div></td>
			          <td width="74" height="22" background="images/background2.gif" > <div align="center" class="navigation"><a href="?n=analyses"><?php echo _t("Analyses"); ?></a></div></td>
			          <td width="64" height="22" background="images/background2.gif" > <div align="center" class="texte1"><a href="?n=mois&recul=mois"><?php echo _t("Du Mois"); ?> </a></div></td>
			        </tr>
			      </table>
	      <?php	break;
	      	case "120": ?>
	      		<table border="1" cellspacing="0" cellpadding="0" align="center" bordercolor="#CCCCCC" width="510">
			        <tr bgcolor="#8020a0"> 
			          <td width="36" bgcolor="#8020a0" background="images/background2.gif" ><div align="center" class="navigation"><a href="?recul=150&n=150"  onMouseOver="drc('  Du <?php echo date('d/m/Y',time()-150*86400);?> au <?php echo date('d/m/Y');?>','&nbsp;&nbsp;Visuel statistique sur 150 jours'); return true;" onMouseOut="nd(); return true;">150</a></div></td>
			          <td width="37" bgcolor="#FFFFFF">
			<div align="center" class="navigation"><em>120</em></div></td>
			          <td width="29" background="images/background2.gif" > <div align="center" class="navigation"><a href="?recul=90&n=90"  onMouseOver="drc('  Du <?php echo date('d/m/Y',time()-90*86400);?> au <?php echo date('d/m/Y');?>','&nbsp;&nbsp;Visuel statistique sur 90 jours'); return true;" onMouseOut="nd(); return true;">90</a></div></td>
			          <td width="31" height="22" background="images/background2.gif" > <div align="center" class="navigation"><a href="?recul=60&n=60"  onMouseOver="drc('  Du <?php echo date('d/m/Y',time()-60*86400);?> au <?php echo date('d/m/Y');?>','&nbsp;&nbsp;Visuel statistique sur 60 jours'); return true;" onMouseOut="nd(); return true;">60</a></div></td>
			          <td width="136" height="22" background="images/background2.gif" > <div align="center" class="navigation"><a href="?recul=30&n=30"  onMouseOver="drc('  Du <?php echo date('d/m/Y',time()-30*86400);?> au <?php echo date('d/m/Y');?>','&nbsp;&nbsp;Visuel statistique sur 30 jours'); return true;" onMouseOut="nd(); return true;">30 
			              <?php echo _t("DerniersJours"); ?></a></div></td>
			          <td width="81" height="22" background="images/background2.gif" > <div align="center" class="navigation"><a href="?recul=365&n=annuelles" onMouseOver="drc('  Du <?php echo date('d/m/Y',time()-365*86400);?> au <?php echo date('d/m/Y');?>','&nbsp;&nbsp;Visuel statistique sur 365 jours'); return true;" onMouseOut="nd(); return true;"><?php echo _t("Annuelles"); ?></a></div></td>
			          <td width="74" height="22" background="images/background2.gif" > <div align="center" class="navigation"><a href="?n=analyses"><?php echo _t("Analyses"); ?></a></div></td>
			          <td width="64" height="22" background="images/background2.gif" > <div align="center" class="texte1"><a href="?n=mois&recul=mois"><?php echo _t("Du Mois"); ?> </a></div></td>
			        </tr>
			      </table>
	      	<?php	break;
	      	case "150": ?>
	      		<table border="1" cellspacing="0" cellpadding="0" align="center" bordercolor="#CCCCCC" width="510">
			        <tr bgcolor="#8020a0"> 
			          <td width="36" bgcolor="#FFFFFF">
			<div align="center" class="navigation"><em>150</em></div></td>
			          <td width="37" bgcolor="#8020a0" background="images/background2.gif" ><div align="center" class="navigation"><a href="?recul=120&n=120"  onMouseOver="drc('  Du <?php echo date('d/m/Y',time()-120*86400);?> au <?php echo date('d/m/Y');?>','&nbsp;&nbsp;Visuel statistique sur 120 jours'); return true;" onMouseOut="nd(); return true;">120</a></div></td>
			          <td width="29" background="images/background2.gif" > <div align="center" class="navigation"><a href="?recul=90&n=90"  onMouseOver="drc('  Du <?php echo date('d/m/Y',time()-90*86400);?> au <?php echo date('d/m/Y');?>','&nbsp;&nbsp;Visuel statistique sur 90 jours'); return true;" onMouseOut="nd(); return true;">90</a></div></td>
			          <td width="31" height="22" background="images/background2.gif" > <div align="center" class="navigation"><a href="?recul=60&n=60"  onMouseOver="drc('  Du <?php echo date('d/m/Y',time()-60*86400);?> au <?php echo date('d/m/Y');?>','&nbsp;&nbsp;Visuel statistique sur 60 jours'); return true;" onMouseOut="nd(); return true;">60</a></div></td>
			          <td width="136" height="22" background="images/background2.gif" > <div align="center" class="navigation"><a href="?recul=30&n=30"  onMouseOver="drc('  Du <?php echo date('d/m/Y',time()-30*86400);?> au <?php echo date('d/m/Y');?>','&nbsp;&nbsp;Visuel statistique sur 30 jours'); return true;" onMouseOut="nd(); return true;">30 
			              <?php echo _t("DerniersJours"); ?></a></div></td>
			          <td width="81" height="22" background="images/background2.gif" > <div align="center" class="navigation"><a href="?recul=365&n=annuelles" onMouseOver="drc('  Du <?php echo date('d/m/Y',time()-365*86400);?> au <?php echo date('d/m/Y');?>','&nbsp;&nbsp;Visuel statistique sur 365 jours'); return true;" onMouseOut="nd(); return true;"><?php echo _t("Annuelles"); ?></a></div></td>
			          <td width="74" height="22" background="images/background2.gif" > <div align="center" class="navigation"><a href="?n=analyses"><?php echo _t("Analyses"); ?></a></div></td>
			          <td width="64" height="22" background="images/background2.gif"> <div align="center" class="texte1"><a href="?n=mois&recul=mois"><?php echo _t("Du Mois"); ?> </a></div></td>
			        </tr>
			      </table>
	      	<?php	break;
	      	default: ?>
				<table border="1" cellspacing="0" cellpadding="0" align="center" bordercolor="#CCCCCC" width="510">
			        <tr bgcolor="#8020a0"> 
			          <td width="36" background="images/background2.gif" bgcolor="#8020a0"><div align="center" class="navigation"><a href="?recul=150&n=150"  onMouseOver="drc('  Du <?php echo date('d/m/Y',time()-150*86400);?> au <?php echo date('d/m/Y');?>','&nbsp;&nbsp;Visuel statistique sur 150 jours'); return true;" onMouseOut="nd(); return true;">150</a></div></td>
			          <td width="37" background="images/background2.gif" bgcolor="#8020a0"><div align="center" class="navigation"><a href="?recul=120&n=120"  onMouseOver="drc('  Du <?php echo date('d/m/Y',time()-120*86400);?> au <?php echo date('d/m/Y');?>','&nbsp;&nbsp;Visuel statistique sur 120 jours'); return true;" onMouseOut="nd(); return true;">120</a></div></td>
			          <td width="29" background="images/background2.gif"> <div align="center" class="navigation"><a href="?recul=90&n=90"  onMouseOver="drc('  Du <?php echo date('d/m/Y',time()-90*86400);?> au <?php echo date('d/m/Y');?>','&nbsp;&nbsp;Visuel statistique sur 90 jours'); return true;" onMouseOut="nd(); return true;">90</a></div></td>
			          <td width="31" height="22" background="images/background2.gif"> <div align="center" class="navigation"><a href="?recul=60&n=60"  onMouseOver="drc('  Du <?php echo date('d/m/Y',time()-60*86400);?> au <?php echo date('d/m/Y');?>','&nbsp;&nbsp;Visuel statistique sur 60 jours'); return true;" onMouseOut="nd(); return true;">60</a></div></td>
			          <td width="136" height="22" background="images/background2.gif"> <div align="center" class="navigation"><a href="?recul=30&n=30"  onMouseOver="drc('  Du <?php echo date('d/m/Y',time()-30*86400);?> au <?php echo date('d/m/Y');?>','&nbsp;&nbsp;Visuel statistique sur 30 jours'); return true;" onMouseOut="nd(); return true;">30 
			          <?php echo _t("DerniersJours"); ?></a></div></td>
			          <td width="81" height="22" background="images/background2.gif"> <div align="center" class="navigation"><a href="?recul=365&n=annuelles" onMouseOver="drc('  Du <?php echo date('d/m/Y',time()-365*86400);?> au <?php echo date('d/m/Y');?>','&nbsp;&nbsp;Visuel statistique sur 365 jours'); return true;" onMouseOut="nd(); return true;"><?php echo _t("Annuelles"); ?></a></div></td>
			          <td width="74" height="22" background="images/background2.gif"> <div align="center" class="navigation"><a href="?n=analyses"><?php echo _t("Analyses"); ?></a></div></td>
			          <td width="64" height="22" bgcolor="#FFFFFF"> 
			          <div align="center" class="texte1"><em><?php echo _t("Du Mois"); ?></em> </div></td>
			        </tr>
			      </table>
	     <?php }?>
	      <div align="center"><?php if($n=="analyses"){ ?>
			<table width="100%" border="0" cellspacing="0" cellpadding="0" height="3">
			  <tr>
			    <td></td>
			  </tr>
			</table>
			<table width="510" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#CCCCCC">
			        <tr background="images/background2.gif"> 
			            <td height="15" colspan="5" bgcolor="#8020a0" class="texte1"><font color="#FFFFFF">&nbsp;<?php echo _t("Taux de transformation = Une inscription pour x Clics (Sessions unique)"); ?></font></td>
			        </tr>        	        
			        <tr class="texte1"> 
			          <td width="27%" height="25"> 
			            <div align="center"><?php echo _t("Nb de Clics"); ?></div>
			            </td>
			          <td width="28%" height="25"> 
			            <div align="center"><?php echo _t("Nb Inscriptions"); ?></div>
			          </td>
			          <td width="45%" height="25" colspan="3"> 
			            <div align="center"><?php echo _t("Transformation moyenne"); ?></div>
			          </td>
			        </tr>
			        <tr class="texte1"> 
			          <td width="27%"> 
			            <div align="center">
			            	<?php echo number_format($sum_hit,0,".",",");?>	            	   	      
			            </div>
			          </td>
			          <td width="28%"> 
	            		<div align="center"> 
	            			<?php echo number_format($sum_inscription,0,".",",");?>		      
			            </div>
			          </td>
			          <td colspan="3"> 
			            <div align="center"><?php echo _t("Une inscription"); ?> / <?=number_format($sum_hit/$sum_inscription,2,".",",")?> <?php echo _t("Clics"); ?></div>
			          </td>
			        </tr>
			        
			        <tr background="images/background2.gif"> 
			            <td height="15" colspan="5" bgcolor="#8020a0" class="texte1">
			            	<div align="center"><font color="#FFFFFF"><?php echo _t("Elargir sur"); ?> // <a href="?n=analyses&recul=30"><?php echo _t("Dernier mois"); ?></a> / <a href="?n=analyses&recul=365"><?php echo _t("Année"); ?></a></font></div>
			            </td>
			        </tr>
			</table>
			<div align="center"><br>
				<img src="analyses.php?id=<?php echo $id;?>&ddate=<?php echo $ddate;?>&fdate=<?php echo $fdate;?>&recul=<?php echo $recul;?>&transformation=1"><br>
			</div>
			<?php }else{ ?>
		    <table width="100%" border="0" cellspacing="0" cellpadding="0" height="3">
		        <tr>
		          <td></td>
		        </tr>
		    </table>
		    <table width="510" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#CCCCCC">
		        <tr background="images/background2.gif"> 
		            <td height="15" colspan="5" bgcolor="#8020a0" class="texte1" background="images/background2.gif" ><font color="#FFFFFF">&nbsp;<?php echo _t("Total"); ?> 
		              <?php echo _t("de vos gains pour la période du"); ?> <?php echo $ddate;?> <?php echo _t("au"); ?> <?php echo $fdate;?></font></td>
		        </tr>
		        <tr class="texte1"> 
		          <td width="20%" height="25"> 
		            <div align="center"><?php echo _t("Clics"); ?></div>
		            </td>
		          <td width="20%" height="25"> 
		            <div align="center"><?php echo _t("Inscriptions"); ?></div>
		          </td>
		          <td width="23%" height="25"> 
		            <div align="center">CA HT*</div>
		          </td>
		          <td width="12%" height="25"> 
		            <div align="center"><?php echo _t("Ratio"); ?></div>
		          </td>
		          <td width="25%" height="25"> 
		            <div align="center"><b><?php echo _t("Reversement HT"); ?></b></div>
		          </td>
		        </tr>
		        <tr class="texte1"> 
		          <td width="20%"> 
		            <div align="center"> 
						<?php echo $tb_01_clics;?>
		            </div>
		          </td>
		          <td> 
		            <div align="center"> 
	            		<?php echo $tb_01_inscription;?>	      
	            	</div>
		          </td>
		          <td> 
		            <div align="center">
		            	<?php echo $tb_01_caht;?> &euro;
		            </div>
		          </td>          

		          <td> 
		            <div align="center"> 
		             <?php echo $tb_01_ratio;?> %
		            </div>
		          </td>
		          <td> 
		            <div align="center"> 
		              <b><?php echo $tb_01_reversement;?> &euro;</b>
		            </div>
		          </td>
		        </tr>
	      </table>
	      <div align="center"><br>
	      </div>
	      <div align="center"></div>
	      
		  <img src="graphs_others.php?id=<?php echo $id;?>&abonne=1&clics=0&inscriptions=0&hits=0&ddate=<?php echo $ddate;?>&fdate=<?php echo $fdate;?>&recul=<?php echo $recul;?>"><br><br>

	      <table width="510" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#CCCCCC">
	        <tr background="images/background2.gif"> 
	            <td height="15" colspan="5" bgcolor="#8020a0" class="texte1" background="images/background2.gif" ><font color="#FFFFFF">&nbsp;Evolution de vos Abonnés du <?php echo $ddate;?> au <?php echo $fdate;?></font></td>
	        </tr>
	        <tr class="texte1"> 
	          <td width="21%" height="25"> 
	            <div align="center"><?php echo _t("Qte Nouveaux"); ?></div>
	            </td>
	          <td width="20%" height="25"> 
	            <div align="center"><?php echo _t("CA Nouveaux"); ?></div>
	          </td>
	          <td width="16%" height="25"> 
	            <div align="center"><?php echo _t("Qte Rebills"); ?></div>
	          </td>
	          <td width="20%" height="25"> 
	            <div align="center"><?php echo _t("CA Rebills"); ?></div>
	          </td>
	          <td width="23%" height="25"> 
	            <div align="center"><?php echo _t("Qte Transacs"); ?></div>
	          </td>
	        </tr>
	        <tr class="texte1"> 
	          <td> 
	            <div align="center"> 

	      			<?php echo $tb_02_qte_nouveaux;?>

	            </div>
	          </td>
	          <td> 
	            <div align="center"> 
	            
	      			<?php echo $tb_02_ca_nouveaux;?> &euro;
	      
	            </div>
	          </td>
	          <td> 
	            <div align="center">
	      			<?php echo $tb_02_qte_rebills;?>
	            </div>
	          </td>          

	          <td> 
	            <div align="center"> 
	      			<?php echo $tb_02_ca_rebills;?> &euro;
	            </div>
	          </td>
	          <td> 
	            <div align="center"> 
					<?php echo $tb_02_qte_transacs;?>
	            </div>
	          </td>
	        </tr>
	      </table>
	      <br><br>

		  <img src="graphs_ca.php?id=<?php echo $id;?>&ddate=<?php echo $ddate;?>&fdate=<?php echo $fdate;?>&recul=<?php echo $recul;?>&ca=1&new=1&re=1"><br>
		  <br>
		  <?php if(!($recul!=30)){ ?>
			  <table width="510" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#CCCCCC">
		        <tr background="images/background2.gif">
		          <td height="15" colspan="3" bgcolor="#8020a0" class="texte1" background="images/background2.gif" ><font color="#FFFFFF">&nbsp;<?php echo _t("Evaluation Prévisionelle à 6/12 mois (Basé sur vos 30 dernier jours)"); ?> </font></td>
		        </tr>
		        <tr class="texte1">
		          <td width="28%" height="25">
		            <div align="center"><?php echo _t("Gains moyens"); ?> </div></td>
		          <td width="37%" height="25">
		            <div align="center"></div>
		            <div align="center"><?php echo _t("votre moyenne d'inscription"); ?> </div>            <div align="center"></div></td>
		          <td width="35%" height="25">
		            <div align="center"><?php echo _t("CA mensuel Prévisionnel"); ?></div></td>
		        </tr>
		        <tr class="texte1">
		          <td>
		            <div align="center">1,20 &euro; / <?php echo _t("Inscription"); ?>  </div></td>
		          <td>
		            <div align="center"></div>
		            <div align="center"> </div>            <div align="center"><?php echo number_format($sum_inscription,0,".",",");?> / <?php echo _t("mois"); ?> </div></td>
		          <td>
		            <div align="center"><?php echo number_format($sum_inscription*1.2,0,".",",");?> € HT</div></td>
		        </tr>
		      </table>
		  <br>
		  <?php }
		  if(!($recul!="test")){ ?>
		     <table width="510" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#CCCCCC">
		        <tr background="images/background2.gif"> 
		            <td height="15" colspan="5" bgcolor="#8020a0" class="texte1" background="images/background2.gif" ><font color="#FFFFFF">&nbsp;Cheminement prévisionnel de transformation de votre trafic</font></td>
		        </tr>

		        <tr class="texte1"> 
		          <td width="56%" height="25"> 
		            <div align="right"><?php echo _t("Base de Calcul"); ?> &nbsp;&nbsp;</div>
		          </td>
		          <td width="24%"> 
		            <div align="center">  &nbsp;&nbsp;<?php echo _t("Résultats moyens"); ?></div>
		          </td>
		          <td width="24%"> 
		            <div align="center">  &nbsp;&nbsp;<?php echo _t("Vos Résultats"); ?></div>
		          </td>
		        </tr> 
		  
		         <tr class="texte1"> 
		          <td> 
		            <div align="right"> 
		      <?php echo _t("Finesse du calcul en Jours"); ?>&nbsp;&nbsp;
		            </div>
		          </td>
		          <td> 
		            <div align="center">&nbsp;&nbsp;-
		            </div>
		          </td>
		          <td> 
		            <div align="center">&nbsp;&nbsp;<?php echo $anciennete;?>
		            </div>
		          </td>
		        </tr>

		         <tr class="texte1"> 
		          <td> 
		            <div align="right"> 
		      <?php echo _t("Qte de Paiements directs"); ?>&nbsp;&nbsp;
		            </div>
		          </td>
		          <td> 
		            <div align="center">&nbsp;&nbsp;-
		            </div>
		          </td>
		          <td> 
		            <div align="center">&nbsp;&nbsp;<?php echo $qte_direct;?>
		            </div>
		          </td>
		        </tr>
		                        
		         <tr class="texte1"> 
		          <td> 
		            <div align="right"> 
		      <?php echo _t("Visiteurs Unique"); ?> &nbsp;&nbsp;
		            </div>
		          </td>
		          <td> 
		            <div align="center">&nbsp;&nbsp;<?php echo $tb_06_01;?>
		            </div>
		          </td>
		          <td> 
		            <div align="center" onMouseOver="drc('<?php echo $tb_06_01_b;?>Infobulle avec Titre',' Visiteurs Unique'); return true;" onMouseOut="nd(); return true;"><?php echo $tb_06_01_a;?></div>
		          </td>
		        </tr>
		        
		        <tr class="texte1"> 
		          <td> 
		            <div align="right"><b><?php echo _t("Base : Qte d'Inscriptions"); ?></b> &nbsp;&nbsp;</div>
		          </td>
		          <td> 
		            <div align="center"> &nbsp;&nbsp;<b>1 000</b></div>
		          </td>
		          <td> 
		            <div align="center"> &nbsp;&nbsp;<b>1 000</b></div>
		          </td>
		        </tr> 
		              
		         <tr class="texte1">
		          <td> 
		            <div align="right"> 
		      <?php echo _t("Abonnements directs / 1000 Inscriptions"); ?> &nbsp;&nbsp;
		            </div>
		          </td>
		          <td> 
		            <div align="center"> 
		       &nbsp;&nbsp;<?php echo $tb_06_02;?>
		            </div>
		          </td>
		          <td> 
		            <div align="center"> 
		       &nbsp;&nbsp;<?php echo $tb_06_02_a;?>
		            </div>
		          </td>
		        </tr>
		     
		        <tr class="texte1">
		          <td> 
		            <div align="right"> 
		      <?php echo _t("Nouveaux abonnements à 12 mois"); ?> &nbsp;&nbsp;
		            </div>
		          </td>
		          <td> 
		            <div align="center"> 
		       &nbsp;&nbsp;<?php echo $tb_06_03;?>
		            </div>
		          </td>
		          <td> 
		            <div align="center"> 
		       &nbsp;&nbsp;<?php echo $tb_06_03_a;?>
		            </div>
		          </td>
		        </tr>    

		         <tr class="texte1">
		          <td> 
		            <div align="right"> 
		      <?php echo _t("Rebill moyen à 12 mois"); ?>  &nbsp;&nbsp;
		            </div>
		          </td>
		          <td> 
		            <div align="center"> 
		       &nbsp;&nbsp;<?php echo $tb_06_04;?>
		            </div>
		          </td>
		          <td> 
		            <div align="center"> 
		       &nbsp;&nbsp;<?php echo $tb_06_04_a;?>
		            </div>
		          </td>
		        </tr>
		        

		        <tr class="texte1">
		          <td> 
		            <div align="right"> 
		      <?php echo _t("Qte de rebill à 12 mois"); ?> &nbsp;&nbsp;
		            </div>
		          </td>
		          <td> 
		            <div align="center"> 
		       &nbsp;&nbsp;<?php echo $tb_06_05;?>
		            </div>
		          </td>
		          <td> 
		            <div align="center"> 
		       &nbsp;&nbsp;<?php echo $tb_06_05_a;?>
		            </div>
		          </td>
		        </tr>        

		         <tr class="texte1">
		          <td> 
		            <div align="right"> 
		     <?php echo _t("Qte de transactions à 12 mois"); ?> &nbsp;&nbsp;
		            </div>
		          </td>
		          <td> 
		            <div align="center"> 
		       &nbsp;&nbsp;<?php echo $tb_06_06;?>
		            </div>
		          </td>
		          <td> 
		            <div align="center"> 
		       &nbsp;&nbsp;<?php echo $tb_06_06_a;?>
		            </div>
		          </td>
		        </tr>

		         <tr class="texte1">
		          <td> 
		            <div align="right"> 
		     <?php echo _t("CA HT moyen par transactions"); ?> &nbsp;&nbsp;
		            </div>
		          </td>
		          <td> 
		            <div align="center"> 
		       &nbsp;&nbsp;<?php echo $tb_06_07;?> &euro;
		            </div>
		          </td>
		          <td> 
		            <div align="center"> 
		       &nbsp;&nbsp;<?php echo $tb_06_07_a;?> &euro;
		            </div>
		          </td>
		        </tr>

		         <tr class="texte1">
		          <td> 
		            <div align="right"> 
		     <?php echo _t("Total de CA HT sur 12 mois"); ?> &nbsp;&nbsp;
		            </div>
		          </td>
		          <td> 
		            <div align="center"> 
		       &nbsp;&nbsp;<?php echo $tb_06_08;?> &euro;
		            </div>
		          </td>
		          <td> 
		            <div align="center"> 
		       &nbsp;&nbsp;<?php echo $tb_06_08_a;?> &euro;
		            </div>
		          </td>
		        </tr>

		         <tr class="texte1">
		          <td> 
		            <div align="right"> 
		     <?php echo _t("CA HT par inscription"); ?> &nbsp;&nbsp;
		            </div>
		          </td>
		          <td> 
		            <div align="center"> 
		       &nbsp;&nbsp;<?php echo $tb_06_09;?> &euro;
		            </div>
		          </td>
		          <td> 
		            <div align="center"> 
		       &nbsp;&nbsp;<?php echo $tb_06_09_a;?> &euro;
		            </div>
		          </td>
		        </tr>

		         <tr class="texte1">
		          <td> 
		            <div align="right"> 
		     <?php echo _t("Votre Ratio (Reversement)"); ?> &nbsp;&nbsp;
		            </div>
		          </td>
		          <td> 
		            <div align="center"> 
		       &nbsp;&nbsp;<?php echo $tb_06_10;?> %
		            </div>
		          </td>
		          <td> 
		            <div align="center"> 
		       &nbsp;&nbsp;<?php echo $tb_06_10_a;?> %
		            </div>
		          </td>
		        </tr>

		         <tr class="texte1">
		          <td> 
		            <div align="right"> 
		     <?php echo _t("Votre CA HT par inscription"); ?> &nbsp;&nbsp;
		            </div>
		          </td>
		          <td> 
		            <div align="center"> 
		       &nbsp;&nbsp;<?php echo $tb_06_11;?>  &euro;
		            </div>
		          </td>
		          <td> 
		            <div align="center"> 
		       &nbsp;&nbsp;<?php echo $tb_06_11_a;?>  &euro;
		            </div>
		          </td>
		        </tr>

		         <tr class="texte1">
		          <td> 
		            <div align="right"> 
		     <?php echo _t("Votre CA HT par visiteur unique"); ?> &nbsp;&nbsp;
		            </div>
		          </td>
		          <td> 
		            <div align="center"> 
		       &nbsp;&nbsp;<?php echo $tb_06_12;?>  &euro;
		            </div>
		          </td>
		          <td> 
		            <div align="center"> 
		       &nbsp;&nbsp;<?php echo $tb_06_12_a;?>  &euro;
		            </div>
		          </td>
		        </tr>

		         <tr class="texte1">
		          <td> 
		            <div align="right"> 
		     <?php echo _t("Total de votre CA HT sur 12 mois"); ?> &nbsp;&nbsp;
		            </div>
		          </td>
		          <td> 
		            <div align="center"> 
		       &nbsp;&nbsp;<?php echo $tb_06_13;?> &euro;
		            </div>
		          </td>
		          <td> 
		            <div align="center"> 
		       &nbsp;&nbsp;<?php echo $tb_06_13_a;?> &euro;
		            </div>
		          </td>
		        </tr>

		         <tr class="texte1">
		          <td> 
		            <div align="right"> 
		     <?php echo _t("Qte de Visiteurs / jours"); ?> &nbsp;&nbsp;
		            </div>
		          </td>
		          <td> 
		            <div align="center"> 
		       &nbsp;&nbsp;<?php echo $tb_06_14;?>
		            </div>
		          </td>
		          <td> 
		            <div align="center"> 
		       &nbsp;&nbsp;<?php echo $tb_06_14_a;?>
		            </div>
		          </td>
		        </tr>

		         <tr class="texte1">
		          <td> 
		            <div align="right"> 
		     <?php echo _t("Qte de Inscriptions / jours"); ?> &nbsp;&nbsp;
		            </div>
		          </td>
		          <td> 
		            <div align="center"> 
		       &nbsp;&nbsp;<?php echo $tb_06_15;?>
		            </div>
		          </td>
		          <td> 
		            <div align="center"> 
		       &nbsp;&nbsp;<?php echo $tb_06_15_a;?>
		            </div>
		          </td>
		        </tr>

		         <tr class="texte1">
		          <td> 
		            <div align="right"> 
		     <?php echo _t("CA HT moyen / jours à 12 mois"); ?> &nbsp;&nbsp;
		            </div>
		          </td>
		          <td> 
		            <div align="center"> 
		       &nbsp;&nbsp;<?php echo $tb_06_16;?>
		            </div>
		          </td>
		          <td> 
		            <div align="center"> 
		       &nbsp;&nbsp;<?php echo $tb_06_16_a;?> &euro;
		            </div>
		          </td>
		        </tr>  
		        
		         <tr class="texte1">
		          <td> 
		            <div align="right"> 
		     <b><?php echo _t("CA HT moyen / Mois à 12mois"); ?></b> &nbsp;&nbsp;
		            </div>
		          </td>
		          <td> 
		            <div align="center"> 
		       &nbsp;&nbsp;<?php echo $tb_06_17;?>
		            </div>
		          </td>
		          <td> 
		            <div align="center"> 
		       &nbsp;&nbsp;<b><?php echo $tb_06_17_a;?> &euro;</b>
		            </div>
		          </td>
		        </tr>         
		                                                                     
		      </table>
		      <br>
	      <?php }?>   
	      <br>

		  <img src="graphs_others.php?id=<?php echo $id;?>&clics=0&inscriptions=1&hits=0&ddate=<?php echo $ddat;?>&fdate=<?php echo $fdate;?>&recul=<?php echo $recul;?>"><br><br>
		<?php if(!($recul!="test")){ ?>
	      <table width="510" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#CCCCCC">
	        <tr background="images/background2.gif"> 
	            <td height="15" colspan="5" bgcolor="#8020a0" class="texte1" background="images/background2.gif" ><font color="#FFFFFF">&nbsp;<?php echo _t("Vos gains Immédiats par poste du"); ?> <?php echo $ddate;?> <?php echo _t("au"); ?> <?php echo $fdate;?></font></td>
	        </tr>
	        <tr class="texte1"> 
	          <td width="20%" height="25"> 
	            <div align="center"><?php echo _t("Par Nouveaux"); ?></div>
	            </td>
	          <td width="16%" height="25"> 
	            <div align="center"><?php echo _t("Par Rebills"); ?></div>
	          </td>
	          <td width="24%" height="25"> 
	            <div align="center"><?php echo _t("Par Transaction"); ?></div>
	          </td>
	          <td width="18%" height="25"> 
	            <div align="center"><?php echo _t("Par Visiteur"); ?></div>
	          </td>
	          <td width="22%" height="25"> 
	            <div align="center"><?php echo _t("Par Inscription"); ?></div>
	          </td>
	        </tr>
	        <tr class="texte1"> 
	          <td> 
	            <div align="center"> 
	            
	      			<?php echo $tb_03_par_nouveaux;?> &euro;

	            </div>
	          </td>
	          <td> 
	            <div align="center"> 
	            
	      			<?php echo $tb_03_par_rebills;?> &euro;
	      
	            </div>
	          </td>
	          <td> 
	            <div align="center">
	      			<?php echo $tb_03_par_transacs;?> &euro;
	            </div>
	          </td>          

	          <td> 
	            <div align="center"> 
	      			<?php echo $tb_03_par_visiteur;?> &euro;
	            </div>
	          </td>
	          <td> 
	            <div align="center"> 
	      			<?php echo $tb_03_par_inscription;?> &euro;
	            </div>
	          </td>
	        </tr>
	      </table>
	      <br>
      		<?php }?>	      
	      <br>

		  <img src="graphs_others.php?id=<?php echo $id;?>&clics=1&inscriptions=0&hits=1&ddate=<?php echo $ddate;?>&fdate=<?php echo $fdate;?>&recul=<?php echo $recul;?>"><br>
		  <?php }?>
	      </div>
	    </td>
	  </tr>
	</table>

	<div align="center">

	        <table border="1" cellspacing="0" cellpadding="0" align="center" bordercolor="#CCCCCC" width="510" height="20" class="texte1">
	          <tr bgcolor="#8020a0"> 
	            <td background="images/background2.gif" bgcolor="#8020a0"> 
	              &nbsp;&nbsp;<a href="/stats.php" target="_blank" color="#999999"><?php echo _t("Complements Statistiques"); ?></a>
	            </td>
	          </tr>
	        </table>

	<br><br>
	<span class="copyright">
	<font color="#000000"><?php echo _t("© All Rights Reserved - Tous Droits Réservés"); ?> // <?php echo $id;?> <?php echo _t("*Aprés abattements"); ?></font> </span> </div>
	</body>	    
</html>

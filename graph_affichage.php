<?php
  session_start();
  //require(dirname(__FILE__). '/global_conn.php');
  require (dirname(__FILE__). '/jpgraph/jpgraph.php');
  require (dirname(__FILE__). '/jpgraph/jpgraph_line.php');
  require (dirname(__FILE__). '/jpgraph/jpgraph_scatter.php');
  
  header("Expires: " . gmdate("D, d M Y H:i:s", time() + (0*60)) . " GMT"); 
	ob_start();
	ob_clean();
?>
<?php
$s = !empty($_REQUEST["s"])?$_REQUEST["s"]:"";
$Title = "";
$gr_data = array();
//----------------------------------------
//exemples quand il n'y a pas de session
if (empty($s)){
  print "pour le mode debugg  ajouter \"&debugg=1\"<br>";
  print "<a href=\"?s=exemple1\">exemple1</a><br>";
  print "<a href=\"?s=exemple2\">exemple2</a><br>";
  print "<a href=\"?s=exemple3\">exemple3</a><br>";
  print "<a href=\"?s=exemple4\">exemple4</a><br>";
  print "<a href=\"?s=exemple5\">exemple5</a><br>";
  print "<a href=\"?s=exemple6\">exemple6</a><br>";
  print "<a href=\"?s=exemple7\">exemple7</a><br>";
  exit();
}
$debugg = !empty($_REQUEST["debugg"])?$_REQUEST["debugg"]:"";
if (!empty($debugg)){
  header("Content-type: "."Image/Gif"); 
}
switch ($s) {
    case 'exemple1':
		//$Title = "exemple1";
		$gr_data = array(
			'title' => 'exemple1',
			'data' => array(
				'datax' => array(
					array(
					rand(1, 100),
					rand(1, 100),
					rand(1, 100),
					rand(1, 100),
					rand(1, 100),
					rand(1, 100),
					rand(1, 100),
					rand(1, 100),
					rand(1, 100),
					rand(1, 100),
					rand(1, 100),
					rand(1, 100)
					),
					array(
						rand(1, 100),
						rand(1, 100),
						rand(1, 100)
						),
				),
				'datay' => array(
					
				)
			)
		);
        break;
    case 'exemple2':
		$Title = "exemple2";
		$gr_data = array(
			
		);
        break;
    case 'exemple3':
		$Title = "exemple3";
		$gr_data = array(
			
		);
        break;
	case 'exemple4':
		$Title = "exemple4";
		$gr_data = array(
			
		);
        break;
	case 'exemple5':
		$Title = "exemple5";
		$gr_data = array(
			
		);
        break;
	case 'exemple6':
		$Title = "exemple6";
		$gr_data = array(
			
		);
        break;
    default:
	 break;
}


//----------------------------------

// Setup the graph
$graph = new Graph(1000,300);
$gr_data = !empty($_SESSION[" ".$s])?$_SESSION[" ".$s]:"";
//var_dump($gr_data);exit();
$count_data = count($gr_data['data']['datax'][0]['data']);
$max = 0;
if(empty($gr_data)){
	$graph->SetScale('int',0,1,0,1);
}else{
	if($count_data == 1){
		// tinh max
		foreach($gr_data['data']['datay'][0]['data'] as $item){
			if($max < $item){
				$max = $item;
			}
		}
		$graph->SetScale('int',0,$max*1.2,0,1);
	}else{
		//$graph->SetScale("textlin");
		$graph->SetScale("int");
		$graph->yaxis->scale->SetAutoMin(0);
	}
	$_SESSION[" ".$s] = "";
}
$graph->img->SetAntiAliasing(false);
$graph->img->SetTransparent("white"); 
$graph->title->Set($gr_data['title']);
$graph->title->SetColor("#000000");
$graph->title->SetFont(FF_FONT1); 
$graph->SetBox(false);


$graph->legend->SetPos(0.05,0,'right','top');
$graph->legend->SetColumns(1);
//$graph->xaxis->SetTextTickInterval(2,-1);
//$graph->xaxis->HideTicks(true,false);
//var_dump($gr_data['data']['datax'][0]['data']);exit();
 /* if($count_data == 1){
	$bdate = $gr_data['data']['datax'][0]['data'][0];
	$bdate = str_replace('/', '-', $bdate);
	$bdate_timestamp = strtotime($bdate);
	$bdate = strtotime ( '-1 day' , $bdate_timestamp ) ;
	$bdate = date ( 'd/m/Y' , $bdate );
	array_unshift($gr_data['data']['datax'][0]['data'], $bdate);
}  */
$graph->xaxis->SetTickLabels($gr_data['data']['datax'][0]['data']);
if ($count_data >= 20)
	$graph->xaxis->SetTextLabelInterval(2);
elseif($count_data >= 50)
	$graph->xaxis->SetTextLabelInterval(6);
//$graph->xaxis->SetTextLabelInterval(2);
//$graph->xaxis->HideLastTickLabel();
//$graph->xaxis->scale->ticks->SetSize(30,3);
$graph->xaxis->SetLabelAngle(90);
//$graph->xaxis->SetLabelFormatString("%-02.1f");
$graph->ygrid->SetLineStyle("dotted");
$graph->ygrid->SetColor('#464637');


//draw graph with graph data
//var_dump((!empty($gr_data) && count($gr_data)>2));exit();
//$gr_data = !empty($_SESSION[" ".$s])?$_SESSION[" ".$s]:"";
//var_dump($gr_data['data']['datay']); exit();
if(!empty($gr_data)){
	$i = 0;
	foreach($gr_data['data']['datay'] as $data){
		$lacouleur = array("#CC3366","#FF33FF","#9966CC","#993333","#FF9900","#66CCFF","#339966","#669933","#66CCFF","00ff00","0000ff","ff0000","#FF9933","#6633CC","#00CCCC","#FF3300","#CCFFCC",);
		
		//${"p" . $i} = new LinePlot($data['data'],$gr_data['data']['datax'][0]['data']);
		$data_co = count($data['data']);
		 if(count($data['data']) <= 1){
			${"s" . $i} = new ScatterPlot($data['data']);
			${"s" . $i}->mark->SetFillColor($lacouleur[($i+1)]);
			${"s" . $i}->mark->SetColor($lacouleur[($i+1)]);
			$graph->Add(${"s" . $i});
		} 
		${"p" . $i} = new LinePlot($data['data']);
		//${"p" . $i}->SetBarCenter(); 
		$graph->Add(${"p" . $i});
		${"p" . $i}->SetWeight(1); 
		${"p" . $i}->SetColor($lacouleur[($i+1)]);
		${"p" . $i}->SetLegend($data['title']);
		//${"p" . $i}->SetLegend($max);
		//${"p" . $i}->value->SetAlign('left'); 
		//${"p" . $i}->value->SetAngle(45); 
		//${"p" . $i}->value->SetMargin(20);
		$data_max = max($data['data']); 
		$data_min = min($data['data']); 
		${"p" . $i}->value->SetFormat('%d'); 
		switch($data['title']){
			case "nb clics":
				${"p" . $i}->value->SetMargin(0);
				break;
			case "nb paiements auto":
				${"p" . $i}->value->SetMargin(0);
				break;
			default:
				${"p" . $i}->value->SetMargin(0);
				break;
		}
		

		${"p" . $i}->value->SetAlign('left'); 
		${"p" . $i}->value->M_Show(true,$data_co,$data_max,$data_min);
		
		${"p" . $i}->SetStyle("solid"); 
		$i++;
	}
}
$graph->legend->SetFrameWeight(1);
$graph->legend->Pos( 0.3,0.055,"right","top");
$graph->legend->SetLayout(LEGEND_VERT);
$graph->legend->SetLineWeight(10);
// Output line
$graph->Stroke(); 


?>

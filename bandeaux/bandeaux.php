<?php
session_start();
require dirname(__FILE__).'/../global_conn.php';
$id_affilie = !empty($_SESSION['id_affilie']) ? $_SESSION['id_affilie'] : 0;
$demo = !empty($_SESSION['demo']) ? $_SESSION['demo'] : "";

//global $id_affilie_demo;
$id_affilie_demo = 22545;

if(($id_affilie==0)||(!is_numeric($id_affilie))||($demo=="1")){
	$id_affilie = $id_affilie_demo;
	$_SESSION['demo'] = "1";
	$demo = "1";
}

if($demo =="1"){
	$msg_demo = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>"._t("Espace Démo")."</b>";
}else{
	$msg_demo = "";
}
?>
<html>
<head>
<title><?php echo _t("Lyad affiliation"); ?></title>
<META http-equiv=Page-Enter content=blendTrans(Duration=1.0)>

<link href="../affiliation.css" rel="stylesheet" type="text/css">
<link href="tsm.css" type="text/css" rel="stylesheet" />
<STYLE type=text/css>

	TD.titre {
		FONT-SIZE: 11px; 
		CURSOR: hand; 
		COLOR: #000000; 
		FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; 
		BACKGROUND-COLOR: #959ead
	}
	TD.soustitre {
		FONT-SIZE: 10px; 
		CURSOR: hand; 
		COLOR: #000000; 
		FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; 
		BACKGROUND-COLOR: #c3c9d4
	}
.Style1 {font-size: 10pt; font-weight: normal; font-variant: normal; font-family: Verdana;}
</STYLE>

<script language="javascript" src="<?php echo HOST;?>/bandeaux/tsm.js" type="text/javascript"></script>
<SCRIPT language=JavaScript src="<?php echo HOST;?>/bandeaux/menu_dhtml.js"></SCRIPT>

<SCRIPT LANGUAGE="javascript">
function ShowTooltip(fArg){
		var tooltipOBJ = eval("document.all['tt" + fArg + "']");
		var tooltipOffsetTop = tooltipOBJ.scrollHeight + 15;
		var testTop = (document.body.scrollTop + event.clientY) - tooltipOffsetTop;
		var testLeft = event.clientX - 250;
		var tooltipAbsLft = (testLeft < 0) ? 160 : testLeft;
		var tooltipAbsTop = (testTop < document.body.scrollTop) ? document.body.scrollTop + 10 : testTop;
		tooltipOBJ.style.posLeft = tooltipAbsLft;
		tooltipOBJ.style.posTop = tooltipAbsTop;
		tooltipOBJ.style.visibility = "visible";
		}

function HideTooltip(fArg){
	var tooltipOBJ = eval("document.all['tt" + fArg + "']");
	tooltipOBJ.style.visibility = "hidden";
	}

function tsmInitAll()
{
	with (atsm = new tabStripMenu("showMe",510,300))
	{
		setMargin(2);


		addTab(   "<IMG height=21 src='images/picto.gif' width=21 alt='Image'>",
			  "<iframe style='border:none' name='exemple' height='350' src='exemple.php' width='500' FRAMEBORDER='0'></iframe>"
			+ "");			

		addTab(	  "Ex",
			  "<iframe style='border:none' name='formats' height='730' src='formats.php' width='500' FRAMEBORDER='0'></iframe>"
			+ "");	
	
		addTab(	  "60x60",
			  "<iframe style='border:none' name='formats' height='550' src='60x60.php' width='500' FRAMEBORDER='0'></iframe>"
			+ "");	

		addTab(   "88x31",
			  "<iframe style='border:none' name='formats' height='350' src='88x31.php' width='500' FRAMEBORDER='0'></iframe>"
			+ "");	
		
		addTab(   "90x53",
			  "<iframe style='border:none' name='formats' height='520' src='90x53.php' width='500' FRAMEBORDER='0'></iframe>"
			+ "");	
			
		addTab(   "120x90",
			  "<iframe style='border:none' name='formats' height='350' src='120x90.php' width='500' FRAMEBORDER='0'></iframe>"
			+ "");

		addTab(   "234x60",
			  "<iframe style='border:none' name='formats' height='550' src='234x60.php' width='500' FRAMEBORDER='0'></iframe>"
			+ "");

		addTab(   "260x80",
			  "<iframe style='border:none' name='formats' height='600' src='260x80.php' width='500' FRAMEBORDER='0'></iframe>"
			+ "");
			
		addTab(   "468x60",
			  "<iframe style='border:none' name='formats' height='600' src='468x60.php' width='500' FRAMEBORDER='0'></iframe>"
			+ "");
			
		addTab(   "120x600",
			  "<iframe style='border:none' name='formats' height='1350' src='120x600.php' width='500' FRAMEBORDER='0'></iframe>"
			+ "");

		addTab(   "Top",
			  "<iframe style='border:none' name='formats' height='1350' src='tops.php' width='500' FRAMEBORDER='0'></iframe>"
			+ "");
									
			build();
	}
}
</SCRIPT>

</head>

<body onload="tsmInitAll(); tsmLoadAll()" >

<table width="80%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr> 
    <td class="texte1">
    	<?php 
    	if(isset($_SESSION['demo'])&&($_SESSION['demo']=="1")){
    	?>
      <div align="center"> 
        <table border="1" cellspacing="0" cellpadding="0" align="center" bordercolor="#CCCCCC" width="100%">
          <tr bgcolor="#8020a0"> 
            <td background="../images/background2.gif" bgcolor="#8020a0"> 
              <div align="left" class="navigation2"><font color="#FFFFFF">&nbsp;<b><font size="2"><?php echo _t("Espace Démo"); ?></font></b></font></div>
            </td>
          </tr>
        </table>
      	<table width="100%" border="0" cellspacing="0" cellpadding="0" height="3">
        <tr>
          <td></td>
        </tr>
      	</table>
		<?php }?>
      </div>
      <table border="1" cellspacing="0" cellpadding="0" align="center" bordercolor="#CCCCCC" width="510">
        <tr> 
          <td width="131" height="21" background="../images/background2.gif" bgcolor="#8020a0"> 
            <div align="center">
            <?php if(isset($_SESSION['demo'])&&($_SESSION['demo']=="1")){ ?>
				<a href="#" class="navigation" onMouseOver="ShowTooltip(1);" onMouseOut="HideTooltip(1);" ><?php echo _t("Vos Coordonnées"); ?></a></div>
				<div class=reminderTooltip id=tt1 style="WIDTH: 160px;"><?php echo _t("Indisponible en Espace Demo !"); ?></div>
            <?php }else{ ?>
				<a href="../coordonnees.php" class="navigation"><?php echo _t("Vos Coordonées"); ?></a></div>
			<?php }?>
            </div>
          </td>
          <td width="115" height="21" background="../images/background2.gif" bgcolor="#8020a0"> 
            <div align="center" class="texte1"><a href="../indexstats.php"><?php echo _t("Vos Statistiques"); ?></a></div>
          </td>
          <td height="21" width="153"> 
            <div align="center" class="navigation"><em><?php echo _t("Bandeaux &amp; Outils"); ?></em> 
            </div>
          </td>
          <td width="97" height="21" background="../images/background2.gif" bgcolor="#8020a0"> 
            <div align="center"><a href="../choixdep.php" class="navigation"> </a>
            <?php if(isset($_SESSION['demo'])&&($_SESSION['demo']=="1")){ ?>
                <a href="#" class="navigation" onMouseOver="ShowTooltip(2);" onMouseOut="HideTooltip(2);" ><?php echo _t("Facturation"); ?></a></div>
              	<div class=reminderTooltip id=tt2 style="WIDTH: 160px;"><?php echo _t("Indisponible en Espace Demo !"); ?></div>
            <?php }else{ ?>
              	<a href="../appel_facture_list.php" class="navigation"><?php echo _t("Facturation"); ?></a> 
			<?php }?>        
            </div>
          </td>
        </tr>
      </table>

      <table width="100%" border="0" cellspacing="0" cellpadding="0" height="3">
        <tr>
          <td></td>
        </tr>
      </table>
      
      <table width="100%" height="21" border="1" cellspacing="0" cellpadding="0"  bordercolor="#CCCCCC" >
        <tr> 
          <td background="../images/background2.gif" bgcolor="#8020a0" class="navigation2"> 
            <font color="#FFFFFF" size="2">&nbsp;<?php echo _t("Les bandeaux"); ?> / <a href="default.php"><?php echo _t("Les liens"); ?></a> / <a href="div.php"><?php echo _t("La Div Glissante"); ?></a>/ <?php echo $msg_demo;?></font>
          </td>
        </tr>
      </table>

      <table width="100%" border="0" cellspacing="0" cellpadding="0" height="3">
        <tr>
          <td></td>
        </tr>
      </table>

<div id="showMe" ></div>
<br>
<div class="copyright"><?php echo _t("© All Rights Reserved - Tous Droits Résevés"); ?></div>
</body>
</html>

<html>
<head>
<title>Lyad.com // Formats</title>
<style type="text/css">
<!--
.xcode {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
}
-->
</style>
</head>

<body leftmargin="0" topmargin="5" rightmargin="0">
<table width="490" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td valign="top"><div align="center">      <table width="468" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td><div align="left"><strong class="xcode">468x60</strong><br>            
                <img src="images/468x60_jpg_07.jpg" width="468" height="60"></div></td>
        </tr>
      </table>
      <br>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="32%"><table width="120" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
              <td><div align="left"><strong class="xcode">120x600</strong><br>                
                    <img src="images/bandeau_02_a.jpg" width="120" height="600"></div></td>
            </tr>
          </table></td>
          <td width="68%" valign="top">
            <table width="260" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td><div align="left"><strong class="xcode">260x80</strong><br>                  
                      <img src="images/260x80_v2_04.jpg" width="260" height="80"></div></td>
              </tr>
            </table>            <br>            <table width="120" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td><div align="left"><strong class="xcode">120x90</strong><br>                  
                      <img src="images/120x90_jpg_V07.jpg" width="120" height="90"></div></td>
              </tr>
            </table>
            <br>            
            <table width="90" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td><div align="left"><strong class="xcode">90x53</strong><br>                  
                      <img src="images/90x53_jpg_14.jpg" width="90" height="53"></div></td>
              </tr>
            </table>
            <br>
            <table width="60" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td><div align="left"><strong class="xcode">60x60</strong><br>                  
                      <img src="images/bandeau_60x60_03_b.jpg" width="60" height="60"></div></td>
              </tr>
            </table>            </td>
          </tr>
      </table>
      </div></td>
  </tr>
</table>
</body>
</html>

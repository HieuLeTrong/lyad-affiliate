var nbTotalMenu = 2;

function clicMenu(num) {
	// Cette fonction gere à elle seule les effets de 
	// d&eacute;roulement de notre menu vertical
	
	// Booleen reconnaissant le navigateur
	isIE = (document.all) 
	isNN6 = (!isIE) && (document.getElementById)

	// Compatibilit&eacute; : l'objet menu est detect&eacute; selon le navigateur
	if (isIE) menu =  document.all['menufirst' + num];
	if (isNN6) menu = document.getElementById('menufirst' + num);

	// On ferme tout les menus d&eacute;jà ouvert
	for (i=1;i<=nbTotalMenu;i++) {
		if (i != num) {
			if (isIE) menuDiff =  document.all['menufirst'+i];
			if (isNN6) menuDiff = document.getElementById('menufirst'+i);
			if (menuDiff.style.display != "none") {
				menuDiff.style.display = "none"
				document.images['image'+i].src='fleche2.gif';
			}
		}
	}

	// On ouvre ou ferme celui qui vient d'être cliqu&eacute;
	if (menu.style.display == "none"){
		// Cas ou le tableau est cach&eacute;
		menu.style.display = ""
		document.images['image'+num].src='fleche.gif';
	} else {
		// On le cache
		menu.style.display = "none"
		document.images['image'+num].src='fleche2.gif';
	}
}

function changeCouleur(cellule,couleurFond,couleurPolice) {
	// Cette fonction modifie la couleur de fond 
	// et de police d'une cellule de tableau
	cellule.style.backgroundColor = couleurFond;
	cellule.style.color=couleurPolice;
}

function ouvrirLien(url,ouverture,cible) {
	// Cette fonction permet d'ouvrir trois type de lien 
	// 1 : Dans la fenêtre courante
	// 2 : Dans une nouvelle fenêtre
	// 3 : Dans une frame dont la cible doit être pr&eacute;cis&eacute;
	
	switch (ouverture) {
		case "1" :
			// Ouverture dans la fenetre courante
			document.location.href = url;
			break;
		case "2" :
			// Ouverture d'une nouvelle fenetre
			window.open(url)
			break;
		case "3" :
			// Ouverture dans une frame
			window.top.parent.frames[cible].location.href  = url
			break;
		default :
			// Normalement, on n'arrive jamais ici
			alert('Erreur : Cette valeur est hors limite');
			break;
	}
}






/////Pr exemple

function clicMenus(num) {

// Booleen reconnaissant le navigateur
	isIE = (document.all) 
	isNN6 = (!isIE) && (document.getElementById)
	
// Compatibilit&eacute; : l'objet menu est d&eacute;tect&eacute; selon le navigateur
	if (isIE) menu =  document.all['menu1' + num];
	if (isNN6) menu = document.getElementById('menu1' + num);

// On ouvre ou ferme
	if (menu.style.display == "none"){
		// Cas ou le tableau est cach&eacute;
		menu.style.display = ""

	} else {
		// On le cache
		menu.style.display = "none"

	}
}

function clicMenuss(num) {

	// Booleen reconnaissant le navigateur
	isIE = (document.all) 
	isNN6 = (!isIE) && (document.getElementById)
	
// Compatibilit&eacute; : l'objet menu est d&eacute;tect&eacute; selon le navigateur
	if (isIE) menu =  document.all['menu2' + num];
	if (isNN6) menu = document.getElementById('menu2' + num);

// On ouvre ou ferme
	if (menu.style.display == "none"){
		// Cas ou le tableau est cach&eacute;
		menu.style.display = ""

	} else {
		// On le cache
		menu.style.display = "none"

	}
}

function clicMenus4(num) {

	// Booleen reconnaissant le navigateur
	isIE = (document.all) 
	isNN6 = (!isIE) && (document.getElementById)
	
// Compatibilit&eacute; : l'objet menu est d&eacute;tect&eacute; selon le navigateur
	if (isIE) menu =  document.all['menu3' + num];
	if (isNN6) menu = document.getElementById('menu3' + num);

// On ferme tout les menus d&eacute;jà ouvert
	for (i=1;i<=nbTotalMenu;i++) {
		if (i != num) {
			if (isIE) menuDiff =  document.all['menu3'+i];
			if (isNN6) menuDiff = document.getElementById('menu3'+i);
			if (menuDiff.style.display != "none") {
				menuDiff.style.display = "none"
			}
		}
	}
	
// On ouvre ou ferme
	if (menu.style.display == "none"){
		// Cas ou le tableau est cach&eacute;
		menu.style.display = ""

	} else {
		// On le cache
		menu.style.display = "none"

	}
}
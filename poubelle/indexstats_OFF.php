<?php
session_start();
require(dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . '/global_conn.php');  
    $jour=strftime(date("d"));
    if ($jour<10)
    {
        $jour="0".$jour;
    } 
    $mois=strftime(date("m"));
    if ($mois<10)
    {
        $mois="0".$mois;
    } 
    $annee=strftime(date("Y"));
    function convert()
    {
        $moisdate = $jourdate = $anneedate = null;
        $moisdate=strftime(date("m"));
        if ($moisdate<10)
        {
            $moisdate="0".strftime(date("m"));
        }
        $jourdate=strftime(date("d"));
        if ($jourdate<10)
        {
            $jourdate="0".$day[strftime(date("d"))];
        }
        $anneedate=strftime(date("Y"));
        $function_ret=$jourdate."/".$moisdate."/".$anneedate;
        return $function_ret;
    }    
    $currentlogin = !empty($_SESSION['login']) ? $_SESSION['login'] : '';
    $fdate=$jour."/".$mois."/".$annee;
    $recul = !empty($_REQUEST["recul"]) ? $_REQUEST["recul"] : '';
    if ($recul=="mois")
    {
        $ddate="01/".$mois."/".$annee;
    } else {
        if ($recul=="")
        {
            $recul="90";
            $ddate=time()-90;
        } else {
            $ddate=convert(time()-$recul);
        } 
    } 
    $pays="france";
    $webmaster = !empty($_SESSION['id']) ? $_SESSION['id'] : '';
    $login = !empty($_SESSION['login']) ? $_SESSION['login']: '';
    $s_demo = !empty($_SESSION['demo']) ? $_SESSION['demo']: '';
    if ($s_demo=="1")
    {
        $id="605";
    } else {
        $id = !empty($_SESSION['id']) ? $_SESSION['id'] : '';
        if ($id=="")
        {
            header("Location: {$assets->domain}/membres.php"); 
        } 
    } 
    /*
    Dim oConn
    Set oConn = Server.CreateObject("ADODB.Connection")
    oConn.Open konn_af_grenoble
    */
?>
<html>
    <head>
        <title><?php echo _t("Rencontre Affiliation"); ?></title>
        <META http-equiv=Page-Enter content=blendTrans(Duration=1.0)>
        <STYLE TYPE="text/css"> A {color:#ffffff;} A:hover {color:#cccccc} </STYLE>

        <SCRIPT LANGUAGE="javascript"><!--
            function ShowTooltip(fArg)
            {
                var tooltipOBJ = eval("document.all['tt" + fArg + "']");
                var tooltipOffsetTop = tooltipOBJ.scrollHeight + 15;
                var testTop = (document.body.scrollTop + event.clientY) - tooltipOffsetTop;
                var testLeft = event.clientX - 250;
                var tooltipAbsLft = (testLeft < 0) ? 160 : testLeft;
                var tooltipAbsTop = (testTop < document.body.scrollTop) ? document.body.scrollTop + 10 : testTop;
                tooltipOBJ.style.posLeft = tooltipAbsLft;
                tooltipOBJ.style.posTop = tooltipAbsTop;
                tooltipOBJ.style.visibility = "visible";
            }
            function HideTooltip(fArg)
            {
                var tooltipOBJ = eval("document.all['tt" + fArg + "']");
                tooltipOBJ.style.visibility = "hidden";
            }
        //--></SCRIPT>
        <link href="affiliation.css" rel="stylesheet" type="text/css">
    </head>

    <body>
        <table width="80%" border="0" cellspacing="0" cellpadding="0" align="center">
            <tr> 
                <td class="texte1"> 
                    <?php 
                        $s_demo = !empty($_SESSION['demo']) ? $_SESSION['demo']: '';
                        if($s_demo=="1")
                        {
                        ?> 
                        <div align="center"> 
                            <table border="1" cellspacing="0" cellpadding="0" align="center" bordercolor="#CCCCCC" width="510">
                                <tr bgcolor="#8020a0"> 
                                    <td background="<?php echo $assets->baseUrl;?>/images/background2.gif" bgcolor="#8020a0"> 
                                        <div align="left" class="titre">&nbsp;<b><?php echo _t("Espace Démo"); ?> </b></div>
                                    </td>
                                </tr>
                            </table>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" height="3">
                                <tr>
                                    <td></td>
                                </tr>
                            </table>
                        <?php }?>
                    </div>
                    <table width="510" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#CCCCCC">
                        <tr> 
                            <td width="131" background="<?php echo $assets->baseUrl;?>/images/background2.gif" bgcolor="#8020a0"> 
                                <div align="center">
                                    <?php 
                                        if($s_demo=="1")
                                        {
                                        ?>
                                        <a href="#" class="navigation" onMouseOver="ShowTooltip(1);" onMouseOut="HideTooltip(1);" ><?php echo _t("Vos Coordonnées"); ?></a></div>
                                    <div class=reminderTooltip id=tt1 style="WIDTH: 160px;"><?php echo _t("Indisponible en Espace Demo !"); ?></div>
                                    <?php 
                                    } else {
                                    ?>
                                    <a href="coordonees.php" class="navigation"><?php echo _t("Vos Coordonées"); ?></a></div>
                                    <?php 
                                    } 
                                ?>
                            </td>
                            <td width="115"> 
                                <div align="center" class="texte1"><i><?php echo _t("Vos Statistiques"); ?></i></div>
                            </td>
                            <td width="153" background="<?php echo $assets->baseUrl;?>/images/background2.gif" bgcolor="#8020a0"> 
                                <div align="center"><a href="bandeaux" class="navigation"><?php echo _t("Bandeaux &amp; Outils"); ?></a></div>
                            </td>
                            <td width="97" background="<?php echo $assets->baseUrl;?>/images/background2.gif" bgcolor="#8020a0"> 
                                <div align="center">
                                    <div align="center"> 
                                        <?php

                                            if($s_demo=="1") {

                                            ?>
                                            <a href="#" class="navigation" onMouseOver="ShowTooltip(2);" onMouseOut="HideTooltip(2);" ><?php echo _t("Facturation"); ?></a></div>
                                        <div class=reminderTooltip id=tt2 style="WIDTH: 160px;"><?php echo _t("Indisponible en Espace Demo !"); ?></div>
                                        <?php 
                                        } else {
                                        ?>
                                        <a href="../appel_facture" class="navigation"><?php echo _t("Facturation"); ?></a> 
                                        <?php 
                                        } 
                                    ?>
                                </div>
                            </td>
                        </tr>
                    </table>

                    <table width="100%" border="0" cellspacing="0" cellpadding="0" height="3">
                        <tr>
                            <td></td>
                        </tr>
                    </table>
                    <?php 
                        $n = !empty($_REQUEST['n']) ? $_REQUEST['n']: '';
                        if ($n=="")
                        {
                            $n="90";
                        }
                        switch ($n)
                        {
                            case "racine":
                            ?>
                            <table border="1" cellspacing="0" cellpadding="0" align="center" bordercolor="#CCCCCC" width="510">
                                <tr> 
                                    <td width="36" background="<?php echo $assets->baseUrl;?>/images/background2.gif" bgcolor="#8020a0"><div align="center" class="navigation"><a href="?recul=150&n=150">150</a></div></td>
                                    <td width="37" background="<?php echo $assets->baseUrl;?>/images/background2.gif" bgcolor="#8020a0"><div align="center" class="navigation"><a href="?recul=120&n=120">120</a></div></td>
                                    <td width="29" background="<?php echo $assets->baseUrl;?>/images/background2.gif" bgcolor="#8020a0"> <div align="center" class="navigation"><a href="?recul=90&n=90">90</a></div></td>
                                    <td width="31" height="22" background="<?php echo $assets->baseUrl;?>images/background2.gif" bgcolor="#8020a0"> <div align="center" class="navigation"><a href="?recul=60&n=60">60</a></div></td>
                                    <td width="136" height="22" background="<?php echo $assets->baseUrl;?>/images/background2.gif" bgcolor="#8020a0"> <div align="center" class="navigation"><a href="?recul=15&n=15">15 
                                                <?php echo _t("DerniersJours"); ?></a></div></td>
                                    <td width="81" height="22" background="<?php echo $assets->baseUrl;?>/images/background2.gif" bgcolor="#8020a0"> <div align="center" class="navigation"><a href="?recul=365&n=annuelles"><?php echo _t("Annuelles"); ?></a></div></td>
                                    <td width="74" height="22" background="<?php echo $assets->baseUrl;?>/images/background2.gif" bgcolor="#8020a0"> <div align="center" class="navigation"><a href="?n=analyses"><?php echo _t("Analyses"); ?></a></div></td>
                                    <td width="64" height="22"> 
                                        <div align="center" class="texte1"><em><?php echo _t("Du Mois"); ?></em> </div></td>
                                </tr>
                            </table>
                            <?php 
                                break;
                            case "analyses":
                            ?>
                            <table border="1" cellspacing="0" cellpadding="0" align="center" bordercolor="#CCCCCC" width="510">
                                <tr bgcolor="#8020a0"> 
                                    <td width="36" background="<?php echo $assets->baseUrl;?>/images/background2.gif" bgcolor="#8020a0"><div align="center" class="navigation"><a href="?recul=150&n=150">150</a></div></td>
                                    <td width="37" background="<?php echo $assets->baseUrl;?>/images/background2.gif" bgcolor="#8020a0"><div align="center" class="navigation"><a href="?recul=120&n=120">120</a></div></td>
                                    <td width="29" background="<?php echo $assets->baseUrl;?>/images/background2.gif" bgcolor="#8020a0"> <div align="center" class="navigation"><a href="?recul=90&n=90">90</a></div></td>
                                    <td width="31" height="22" background="<?php echo $assets->baseUrl;?>/images/background2.gif" bgcolor="#8020a0"> <div align="center" class="navigation"><a href="?recul=60&n=60">60</a></div></td>
                                    <td width="136" height="22" background="<?php echo $assets->baseUrl;?>/images/background2.gif" bgcolor="#8020a0"> <div align="center" class="navigation"><a href="?recul=15&n=15">15 
                                                <?php echo _t("DerniersJours"); ?></a></div></td>
                                    <td width="81" height="22" background="<?php echo $assets->baseUrl;?>/images/background2.gif" bgcolor="#8020a0"> <div align="center" class="navigation"><a href="?recul=365&n=annuelles"><?php echo _t("Annuelles"); ?></a></div></td>
                                    <td width="74" height="22" bgcolor="#FFFFFF"> 
                                        <div align="center" class="navigation"><em><?php echo _t("Analyses"); ?></em></div></td>
                                    <td width="64" height="22" background="<?php echo $assets->baseUrl;?>/images/background2.gif" bgcolor="#8020a0"> <div align="center" class="texte1"><a href="?n=mois&recul=mois"><?php echo _t("Du Mois"); ?> </a></div></td>
                                </tr>
                            </table>
                            <?php 
                                break;
                            case "annuelles":
                            ?>
                            <table border="1" cellspacing="0" cellpadding="0" align="center" bordercolor="#CCCCCC" width="510">
                                <tr bgcolor="#8020a0"> 
                                    <td width="36" background="<?php echo $assets->baseUrl;?>/images/background2.gif" bgcolor="#8020a0"><div align="center" class="navigation"><a href="?recul=150&n=150">150</a></div></td>
                                    <td width="37" background="<?php echo $assets->baseUrl;?>/images/background2.gif" bgcolor="#8020a0"><div align="center" class="navigation"><a href="?recul=120&n=120">120</a></div></td>
                                    <td width="29" background="<?php echo $assets->baseUrl;?>/images/background2.gif"> <div align="center" class="navigation"><a href="?recul=90&n=90">90</a></div></td>
                                    <td width="31" height="22" background="<?php echo $assets->baseUrl;?>/images/background2.gif"> <div align="center" class="navigation"><a href="?recul=60&n=60">60</a></div></td>
                                    <td width="136" height="22" background="<?php echo $assets->baseUrl;?>/images/background2.gif"> <div align="center" class="navigation"><a href="?recul=15&n=15">15 
                                                <?php echo _t("DerniersJours"); ?></a></div></td>
                                    <td width="81" height="22" bgcolor="#FFFFFF"> 
                                        <div align="center" class="navigation"><em><?php echo _t("Annuelles"); ?></em></div></td>
                                    <td width="74" height="22" background="<?php echo $assets->baseUrl;?>/images/background2.gif"> <div align="center" class="navigation"><a href="?n=analyses"><?php echo _t("Analyses"); ?></a></div></td>
                                    <td width="64" height="22" background="<?php echo $assets->baseUrl;?>/images/background2.gif"> <div align="center" class="texte1"><a href="?n=mois&recul=mois"><?php echo _t("Du Mois"); ?> </a></div></td>
                                </tr>
                            </table>
                            <?php 
                                break;
                            case "15":
                            ?>
                            <table border="1" cellspacing="0" cellpadding="0" align="center" bordercolor="#CCCCCC" width="510">
                                <tr bgcolor="#8020a0"> 
                                    <td width="36" background="<?php echo $assets->baseUrl;?>/images/background2.gif" bgcolor="#8020a0"><div align="center" class="navigation"><a href="?recul=150&n=150">150</a></div></td>
                                    <td width="37" background="<?php echo $assets->baseUrl;?>/images/background2.gif" bgcolor="#8020a0"><div align="center" class="navigation"><a href="?recul=120&n=120">120</a></div></td>
                                    <td width="29" background="<?php echo $assets->baseUrl;?>/images/background2.gif" > <div align="center" class="navigation"><a href="?recul=90&n=90">90</a></div></td>
                                    <td width="31" height="22" background="<?php echo $assets->baseUrl;?>/images/background2.gif" > <div align="center" class="navigation"><a href="?recul=60&n=60">60</a></div></td>
                                    <td width="136" height="22" bgcolor="#FFFFFF"> 
                                        <div align="center" class="navigation"><em>15 <?php echo _t("DerniersJours"); ?></em></div></td>
                                    <td width="81" height="22" background="<?php echo $assets->baseUrl;?>/images/background2.gif" > <div align="center" class="navigation"><a href="?recul=365&n=annuelles"><?php echo _t("Annuelles"); ?></a></div></td>
                                    <td width="74" height="22" background="<?php echo $assets->baseUrl;?>/images/background2.gif" > <div align="center" class="navigation"><a href="?n=analyses"><?php echo _t("Analyses"); ?></a></div></td>
                                    <td width="64" height="22" background="<?php echo $assets->baseUrl;?>/images/background2.gif" > <div align="center" class="texte1"><a href="?n=mois&recul=mois"><?php echo _t("Du Mois"); ?> </a></div></td>
                                </tr>
                            </table>
                            <?php 
                                break;
                            case "60":
                            ?>
                            <table border="1" cellspacing="0" cellpadding="0" align="center" bordercolor="#CCCCCC" width="510">
                                <tr bgcolor="#8020a0"> 
                                    <td width="36" bgcolor="#8020a0" background="<?php echo $assets->baseUrl;?>/images/background2.gif" ><div align="center" class="navigation"><a href="?recul=150&n=150">150</a></div></td>
                                    <td width="37" bgcolor="#8020a0" background="<?php echo $assets->baseUrl;?>/images/background2.gif" ><div align="center" class="navigation"><a href="?recul=120&n=120">120</a></div></td>
                                    <td width="29" background="<?php echo $assets->baseUrl;?>/images/background2.gif" > <div align="center" class="navigation"><a href="?recul=90&n=90">90</a></div></td>
                                    <td width="31" height="22" bgcolor="#FFFFFF"> 
                                        <div align="center" class="navigation"><em>60</em></div></td>
                                    <td width="136" height="22" background="<?php echo $assets->baseUrl;?>/images/background2.gif" > <div align="center" class="navigation"><a href="?recul=15&n=15">15 
                                                <?php echo _t("DerniersJours"); ?></a></div></td>
                                    <td width="81" height="22" background="<?php echo $assets->baseUrl;?>/images/background2.gif" > <div align="center" class="navigation"><a href="?recul=365&n=annuelles"><?php echo _t("Annuelles"); ?></a></div></td>
                                    <td width="74" height="22" background="<?php echo $assets->baseUrl;?>/images/background2.gif" > <div align="center" class="navigation"><a href="?n=analyses"><?php echo _t("Analyses"); ?></a></div></td>
                                    <td width="64" height="22" background="<?php echo $assets->baseUrl;?>/images/background2.gif" > <div align="center" class="texte1"><a href="?n=mois&recul=mois"><?php echo _t("Du Mois"); ?> </a></div></td>
                                </tr>
                            </table>
                            <?php 
                                break;
                            case "90":
                            ?>
                            <table border="1" cellspacing="0" cellpadding="0" align="center" bordercolor="#CCCCCC" width="510">
                                <tr bgcolor="#8020a0"> 
                                    <td width="36" bgcolor="#8020a0" background="<?php echo $assets->baseUrl;?>/images/background2.gif" ><div align="center" class="navigation"><a href="?recul=150&n=150">150</a></div></td>
                                    <td width="37" bgcolor="#8020a0" background="<?php echo $assets->baseUrl;?>/images/background2.gif" ><div align="center" class="navigation"><a href="?recul=120&n=120">120</a></div></td>
                                    <td width="29" bgcolor="#FFFFFF"> 
                                        <div align="center" class="navigation"><em>90</em></div></td>
                                    <td width="31" height="22" background="<?php echo $assets->baseUrl;?>/images/background2.gif" > <div align="center" class="navigation"><a href="?recul=60&n=60">60</a></div></td>
                                    <td width="136" height="22" background="<?php echo $assets->baseUrl;?>/images/background2.gif" > <div align="center" class="navigation"><a href="?recul=15&n=15">15 
                                                <?php echo _t("DerniersJours"); ?></a></div></td>
                                    <td width="81" height="22" background="<?php echo $assets->baseUrl;?>/images/background2.gif" > <div align="center" class="navigation"><a href="?recul=365&n=annuelles"><?php echo _t("Annuelles"); ?></a></div></td>
                                    <td width="74" height="22" background="<?php echo $assets->baseUrl;?>/images/background2.gif" > <div align="center" class="navigation"><a href="?n=analyses"><?php echo _t("Analyses"); ?></a></div></td>
                                    <td width="64" height="22" background="<?php echo $assets->baseUrl;?>/images/background2.gif" > <div align="center" class="texte1"><a href="?n=mois&recul=mois"><?php echo _t("Du Mois"); ?> </a></div></td>
                                </tr>
                            </table>
                            <?php 
                                break;
                            case "120":
                            ?>
                            <table border="1" cellspacing="0" cellpadding="0" align="center" bordercolor="#CCCCCC" width="510">
                                <tr bgcolor="#8020a0"> 
                                    <td width="36" bgcolor="#8020a0" background="<?php echo $assets->baseUrl;?>/images/background2.gif" ><div align="center" class="navigation"><a href="?recul=150&n=150">150</a></div></td>
                                    <td width="37" bgcolor="#FFFFFF">
                                        <div align="center" class="navigation"><em>120</em></div></td>
                                    <td width="29" background="<?php echo $assets->baseUrl;?>/images/background2.gif" > <div align="center" class="navigation"><a href="?recul=90&n=90">90</a></div></td>
                                    <td width="31" height="22" background="<?php echo $assets->baseUrl;?>/images/background2.gif" > <div align="center" class="navigation"><a href="?recul=60&n=60">60</a></div></td>
                                    <td width="136" height="22" background="<?php echo $assets->baseUrl;?>/images/background2.gif" > <div align="center" class="navigation"><a href="?recul=15&n=15">15 
                                                <?php echo _t("DerniersJours"); ?></a></div></td>
                                    <td width="81" height="22" background="<?php echo $assets->baseUrl;?>/images/background2.gif" > <div align="center" class="navigation"><a href="?recul=365&n=annuelles"><?php echo _t("Annuelles"); ?></a></div></td>
                                    <td width="74" height="22" background="<?php echo $assets->baseUrl;?>/images/background2.gif" > <div align="center" class="navigation"><a href="?n=analyses"><?php echo _t("Analyses"); ?></a></div></td>
                                    <td width="64" height="22" background="<?php echo $assets->baseUrl;?>/images/background2.gif" > <div align="center" class="texte1"><a href="?n=mois&recul=mois"><?php echo _t("Du Mois"); ?> </a></div></td>
                                </tr>
                            </table>
                            <?php 
                                break;
                            case "150":
                            ?>
                            <table border="1" cellspacing="0" cellpadding="0" align="center" bordercolor="#CCCCCC" width="510">
                                <tr bgcolor="#8020a0"> 
                                    <td width="36" bgcolor="#FFFFFF">
                                        <div align="center" class="navigation"><em>150</em></div></td>
                                    <td width="37" bgcolor="#8020a0" background="<?php echo $assets->baseUrl;?>/images/background2.gif" ><div align="center" class="navigation"><a href="?recul=120&n=120">120</a></div></td>
                                    <td width="29" background="<?php echo $assets->baseUrl;?>/images/background2.gif" > <div align="center" class="navigation"><a href="?recul=90&n=90">90</a></div></td>
                                    <td width="31" height="22" background="<?php echo $assets->baseUrl;?>/images/background2.gif" > <div align="center" class="navigation"><a href="?recul=60&n=60">60</a></div></td>
                                    <td width="136" height="22" background="<?php echo $assets->baseUrl;?>/images/background2.gif" > <div align="center" class="navigation"><a href="?recul=15&n=15">15 
                                                <?php echo _t("DerniersJours"); ?></a></div></td>
                                    <td width="81" height="22" background="<?php echo $assets->baseUrl;?>/images/background2.gif" > <div align="center" class="navigation"><a href="?recul=365&n=annuelles"><?php echo _t("Annuelles"); ?></a></div></td>
                                    <td width="74" height="22" background="<?php echo $assets->baseUrl;?>/images/background2.gif" > <div align="center" class="navigation"><a href="?n=analyses"><?php echo _t("Analyses"); ?></a></div></td>
                                    <td width="64" height="22" background="<?php echo $assets->baseUrl;?>/images/background2.gif"> <div align="center" class="texte1"><a href="?n=mois&recul=mois"><?php echo _t("Du Mois"); ?> </a></div></td>
                                </tr>
                            </table>
                            <?php 
                                break;
                            default:

                            ?>
                            <table border="1" cellspacing="0" cellpadding="0" align="center" bordercolor="#CCCCCC" width="510">
                                <tr bgcolor="#8020a0"> 
                                    <td width="36" background="<?php echo $assets->baseUrl;?>/images/background2.gif" bgcolor="#8020a0"><div align="center" class="navigation"><a href="?recul=150&n=150">150</a></div></td>
                                    <td width="37" background="<?php echo $assets->baseUrl;?>/images/background2.gif" bgcolor="#8020a0"><div align="center" class="navigation"><a href="?recul=120&n=120">120</a></div></td>
                                    <td width="29" background="<?php echo $assets->baseUrl;?>/images/background2.gif"> <div align="center" class="navigation"><a href="?recul=90&n=90">90</a></div></td>
                                    <td width="31" height="22" background="<?php echo $assets->baseUrl;?>/images/background2.gif"> <div align="center" class="navigation"><a href="?recul=60&n=60">60</a></div></td>
                                    <td width="136" height="22" background="<?php echo $assets->baseUrl;?>/images/background2.gif"> <div align="center" class="navigation"><a href="?recul=15&n=15">15 
                                                <?php echo _t("DerniersJours"); ?></a></div></td>
                                    <td width="81" height="22" background="<?php echo $assets->baseUrl;?>/images/background2.gif"> <div align="center" class="navigation"><a href="?recul=365&n=annuelles"><?php echo _t("Annuelles"); ?></a></div></td>
                                    <td width="74" height="22" background="<?php echo $assets->baseUrl;?>/images/background2.gif"> <div align="center" class="navigation"><a href="?n=analyses"><?php echo _t("Analyses"); ?></a></div></td>
                                    <td width="64" height="22" bgcolor="#FFFFFF"> 
                                        <div align="center" class="texte1"><em><?php echo _t("Du Mois"); ?></em> </div></td>
                                </tr>
                            </table>
                            <?php 
                                break;
                        } 
                    ?>

                    <div align="center">
                        <?php 
                            if($n=="analyses")
                            {
                            ?>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" height="3">
                                <tr>
                                    <td></td>
                                </tr>
                            </table>
                            <table width="510" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#CCCCCC">
                                <tr background="<?php echo $assets->baseUrl;?>/images/background2.gif"> 
                                    <td height="15" colspan="5" bgcolor="#8020a0" class="texte1"><font color="#FFFFFF">&nbsp;<?php echo _t("Taux de transformation = Une inscription pour x Clics (Sessions unique)"); ?></font></td>
                                </tr>
                                <tr class="texte1"> 
                                    <td width="27%" height="25"> 
                                        <div align="center"><?php echo _t("Nb de Clics"); ?></div>
                                    </td>
                                    <td width="28%" height="25"> 
                                        <div align="center"><?php echo _t("Nb Inscriptions"); ?></div>
                                    </td>
                                    <td width="45%" height="25" colspan="3"> 
                                        <div align="center"><?php echo _t("Transformation moyenne"); ?></div>
                                    </td>
                                </tr>
                                <tr class="texte1"> 
                                    <td width="27%"> 
                                        <div align="center"> 
                                            <?php
    
		$result = query("CALL aff_total_clics(?,?,?)",array(
			array('value'=>$id,"type"=>PDO::PARAM_STR,"length"=>300),
			array('value'=>$ddate,"type"=>PDO::PARAM_STR,"length"=>300),
		    array('value'=>$fdate,"type"=>PDO::PARAM_STR,"length"=>300)
		),false);
    
    
                                                if(!empty($result))
                                                {
                                                ?> 0
                                                <?php 
                                                } else {
                                                    $total_clics=(0);
                                                    echo (0); 
                                                } 
                                            ?>
                                        </div>
                                    </td>
                                    <td width="28%"> 
                                        <div align="center"> 
                                            <?php 
                                                
		$result = query("CALL aff_total_inscriptions(?,?,?)",array(
			array('value'=>$id,"type"=>PDO::PARAM_STR,"length"=>300),
			array('value'=>$ddate,"type"=>PDO::PARAM_STR,"length"=>300),
		    array('value'=>$fdate,"type"=>PDO::PARAM_STR,"length"=>300)
		),false);
                                                
                                                if(!empty($result))
                                                {
                                                ?> 0
                                                <?php 
                                                } else {
                                                    $total_inscriptions=(0);
                                                    if (!isset($total_inscriptions))
                                                    {
                                                        $total_inscriptions=1;
                                                    }      
                                                    echo $total_inscriptions;
                                                } 
                                            ?>
                                        </div>
                                    </td>
                                    <td colspan="3"> 
                                        <div align="center"><?php echo _t("Une inscription"); ?> / <?php echo number_format($total_clics/$total_inscriptions,2);?> <?php echo _t("Clics"); ?></div>
                                    </td>
                                </tr>

                                <tr background="<?php echo $assets->baseUrl;?>/images/background2.gif"> 
                                    <td height="15" colspan="5" bgcolor="#8020a0" class="texte1">
                                        <div align="center"><font color="#FFFFFF"><?php echo _t("Elargir sur"); ?> // <a href="?n=analyses&recul=30"><?php echo _t("Dernier mois"); ?></a> / <a href="?n=analyses&recul=365"><?php echo _t("Année"); ?></a></font></div>
                                    </td>
                                </tr>
                            </table>
                            <div align="center"><br>
                                <img src="analyses.php?id=<?php echo $id;?>&ddate=<?php echo $ddate;?>&fdate=<?php echo $fdate;?>&recul=<?php echo $recul;?>&transformation=1"><br>
                            </div>
                            <?php 
                            } else {
                            ?>      
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" height="3">
                                <tr>
                                    <td></td>
                                </tr>
                            </table>
                            <table width="510" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#CCCCCC">
                                <tr background="<?php echo $assets->baseUrl;?>/images/background2.gif"> 
                                    <td height="15" colspan="5" bgcolor="#8020a0" class="texte1" background="<?php echo $assets->baseUrl;?>/images/background2.gif" ><font color="#FFFFFF">&nbsp;<?php echo _t("Total de vos gains pour la période du"); ?> <?php echo $ddate;?> <?php echo _t("au"); ?> <?php echo $fdate;?></font></td>
                                </tr>
                                <tr class="texte1"> 
                                    <td width="33%" height="25"> 
                                        <div align="center"><?php echo _t("Nb de Clics"); ?></div>
                                    </td>
                                    <td width="33%" height="25"> 
                                        <div align="center"><?php echo _t("Nb Inscriptions"); ?></div>
                                    </td>
                                    <td height="25" colspan="3"> 
                                        <div align="center"><?php echo _t("Gains effectif HT"); ?></div>
                                    </td>
                                </tr>
                                <tr class="texte1"> 
                                    <td width="33%"> 
                                        <div align="center"> 
                                            <?php
												$result = query("SELECT aff_total_clics(?,?,?)",array(
													array('value'=>$id, "type"=>PDO::PARAM_STR, "length"=>50),
													array('value'=>$ddate, "type"=>PDO::PARAM_STR, "length"=>50),
													array('value'=>$fdate, "type"=>PDO::PARAM_STR, "length"=>50)
												),false);												
                                                
                                                if(!empty($result))
                                                { 
                                                ?>
                                                0
                                                <?php 
                                                } else { 
                                                    echo (0);
                                                } 
                                            ?>
                                        </div>
                                    </td>
                                    <td width="33%"> 
                                        <div align="center"> 
                                            <?php 
												$result = query("SELECT aff_total_inscriptions(?,?,?)",array(
													array('value'=>$id, "type"=>PDO::PARAM_STR, "length"=>50),
													array('value'=>$ddate, "type"=>PDO::PARAM_STR, "length"=>50),
													array('value'=>$fdate, "type"=>PDO::PARAM_STR, "length"=>50)
												),false);
												
                                                if(!empty($result))
                                                { 
                                                    $total_inscriptions="0";
                                                ?>
                                                0
                                                <?php 
                                                } else {
                                                    $total_inscriptions=(0);
                                                    if (!isset($total_inscriptions))
                                                    {
                                                        $total_inscriptions=1;
                                                    }
                                                    echo $total_inscriptions;
                                                } 
                                            ?>
                                        </div>
                                    </td>
                                    <td colspan="3"> 
                                        <div align="center"> 
                                            <?php echo number_format($total_inscriptions,2);?> &euro; 
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            <div align="center"><br>
                            </div>
                            <div align="center"></div>

                            <img src="<?php echo $assets->baseUrl;?>/graphs.php?id=<?php echo $id;?>&clics=0&inscriptions=1&hits=0&ddate=<?php echo $ddate;?>&fdate=<?php echo $fdate;?>&recul=<?php echo $recul;?>"><br><br>
                            <img src="<?php echo $assets->baseUrl;?>/graphs.php?id=<?php echo $id;?>&clics=1&inscriptions=1&hits=1&ddate=<?php echo $ddate;?>&fdate=<?php echo $fdate;?>&recul=<?php echo $recul;?>"><br>
                            <?php 
                            }
                        ?>
                    </div>
                </td>
            </tr>
        </table>
        <div align="center"><span class="copyright"><font color="#000000"><?php echo _t("© All Rights Reserved - Tous Droits Réservés"); ?> / <?php echo $id;?></font> </span> </div>
    </body>
</html>

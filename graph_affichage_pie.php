<?php // content="text/plain; charset=utf-8"
session_start();
require_once ('jpgraph/jpgraph.php');
require_once ('jpgraph/jpgraph_pie.php');
//rand color
function rand_color() { 
    $str = '#'; 
    for($i = 0 ; $i < 6 ; $i++) { 
        $randNum = rand(0 , 15); 
        switch ($randNum) { 
            case 10: $randNum = 'A'; break; 
            case 11: $randNum = 'B'; break; 
            case 12: $randNum = 'C'; break; 
            case 13: $randNum = 'D'; break; 
            case 14: $randNum = 'E'; break; 
            case 15: $randNum = 'F'; break; 
        } 
        $str .= $randNum; 
    } 
    return $str; 
} 
//data
$s = !empty($_REQUEST["s"])?$_REQUEST["s"]:"";
$Title = "";
$gr_data = array();
if (empty($s)){
  print "pour le mode debugg  ajouter \"&debugg=1\"<br>";
  print "<a href=\"?s=exemple1\">exemple1</a><br>";
  print "<a href=\"?s=exemple2\">exemple2</a><br>";
  print "<a href=\"?s=exemple3\">exemple3</a><br>";
  print "<a href=\"?s=exemple4\">exemple4</a><br>";
  print "<a href=\"?s=exemple5\">exemple5</a><br>";
  print "<a href=\"?s=exemple6\">exemple6</a><br>";
  print "<a href=\"?s=exemple7\">exemple7</a><br>";
  exit();
}
$debugg = !empty($_REQUEST["debugg"])?$_REQUEST["debugg"]:"";
if (!empty($debugg)){
  header("Content-type: "."Image/Gif"); 
}
switch ($s) {
    case 'exemple1':
		$gr_data = array(
			'title' => 'pie',
			'data' => array(40,21,17,14,23),
			'legends' => array('1','2','3','4','5')
		);
        break;
    case 'exemple2':
		$gr_data = array(
			'title' => 'pie',
			'data' => array(40,21,17,14,23),
			'legends' => array('1','2','3','4','5')
		);
        break;
    case 'exemple3':
		$gr_data = array(
			'title' => 'pie',
			'data' => array(40,21,17,14,23),
			'legends' => array('1','2','3','4','5')
		);$gr_data = array(
			'title' => 'pie',
			'data' => array(40,21,17,14,23),
			'legends' => array('1','2','3','4','5')
		);
        break;
	case 'exemple5':
		$gr_data = array(
			'title' => 'pie',
			'data' => array(40,21,17,14,23),
			'legends' => array('1','2','3','4','5')
		);
        break;
	case 'exemple6':
		$gr_data = array(
			'title' => 'pie',
			'data' => array(40,21,17,14,23),
			'legends' => array('1','2','3','4','5')
		);
        break;
    default:
	 break;
}


$gr_data = !empty($_SESSION[" ".$s])?$_SESSION[" ".$s]:"";
//var_dump($gr_data);

if(empty($gr_data)){
	$gr_data = array(
		'title' => 'pie',
		'data' => array(40,21,17,14,23),
		'legends' => array('1','2','3','4','5')
	);
}

// Create the Pie Graph. 
$graph = new PieGraph(450,350);
$graph->img->SetTransparent("white"); 

$graph->legend->SetPos(0,0,'right','top');
$graph->legend->SetColumns(1);
 $graph->legend->SetFillColor("gray");

$theme_class="DefaultTheme";
//$graph->SetTheme(new $theme_class());
if(!empty($gr_data)){
	// color
	$lacouleur = array("#990066","#000099","#99CC99","#FFCCFF","#9999FF","#339999","#33CC00","#CC6666","#990099","3399CC","CCFFCC","CC9933","#FF0066","#3333FF","#00FF99","#CCFF00","#CC9966",);
	$colors = array();
	//$i=0;
	foreach($gr_data['data'][1]['data'] as $item){
		array_push($colors,rand_color());
		//array_push($colors,$lacouleur[$i]);
		//$i++;
	}
	// Set A title for the plot
	$graph->title->Set($gr_data['data'][1]['title']);
	$graph->SetBox(true);

	// Create
	$p1 = new PiePlot($gr_data['data'][1]['data']);
	$graph->Add($p1);

	$p1->ShowBorder();
	$p1->SetColor('black');
	$p1->SetSliceColors($colors);
	$p1->SetLegends($gr_data['data'][0]['data']);
}
$graph->Stroke();

?>

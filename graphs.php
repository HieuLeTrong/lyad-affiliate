<?php
  session_start();
  require(dirname(__FILE__). '/global_conn.php');
  require (dirname(__FILE__). '/jpgraph/jpgraph.php');
  require (dirname(__FILE__). '/jpgraph/jpgraph_line.php');
?>
<?php
//http://local.lyad_app.com/graphs.php?id=5&clics=0&inscriptions=0&hits=0&ddate=23/12/2013&fdate=23/01/2014&recul=30
//cette page ne sert plus a rien sur la nouvelle version de l'affiliation 11/03/2006 09:49
//header("Location: "."http://affiliation.lyad.com");
//Err.Raise 6,"erreur_volontaire","verif si page demandé 11/03/2006 09:49"
//exit();

header("Expires: " . gmdate("D, d M Y H:i:s", time() + (0*60)) . " GMT"); ob_start();
ob_clean();


$id=!empty($_SESSION["id"])?$_SESSION["id"]:"";
if (!empty($_REQUEST["id"])){
	if($_REQUEST["id"] == '1267'){
		$id= '1267';
	}
} 
//var_dump($_REQUEST);
$ddate=!empty($_REQUEST["ddate"])?$_REQUEST["ddate"]:"";
$fdate=!empty($_REQUEST["fdate"])?$_REQUEST["fdate"]:"";
//var_dump($ddate);
//var_dump($fdate);
 if(!empty($ddate) && !empty($fdate)){
	$date1 = explode("/",$ddate);
	$date2 = explode("/",$fdate);
	$date2=mktime(0,0,0,$date2[1],$date2[0],$date2[2]);
	$date1=mktime(0,0,0,$date1[1],$date1[0],$date1[2]);
	$d=$date2 - $date1;
	$xdatediff = (int)abs(floor($d/( 365*24*10)));
	//$wcount = $xdatediff;
 }else{
	// $wcount=0;
 }
if (empty($_REQUEST["recul"])){
  $wcount=30;
}
if ($xdatediff>120){
  // $Chart->LineWidth=1;
}else{
  // $Chart->LineWidth=2;
} 

$arr_hits = array();
$arr_clics = array();
$arr_inscriptions = array();
$arr_ventes = array();

$hits = !empty($_REQUEST['hits'])?$_REQUEST['hits']:0;
if ($hits=="1"){
	///**** Hits ****/'	
	$rs_hits = query("call aff_select_hits(?,?,?)",array(
		array('value'=>$id, "type"=>PDO::PARAM_STR, "length"=>5000),
		array('value'=>$ddate, "type"=>PDO::PARAM_STR, "length"=>20),
		array('value'=>$fdate, "type"=>PDO::PARAM_STR, "length"=>20)		
	),false);
	if(!empty($rs_hits)){
		foreach($rs_hits as $item){
			array_push($arr_hits,$item['ping']);
		}
	}
} 

$clics = !empty($_REQUEST['clics'])?$_REQUEST['clics']:0;
if ($clics=="1"){
	///**** clics ****/'	
	$rs_clics = query("call aff_select_clics(?,?,?)",array(
		array('value'=>$id, "type"=>PDO::PARAM_STR, "length"=>5000),
		array('value'=>$ddate, "type"=>PDO::PARAM_STR, "length"=>20),
		array('value'=>$fdate, "type"=>PDO::PARAM_STR, "length"=>20)		
	),false);
	if(!empty($rs_clics)){
		foreach($rs_clics as $item){
			array_push($arr_clics,$item['ping']);
		}
	}
} 

$inscriptions = !empty($_REQUEST['inscriptions'])?$_REQUEST['inscriptions']:0;
if ($inscriptions=="1"){
	///**** Inscriptions ****/'
	$rs_inscriptions = query("call aff_select_inscriptions(?,?,?)",array(
		array('value'=>$id, "type"=>PDO::PARAM_STR, "length"=>5000),
		array('value'=>$ddate, "type"=>PDO::PARAM_STR, "length"=>20),
		array('value'=>$fdate, "type"=>PDO::PARAM_STR, "length"=>20)		
	),false);
	if(!empty($rs_inscriptions)){
		foreach($rs_inscriptions as $item){
			array_push($arr_inscriptions,$item['ping']);
		}
	}
} 

$ventes = !empty($_REQUEST['ventes'])?$_REQUEST['ventes']:0;
if ($ventes=="1"){
	///**** Ventes ****/'
	$rs_ventes = query("call aff_select_ventes(?,?,?)",array(
		array('value'=>$id, "type"=>PDO::PARAM_STR, "length"=>5000),
		array('value'=>$ddate, "type"=>PDO::PARAM_STR, "length"=>20),
		array('value'=>$fdate, "type"=>PDO::PARAM_STR, "length"=>20)		
	),false);
	if(!empty($rs_ventes)){
		foreach($rs_ventes as $item){
			array_push($arr_ventes,$item['ping']);
		}
	}
} 
$Title = "Votre audience ces ".$xdatediff." derniers jours / du ".$ddate." et ".$fdate;
// Setup the graph
$graph = new Graph(520,230);
if(empty($rs_hits) && empty($rs_clics) && empty($rs_inscriptions) && empty($rs_ventes)){
	$graph->SetScale('textlin',0,200,-1,100);
}else{
	$graph->SetScale("textlin");
}
		
$graph->img->SetAntiAliasing(false);
$graph->title->Set($Title);
$graph->title->SetColor("#000000");
$graph->title->SetFont(FF_FONT1); 
$graph->SetBox(false);

$graph->xaxis->SetTextLabelInterval(5);
$graph->ygrid->SetLineStyle("dotted");
$graph->ygrid->SetColor('#464637');

if(!empty($arr_hits)){
	//Create the hits line
	$p0 = new LinePlot($arr_hits);
	$graph->Add($p0);
	$p0->SetWeight(2); 
	$p0->SetColor("#999999");
	$p0->SetLegend("Hits");
	$p0->SetStyle("solid"); 
}
if(!empty($arr_clics)){
	//Create the clics line
	$p1 = new LinePlot($arr_clics);
	$graph->Add($p1);
	$p1->SetWeight(2); 
	$p1->SetColor("#8000a0");
	$p1->SetLegend("Clics");
	$p1->SetStyle("solid"); 
}
if(!empty($arr_inscriptions)){
	//Create the inscriptions line
	$p2 = new LinePlot($arr_inscriptions);
	$graph->Add($p2);
	$p2->SetWeight(2); 
	$p2->SetColor("#600080");
	$p2->SetLegend("Inscriptions");
	$p2->SetStyle("solid"); 
}
if(!empty($arr_ventes)){
	//Create the ventes line
	$p3 = new LinePlot($arr_ventes);
	$graph->Add($p3);
	$p3->SetWeight(2); 
	$p3->SetColor("#000000");
	$p3->SetLegend("Ventes");
	$p3->SetStyle("solid"); 
}
$graph->legend->SetFrameWeight(1);
// Output line
$graph->Stroke(); 



?>


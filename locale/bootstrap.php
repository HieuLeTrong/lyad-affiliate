<?php
include(dirname(__FILE__)."/functions.php");

$lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);

$defaultLanguage = !empty($lang) ? strtolower($lang) : "fr";


$config = array(
	'defaultLanguage'	=> $defaultLanguage,
	'defaultDomain'		=> 'affiliation',
	'availableDomains'	=> array(
		'affiliation','messages','common', 'default', 'pluralize'
	),
	'availableLanguages'	=> array('en', 'vi', 'fr'),
	'cookie'			=> array(
		'expire_at'		=> '+7 days',
		'path'			=> '/'
	)
);

$lang = $config['defaultLanguage'];

if (isset($_COOKIE['sitelang']) && in_array($_COOKIE['sitelang'], $config['availableLanguages'])) {
	$lang = $_COOKIE['sitelang'];
}


// define constants
define('PROJECT_DIR', realpath(dirname(__FILE__)) );
define('LOCALE_DIR', PROJECT_DIR );
define('DEFAULT_LOCALE', $defaultLanguage);

require_once(dirname(__FILE__).'/gettext.inc');

$supported_locales = $config;
$encoding = 'UTF-8';


// gettext setup
T_setlocale(LC_MESSAGES, $lang);
// Set the text domain as 'messages'
$domain = $config['defaultDomain'];

T_bindtextdomain($domain, LOCALE_DIR);
T_bind_textdomain_codeset($domain, $encoding);
T_textdomain($domain);
header('Content-Type: text/html; charset=utf-8');
?>
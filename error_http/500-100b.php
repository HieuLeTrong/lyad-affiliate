<?php
require(dirname(__FILE__). DIRECTORY_SEPARATOR .'..'. '/global_conn.php');
require(dirname(__FILE__). DIRECTORY_SEPARATOR .'..'. '/functions/common.php');

function sauv_base()
{
//ip = Request.ServerVariables("HTTP_X_FORWARDED_FOR") 'proxy transparent
//If ip="" or ip="unknown" Then
  $ip=$_SERVER["REMOTE_ADDR"];
//end if

  // $cmd is of type "ADODB.Command"

$result = null;
    try {
		$result = query("CALL p_insert_t_erreur_http(?,?,?,?,?,?,?,?)",array(
			array('value'=>$_SESSION['id_inscrit'], "type"=>PDO::PARAM_INT),
			array('value'=>$ip, "type"=>PDO::PARAM_STR, "length"=>20),
			array('value'=>$sUrl, "type"=>PDO::PARAM_STR, "length"=>200),
			array('value'=>'404-vitrine-v5', "type"=>PDO::PARAM_STR, "length"=>50),
			array('value'=>$_SERVER["HTTP_USER_AGENT"], "type"=>PDO::PARAM_STR, "length"=>1000),
			array('value'=>$_SERVER["HTTP_REFERER"], "type"=>PDO::PARAM_STR, "length"=>1000),
			array('value'=>$_SERVER["QUERY_STRING"], "type"=>PDO::PARAM_STR, "length"=>255),
			array('value'=>null, "type"=>PDO::PARAM_STR, "length"=>255)
		),false);
        if (!empty($result)) {
            
        }
    } catch (Exception $e) {
        throw new Exception($e->getMessage());
    }
} 
sauv_base();
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">

<html dir=ltr>

<head>
<style>
a:link			{font:8pt/11pt verdana; color:FF0000}
a:visited		{font:8pt/11pt verdana; color:#4e4e4e}
</style>

<META NAME="ROBOTS" CONTENT="NOINDEX">

<title>Impossible d'afficher la page</title>

<META HTTP-EQUIV="Content-Type" Content="text-html; charset=Windows-1252">
</head>

<script> 
function Homepage(){
<!--
// in real bits, urls get returned to our script like this:
// res://shdocvw.dll/http_404.htm#http://www.DocURL.com/bar.htm 

	//For testing use DocURL = "res://shdocvw.dll/http_404.htm#https://www.microsoft.com/bar.htm"
	DocURL=document.URL;
	
	//this is where the http or https will be, as found by searching for :// but skipping the res://
	protocolIndex=DocURL.indexOf("://",4);
	
	//this finds the ending slash for the domain server 
	serverIndex=DocURL.indexOf("/",protocolIndex + 3);

	//for the href, we need a valid URL to the domain. We search for the # symbol to find the begining 
	//of the true URL, and add 1 to skip it - this is the BeginURL value. We use serverIndex as the end marker.
	//urlresult=DocURL.substring(protocolIndex - 4,serverIndex);
	BeginURL=DocURL.indexOf("#",1) + 1;
	urlresult=DocURL.substring(BeginURL,serverIndex);
		
	//for display, we need to skip after http://, and go to the next slash
	displayresult=DocURL.substring(protocolIndex + 3 ,serverIndex);
	InsertElementAnchor(urlresult, displayresult);
}

function HtmlEncode(text)
{
    return text.replace(/&/g, '&amp').replace(/'/g, '&quot;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
}

function TagAttrib(name, value)
{
    return ' '+name+'="'+HtmlEncode(value)+'"';
}

function PrintTag(tagName, needCloseTag, attrib, inner){
    document.write( '<' + tagName + attrib + '>' + HtmlEncode(inner) );
    if (needCloseTag) document.write( '</' + tagName +'>' );
}

function URI(href)
{
    IEVer = window.navigator.appVersion;
    IEVer = IEVer.substr( IEVer.indexOf('MSIE') + 5, 3 );

    return (IEVer.charAt(1)=='.' && IEVer >= '5.5') ?
        encodeURI(href) :
        escape(href).replace(/%3A/g, ':').replace(/%3B/g, ';');
}

function InsertElementAnchor(href, text)
{
    PrintTag('A', true, TagAttrib('HREF', URI(href)), text);
}

//-->
</script>

<body bgcolor="FFFFFF">

<table width="410" cellpadding="3" cellspacing="5">

  <tr>    
    <td align="left" valign="middle" width="360">
	<h1 style="COLOR:000000; FONT: 13pt/15pt verdana"><!--Problem-->Impossible d'afficher la page :o(<br><br>
	Accèder a l'accueil de <a href="http://www.lyad.com" target="_top"><font size=3><b>WWW.LYAD.COM</b></font></a></h1>
    </td>
  </tr>
  
  <tr>
    <td width="400" colspan="2">
	<font style="COLOR:000000; FONT: 8pt/11pt verdana">Un problème est survenu dans la page que vous essayez d'ouvrir et vous ne pouvez donc pas afficher cette page.</font></td>
  </tr>
  
  <tr>
    <td width="400" colspan="2">
	<font style="COLOR:000000; FONT: 8pt/11pt verdana">

	<hr color="#C0C0C0" noshade>
	
    <p>Essayez les opérations suivantes :</p>

	<ul>
      <li id="instructionsText1">Cliquez sur le bouton 
      <a href="javascript:location.reload()">
      Actualiser</a> ou réessayez plus tard.<br>
      </li>
	  
      <li>Ouvrez la 
	  
	  <script>
	  <!--
	  if (!((window.navigator.userAgent.indexOf("MSIE") > 0) && (window.navigator.appVersion.charAt(0) == "2")))
	  {
	  	 Homepage();
	  }
	  //-->
	  </script>

	  page d'accueil, puis recherchez les liens vers les informations souhaitées. </li>
    </ul>
	
    <h2 style="font:8pt/11pt verdana; color:000000">HTTP 500.100 - Erreur interne au serveur - Erreur ASP<br>
    Services Internet (IIS)</h2>

	<hr color="#C0C0C0" noshade>
	
	<p>Informations techniques (destinées au personnel du Support technique)</p>

<ul>
<li>Type d'erreur :<br>
<?php echo $sMsgError ?>
</li>
<p>
<li>Type de navigateur :<br>
<?php echo htmlspecialchars($_SERVER["HTTP_USER_AGENT"]); ?>
</li>
<p>
<li>Page :<br>
<?php 
$strMethod = $_SERVER["REQUEST_METHOD"];
echo $strMethod." ";
if($strMethod =='POST'){
   echo  intval($_SERVER['CONTENT_LENGTH'])." bytes to ";
}
echo $_SERVER["SCRIPT_NAME"];
 $lngPos = strpos($_GET,"|");
 if($lngPos >1 ){
   echo  "?".htmlspecialchars(substr($_GET,0,($lngPos-1)));;
 }
 echo "</li>";
 if ($strMethod == "POST") {
  echo "<p><li>POST Data:<br>"; // WARNING: assuming strMethod is an external function
  if ( intval($_SERVER['CONTENT_LENGTH']) > $lngMaxFormBytes) {
    echo htmlspecialchars(substr($_POST,0,$lngMaxFormBytes))." . . ."; // WARNING: unable to convert Request assuming lngMaxFormBytes is an external function cannot convert request.form assuming lngMaxFormBytes is an external function
  }
  else {
    echo htmlspecialchars($_POST); // WARNING: cannot convert request.form
  }
  echo "</li>";
}
?>

<p>
<li>Heure :<br>
<?php 
$datNow = date('d-m-Y');
echo  htmlspecialchars(FormatDateTime(datNow(), 1).", ".FormatDateTime(datNow(), 3));

?>
<%
  datNow = Now()

  Response.Write Server.HTMLEncode(FormatDateTime(datNow, 1) & ", " & FormatDateTime(datNow, 3))
  on error resume next
	  Session.Codepage = bakCodepage 
  on error goto 0
%>
</li>
</p>
<p>
<li>Informations complémentaires :<br>
 <%  strQueryString = "prd=iis&sbp=&pver=5.0&ID=500;100&cat=" & Server.URLEncode(objASPError.Category) & _
    "&os=&over=&hrd=&Opt1=" & Server.URLEncode(objASPError.phpCode)  & "&Opt2=" & Server.URLEncode(objASPError.Number) & _
    "&Opt3=" & Server.URLEncode(objASPError.Description) 
       strURL = "http://www.microsoft.com/france/support" & _
    strQueryString
%>
<a href="<%= strURL %>">Support technique Microsoft</a>
</li>
</p>

    </font></td>
  </tr>
  
</table>
</body>
</html>

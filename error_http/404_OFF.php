<?php 
require(dirname(__FILE__). DIRECTORY_SEPARATOR .'..'. '/global_conn.php');
require(dirname(__FILE__). DIRECTORY_SEPARATOR .'..'. '/functions/common.php');
$sUrl =null;
$sUrl = split(";",$_SERVER["QUERY_STRING"]);
if (strpos(0,$sUrl,"ima%20ages",0) != 0) {
  $sUrl=str_replace("ima%20ages","images",$sUrl); // WARNING: assuming sUrl is an external function
  header("Location: ".$sUrl);
}
elseif (strpos(0,$sUrl,"desi%20inscription",0) != 0) {
  $sUrl=str_replace("desi%20inscription","desinscription",$sUrl);
  header("Location: ".$sUrl);
}else {
    $sess_id_inscrit = !empty($_SESSION['operateur']) ? $_SESSION['id_inscrit'] : null;
    $ser = $_SERVER["REMOTE_ADDR"];
    $ser_HTTP_USER_AGENT = $_SERVER["HTTP_USER_AGENT"];    
	
	$result = query("CALL p_insert_t_erreur_http(?,?,?,?,?,?,?,?)",array(
		array('value'=>$sess_id_inscrit, "type"=>PDO::PARAM_INT),
		array('value'=>$ser, "type"=>PDO::PARAM_STR, "length"=>20),
		array('value'=>$sUrl, "type"=>PDO::PARAM_STR, "length"=>200),
		array('value'=>'404', "type"=>PDO::PARAM_STR, "length"=>50),
		array('value'=>"Cette page n'existe pas (404)", "type"=>PDO::PARAM_STR, "length"=>1000),
		array('value'=>$ser_HTTP_USER_AGENT, "type"=>PDO::PARAM_STR, "length"=>1000),
		array('value'=>null, "type"=>PDO::PARAM_STR, "length"=>255),
		array('value'=>null, "type"=>PDO::PARAM_STR, "length"=>255)
	),false);
	
	
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<html dir=ltr>

<head>
<style>
a:link			{font:8pt/11pt verdana; color:FF0000}
a:visited		{font:8pt/11pt verdana; color:#4e4e4e}
</style>

<META NAME="ROBOTS" CONTENT="NOINDEX">

<title>Le site Web est introuvable</title>

<META HTTP-EQUIV="Content-Type" Content="text-html; charset=Windows-1252">
</head>

<script> 
function Homepage(){
<!--
// in real bits, urls get returned to our script like this:
// res://shdocvw.dll/http_404.htm#http://www.DocURL.com/bar.htm 

	//For testing use DocURL = "res://shdocvw.dll/http_404.htm#https://www.microsoft.com/bar.htm"
	DocURL = document.URL;
		
	//this is where the http or https will be, as found by searching for :// but skipping the res://
	protocolIndex=DocURL.indexOf("://",4);
	
	//this finds the ending slash for the domain server 
	serverIndex=DocURL.indexOf("/",protocolIndex + 3);

		//for the href, we need a valid URL to the domain. We search for the # symbol to find the begining 
	//of the true URL, and add 1 to skip it - this is the BeginURL value. We use serverIndex as the end marker.
	//urlresult=DocURL.substring(protocolIndex - 4,serverIndex);
	BeginURL=DocURL.indexOf("#",1) + 1;
	
	urlresult=DocURL.substring(BeginURL,serverIndex);
				
	//for display, we need to skip after http://, and go to the next slash
	displayresult=DocURL.substring(protocolIndex + 3 ,serverIndex);

	InsertElementAnchor(urlresult, displayresult);
}

function HtmlEncode(text)
{
    return text.replace(/&/g, '&amp').replace(/'/g, '&quot;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
}

function TagAttrib(name, value)
{
    return ' '+name+'="'+HtmlEncode(value)+'"';
}

function PrintTag(tagName, needCloseTag, attrib, inner){
    document.write( '<' + tagName + attrib + '>' + HtmlEncode(inner) );
    if (needCloseTag) document.write( '</' + tagName +'>' );
}

function URI(href)
{
    IEVer = window.navigator.appVersion;
    IEVer = IEVer.substr( IEVer.indexOf('MSIE') + 5, 3 );

    return (IEVer.charAt(1)=='.' && IEVer >= '5.5') ?
        encodeURI(href) :
        escape(href).replace(/%3A/g, ':').replace(/%3B/g, ';');
}

function InsertElementAnchor(href, text)
{
    PrintTag('A', true, TagAttrib('HREF', URI(href)), text);
}

//-->
</script>

<body bgcolor="FFFFFF">

<table width="410" cellpadding="3" cellspacing="5">

  <tr>    
    <td align="left" valign="middle" width="360">
	<h1 style="COLOR:000000; FONT: 13pt/15pt verdana"><!--Problem-->La page Web est introuvable<br><br>
	Accèder a <a href="http://www.lyad.com"><font size=3><b>WWW.LYAD.COM</b></font></a></h1>
    </td>
  </tr>
  
  <tr>
    <td width="400" colspan="2">
	<font style="COLOR:000000; FONT: 8pt/11pt verdana">Le site Web que vous recherchez est indisponible en raison de ses paramètres de configuration d'identification.</font></td>
  </tr>
  
  <tr>
    <td width="400" colspan="2">
	<font style="COLOR:000000; FONT: 8pt/11pt verdana">

	<hr color="#C0C0C0" noshade>
	
    <p>Essayez les opérations suivantes :</p>

	<ul>
      <li>Si vous avez tapé l'adresse de la page dans la barre d'adresses, assurez-vous que vous l'avez tapée correctement.<br>
      </li>
	  
      <li>Cliquez sur le bouton <a href="javascript:history.back(1)">Précédent</a> pour essayer un autre lien.</li>
      </ul>
	
    <h2 style="font:8pt/11pt verdana; color:000000">HTTP 404.1 – Site Web introuvable<br> Services Internet (IIS)<BR>
       </h2>

	<hr color="#C0C0C0" noshade>
	
	<p>Informations techniques (destinées au personnel du Support technique)</p>
	
<ul>

<p>
<li>Contexte :<br> Cette erreur indique que le site Web auquel vous essayez d'accéder a une adresse IP qui n'accepte pas les requêtes en provenance du port sur lequel votre requête est arrivée.</li></p>

<p>
<li>Informations complémentaires :<br>
<a href="http://www.microsoft.com/ContentRedirect.php?prd=iis&sbp=&pver=5.0&pid=&ID=404.1&cat=web&os=&over=&hrd=&Opt1=&Opt2=&Opt3=" target="_blank">Support technique Microsoft</a>
</li>
</ul>
 
    </font></td>
  </tr>
  
</table>
</body>
</html>


<?php
	session_start();
	require(dirname(__FILE__). '/global_conn.php');
	require (dirname(__FILE__). '/jpgraph/jpgraph.php');
	require (dirname(__FILE__). '/jpgraph/jpgraph_line.php');
?>
<?php 
header("Expires: " . gmdate("D, d M Y H:i:s", time() + (0*60)) . " GMT"); 
ob_start();
ob_clean(); 
?>
<?php 

$id=!empty($_REQUEST["id"])?$_REQUEST["id"]:"";
if ($_SESSION['id']!="" && $_SESSION['id']!=$id && $id!="505"){
	$id=$_SESSION['id'];
} 
if ($id==""){
  $id=$_SESSION['id'];
} 

$ddate=!empty($_REQUEST["ddate"])?$_REQUEST["ddate"]:"";
$fdate=!empty($_REQUEST["fdate"])?$_REQUEST["fdate"]:"";
 $_clics = !empty($_REQUEST["clics"])?$_REQUEST["clics"]:0;
 $_inscriptions = !empty($_REQUEST["inscriptions"])?$_REQUEST["inscriptions"]:0;
 $_hits = !empty($_REQUEST["hits"])?$_REQUEST["hits"]:0;
 $_abonne = !empty($_REQUEST["abonne"])?$_REQUEST["abonne"]:0;
 if(!empty($ddate) && !empty($fdate)){
	$date1 = explode("/",$ddate);
	$date2 = explode("/",$fdate);
	$date2=mktime(0,0,0,$date2[1],$date2[0],$date2[2]);
	$date1=mktime(0,0,0,$date1[1],$date1[0],$date1[2]);
	$d=$date2 - $date1;
	$xdatediff=(int)abs(floor($d/(365*24*10)))+1;
	//$wcount = $xdatediff;
 }else{
	 //$wcount=0;
 }
 
if ($_inscriptions==1){

  $Title=_t("Inscriptions ces")." ".$xdatediff." "._t("derniers jours / du")." ".$ddate." "._t("et")." ".$fdate;
}else{
	if ($_abonne ==1){
	$Title=_t("Evolution de vos Abonnés ces")." ".$xdatediff." "._t("derniers jours / du")." ".$ddate." "._t("et")." ".$fdate;
	}else{
		$Title=_t("Visiteurs Unique ces")." ".$xdatediff." "._t("derniers jours / du")." ".$ddate." "._t("et")." ".$fdate;
	} 
}   

///**** Begining
$rs = query("CALL p_af_eric_select_ca_new(?,?)",array(
			array('value'=>$id,"type"=>PDO::PARAM_STR,"length"=>255),
			array('value'=>$xdatediff+30,"type"=>PDO::PARAM_STR,"length"=>255)
		),false);
// Setup the graph
$graph = new Graph(520,230);
if(!empty($rs)){
	$graph->SetScale("textlin");
}else{
	$graph->SetScale('textlin',0,200,-1,100);
}

$graph->img->SetAntiAliasing(false);
$graph->title->Set($Title);
$graph->title->SetColor("#000000");
$graph->title->SetFont(FF_FONT1); 
$graph->SetBox(false);
$graph->img->SetImgFormat("png");
// Setup X-scale
//$graph->xaxis->SetPos('min');
//$graph->xaxis->scale->SetAutoMin(-1);
$graph->xaxis->SetTextLabelInterval(5);
$graph->ygrid->SetLineStyle("dotted");
//$graph->xaxis->scale->ticks->SetSize(8,3);
$graph->ygrid->SetColor('#464637');
		
 $clics =  array();
 $ins =  array();
 $ximp = array();
 $xab =  array();
 
 if(!empty($rs)){
	 foreach($rs as $item){
		if ($_clics == 1){
			array_push($clics, $item["nb_hit"]);
		}	
		
		if  ($_inscriptions == 1){
			array_push($ins, $item["nb_inscription"]);
		}

		if  ($_hits == 1){
			array_push($ximp, $item["nb_impression"]);
		}

		if  ($_abonne == 1){
			array_push($xab, $item["nb_abonne"]);
		}
	 }
 }
if(!empty($clics)){
	//Create the clics line
	$p1 = new LinePlot($clics);
	$graph->Add($p1);
	$p1->SetWeight(2); 
	$p1->SetColor("#8000a0");
	$legend = _t("Visiteurs Uniques");
	$p1->SetLegend($legend);
	$p1->SetStyle("solid"); 
}
if(!empty($ins)){
	//Create the ins line
	$p2 = new LinePlot($ins);
	$graph->Add($p2);
	$p2->SetWeight(2); 
	$p2->SetColor("#8000a0");
	$legend = _t("Insciptions");
	$p2->SetLegend($legend);
	$p2->SetStyle("solid"); 
}
if(!empty($ximp)){
	//Create the ximp line
	$p3 = new LinePlot($ximp);
	$graph->Add($p3);
	$p3->SetWeight(2); 
	$p3->SetColor("#999999");
	$legend = _t("Impressions");
	$p3->SetLegend($legend);
	$p3->SetStyle("solid"); 
}
if(!empty($xab)){
		//Create the xab line
		$p4 = new LinePlot($xab);
		$graph->Add($p4);
		$p4->SetWeight(2); 
		$p4->SetColor("#8000a0");
		$legend = _t("Abonnés");	
		$p4->SetLegend($legend);
		$p4->SetStyle("solid"); 
	}
$graph->legend->SetFrameWeight(1);
// Output line
$graph->Stroke(); 


?>


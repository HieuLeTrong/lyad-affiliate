<?php
/**
* PHPMailer language file: refer to English translation for definitive list
* Turkish version
* Türkçe Versiyonu
* ÝZYAZILIM - Elçin Özel - Can Yýlmaz - Mehmet Benlioðlu
*/

$PHPMAILER_LANG['authenticate']         = 'SMTP Hatasi: Dogrulanamiyor.';
$PHPMAILER_LANG['connect_host']         = 'SMTP Hatasi: SMTP hosta baglanilamiyor.';
$PHPMAILER_LANG['data_not_accepted']    = 'SMTP Hatasi: Veri kabul edilmedi.';
$PHPMAILER_LANG['empty_message']        = 'Mesaj içerigi bos';
$PHPMAILER_LANG['encoding']             = 'Bilinmeyen sifreleme: ';
$PHPMAILER_LANG['execute']              = 'Çalitirilamiyor: ';
$PHPMAILER_LANG['file_access']          = 'Dosyaya erisilemiyor: ';
$PHPMAILER_LANG['file_open']            = 'Dosya Hatasi: Dosya açilamiyor: ';
$PHPMAILER_LANG['from_failed']          = 'Basarisiz olan gönderici adresi: ';
$PHPMAILER_LANG['instantiate']          = 'Örnek mail fonksiyonu olusturulamadi.';
$PHPMAILER_LANG['invalid_address']        = 'Gönderilmedi, email adresi geçersiz: ';
$PHPMAILER_LANG['provide_address']      = 'En az bir tane mail adresi belirtmek zorundasiniz alicinin email adresi.';
$PHPMAILER_LANG['mailer_not_supported'] = ' mailler desteklenmemektedir.';
$PHPMAILER_LANG['recipients_failed']    = 'SMTP Hatasi: alicilara ulaimadi: ';
$PHPMAILER_LANG['signing']              = 'Imzalama hatasi: ';
$PHPMAILER_LANG['smtp_connect_failed']  = 'SMTP baglanti() basarisiz.';
$PHPMAILER_LANG['smtp_error']           = 'SMTP sunucu hatasi: ';
$PHPMAILER_LANG['variable_set']         = 'Ayarlanamiyor yada sifirlanamiyor: ';


<?php
/**
* PHPMailer language file: refer to English translation for definitive list
* Latvian version by Eduards M. <e@npd.lv>
*/

$PHPMAILER_LANG['authenticate']         = 'SMTP kluda: Autorizacija neizdevas.';
$PHPMAILER_LANG['connect_host']         = 'SMTP Kluda: Nevar izveidot savienojumu ar SMTP serveri.';
$PHPMAILER_LANG['data_not_accepted']    = 'SMTP Kluda: Nepienem informaciju.';
$PHPMAILER_LANG['empty_message']        = 'Zinojuma teksts ir tuks';
$PHPMAILER_LANG['encoding']             = 'Neatpazits kodejums: ';
$PHPMAILER_LANG['execute']              = 'Neizdevas izpildit komandu: ';
$PHPMAILER_LANG['file_access']          = 'Fails nav pieejams: ';
$PHPMAILER_LANG['file_open']            = 'Faila kluda: Nevar atvert failu: ';
$PHPMAILER_LANG['from_failed']          = 'Nepareiza sutitaja adrese: ';
$PHPMAILER_LANG['instantiate']          = 'Nevar palaist sutianas funkciju.';
$PHPMAILER_LANG['invalid_address']      = 'Nepareiza adrese';
$PHPMAILER_LANG['mailer_not_supported'] = ' sutitajs netiek atbalstits.';
$PHPMAILER_LANG['provide_address']      = 'Ludzu, noradiet vismaz vienu adresatu.';
$PHPMAILER_LANG['recipients_failed']    = 'SMTP kluda: neizdevas nosutit adiem sanemejiem: ';
$PHPMAILER_LANG['signing']              = 'Autorizacijas kluda: ';
$PHPMAILER_LANG['smtp_connect_failed']  = 'SMTP savienojuma kluda';
$PHPMAILER_LANG['smtp_error']           = 'SMTP servera kluda: ';
$PHPMAILER_LANG['variable_set']         = 'Nevar piekirt mainiga vertibu: ';


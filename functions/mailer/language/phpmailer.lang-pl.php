<?php
/**
* PHPMailer language file: refer to English translation for definitive list
* Polish Version
*/

$PHPMAILER_LANG['authenticate']         = 'Blad SMTP: Nie mozna przeprowadzic autentykacji.';
$PHPMAILER_LANG['connect_host']         = 'Blad SMTP: Nie mozna polaczyc sie z wybranym hostem.';
$PHPMAILER_LANG['data_not_accepted']    = 'Blad SMTP: Dane nie zostaly przyjete.';
$PHPMAILER_LANG['empty_message']        = 'Wiadomosc jest pusta.';
$PHPMAILER_LANG['encoding']             = 'Nieznany sposób kodowania znaków: ';
$PHPMAILER_LANG['execute']              = 'Nie mozna uruchomic: ';
$PHPMAILER_LANG['file_access']          = 'Brak dostepu do pliku: ';
$PHPMAILER_LANG['file_open']            = 'Nie mozna otworzyc pliku: ';
$PHPMAILER_LANG['from_failed']          = 'Nastepujacy adres Nadawcy jest nieprawidlowy: ';
$PHPMAILER_LANG['instantiate']          = 'Nie mozna wywolac funkcji mail(). Sprawdz konfiguracje serwera.';
$PHPMAILER_LANG['invalid_address']      = 'Nie mozna wyslac wiadomosci, nastepujacy adres Odbiorcy jest nieprawidlowy: ';
$PHPMAILER_LANG['provide_address']      = 'Nalezy podac prawidlowy adres email Odbiorcy.';
$PHPMAILER_LANG['mailer_not_supported'] = 'Wybrana metoda wysylki wiadomosci nie jest obslugiwana.';
$PHPMAILER_LANG['recipients_failed']    = 'Blad SMTP: Nastepujacy odbiorcy sa nieprawidlowi: ';
$PHPMAILER_LANG['signing']              = 'Blad podpisywania wiadomosci: ';
$PHPMAILER_LANG['smtp_connect_failed']  = 'SMTP Connect() zakonczone niepowodzeniem.';
$PHPMAILER_LANG['smtp_error']           = 'Blad SMTP: ';
$PHPMAILER_LANG['variable_set']         = 'Nie mozna zmienic zmiennej: ';


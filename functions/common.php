<?php
	ini_set('session.bug_compat_warn', 0);
	ini_set('session.bug_compat_42', 0);
	require(dirname(__FILE__). '/debug/Debug.php');

	/**
	 * This action used assign text on image
	 **/
	function watermarkImage($SourceFile, $WaterMarkText,$points = array(),$type = "png") { 

		// load the image from the file specified:
		switch($type){
			case "jpeg":
				$im = imagecreatefromjpeg($SourceFile);
			break;	
			default:
				
			$im = imagecreatefrompng($SourceFile);
			break;
		}
		
		// if there's an error, stop processing the page:
		if(!$im)
		{
			die("File not found");
		}
		 
		// define some colours to use with the image
		$yellow = imagecolorallocate($im, 255, 255, 0);
		$black = imagecolorallocate($im, 0, 0, 0);
		$w = imagecolorallocate($im, 255,0,0);
		
		 
		// get the width and the height of the image
		$width = imagesx($im);
		$height = imagesy($im);
		 
		// draw a black rectangle across the bottom, say, 20 pixels of the image:
		// imagefilledrectangle($im, 0, ($height-20) , $width, $height, $w);
		 
		// now we want to write in the centre of the rectangle:
		$font = 14;
		$text = $WaterMarkText; // store the text we're going to write in $text
		// calculate the left position of the text:
		$leftTextPos = ( $width - imagefontwidth($font)*strlen($text) )/2;

		// finally, write the string:
		if(empty($points)){
			imagestring($im, $font, $leftTextPos, $height-35, $text, $black);
		}else{
			imagestring($im, $font, $points[0], $points[1], $text, $black);
		}
		 
		// output the image
		// tell the browser what we're sending it
		header('Content-type: image/png');
		// output the image as a png
		imagepng($im);
		 
		// tidy up
		imagedestroy($im);

		

	}
	/**
	 * Params: array(
	 * 		'value'=>1,
	 * 		'type'=>PDO::PARAM_STR
	 * 		'length'=>255
	 * )
	 **/
	/**
   * Params: array(
   *    'value'=>1,
   *    'type'=>PDO::PARAM_STR
   *    'length'=>255
   * )
   **/
	function query($sql = null,$params= array(),$fetch= true,$selectSQL = null,$setSQL = null){
    $data = array();
    if($sql == null) return $data;
    //if($params == null) return $data;

    $conn = connection(true);
    
    try{
      $stmt = $conn->prepare($sql);
    }catch(Exception $e){
      $message = $e->getMessage();
      cleanUpDB();
      jsonOut(array(),$message,true);
    }
    $cnt = 0;
    foreach($params as $key=>$value){
		$cnt++;
		if(!is_array($value['value']) || !is_object($value['value'])){
			$value['value'] = isset($value['value']) ? html_entity_decode($value['value']) : $value['value'];
		}
		if(isset($value['length']))
			$stmt->bindParam($cnt,$value['value'],$value['type'],$value['length']);
		else{
			if(!empty($value['type'])){         
			  $stmt->bindParam($cnt,$value['value'],$value['type']);
			}else{
			  $stmt->bindParam($cnt,$value['value'],PDO::PARAM_STR);
			}
		}
    }
    if($setSQL!=null){
      $tmp = explode(",",$setSQL);
      foreach($tmp as $key=>$value){
        $conn->query($value)->fetch();
      }
      
    }
    if($fetch){
      try{
        $stmt->execute();
        $tmpData = array();
        if($selectSQL!=null){
          $tmp = explode(",",$selectSQL);
          foreach($tmp as $key=>$value){
            $select = $conn->query("SELECT ".$value)->fetch();
            foreach($select as $key=>$value):
              if(is_string($key)){
                $tmpData[$key] = $value;
              }
            endforeach;
          }
          
        }
        
        do {
          $rowset = $stmt->fetchAll(PDO::FETCH_ASSOC);
          $data[] = $rowset;
        } while ($stmt->nextRowset());
        cleanUpDB();
        $data = array_merge($data,$tmpData);
        return $data;
      }catch(Exception $e){
        $message = $e->getMessage();
        
        cleanUpDB();
        jsonOut(array(),$message,true);
      }
    }else{
      try{
        $stmt->execute();
        $tmpData = array();
        if($selectSQL!=null){
           $tmp = explode(",",$selectSQL);
          foreach($tmp as $key=>$value){
            $select = $conn->query("SELECT ".$value)->fetch();
            foreach($select as $key=>$value):
              if(is_string($key)){
                $tmpData[$key] = $value;
              }
            endforeach;
          }
          
          
        }
        
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $data = array_merge($data,$tmpData);
        cleanUpDB();
        return $data;
      }catch(Exception $e){
        $message = $e->getMessage();
        
        cleanUpDB();
        jsonOut(array(),$message,true);
      }
    }


	}
	/**
	 * This function used call procedure to mysql
	 * Used: $result = callProcedure($sql,true,array('@id_skin','@application','@titre','@ville','@langue'));
	 * Or: $result = callProcedure($sql);
	 */
	
	function callProcedure($arrSql = null,$return = false,$dataReturn = array(),$getMultipleRes = false,$hand = false){

		$is_array  = false;
		$sql = $arrSql;
		$sql0 = null;
		$multipleSQL = false;
		if(is_array($arrSql)){
			$sql = end($arrSql);
			$sql0 = $arrSql[0];
			$multipleSQL = true;
		}
		// dump($sql);
		if($return == false){
			
			try{
				$result = mysql_query($sql,connection());
				if (!$result) {
					die('Invalid query: ' . mysql_error()." - SQL: {$sql}");
				}else{
					$data = array();
					
					if(@is_resource($result)){
						
					// }else{
					
						$numResults = mysql_num_rows($result);
						if ($numResults > 0) {
							while ($row = mysql_fetch_assoc($result)) {
								
								$data[] = $row;
								//return $desc;
							}
						}
						
						
					}
					return $data;
				}
			}catch(Exception $e){
				echo $e->getMessage()."<br>";
				echo $sql."<br>";
				die('Invalid query: ' . mysql_error()." - SQL: {$sql}");
			}
			
		}else{
			
			
			$mydb = connection(1);
			
			
			if(count($arrSql) > 2 && $multipleSQL){
				foreach($arrSql as $k=>$s){
					
					if($k != count($arrSql) - 1):
						//echo $s."<br>";
						$proc = $mydb->prepare($s);
						$proc->execute();
					endif;

				}
			}else{
				if($sql0!=null){
					$proc = $mydb->prepare($sql0);
					$proc->execute();
				}
			}
			
			//echo $sql;
			$proc = $mydb->prepare($sql);
			try{
				$proc->execute();
			}catch(Exception $e){
				echo $e->getMessage()."<br>";
				echo $sql."<br>";
				die('Invalid query: ' . mysql_error()." - SQL: {$sql}");
			}
			
			
			if($hand) return $proc;
			/**
			 * This partial used get multiple result for procedure
			 **/
			if($getMultipleRes){
				$maxRs = 40;
				$data = array();
				$cnt = 0;
				if(!empty($dataReturn)){
					foreach($dataReturn as $key=>$value){
						$row = $proc->fetchAll(PDO::FETCH_ASSOC);
						$data[$value] = $row;
						if($cnt != $maxRs){
							$proc->nextRowset();
						}
						$cnt++;
					}
				}else{
					
				}
				
				$proc->closeCursor();
				
				return $data;
			}else{
			
				if(!empty($dataReturn)){
					$strQuery = "SELECT";
					$cnt = 0;
					foreach($dataReturn as $key=>$data){
						if( count($dataReturn)-1 == $cnt){
							$strQuery.= " {$data}";
						}else{
							$strQuery.= " {$data},";
						}
						$cnt++;
						
					}
					
					$result = $mydb->query($strQuery)->fetch(PDO::FETCH_ASSOC);
					$proc->closeCursor();
					return $result;
				}else{
					$proc->closeCursor();
					return array();
				}
			}
			
		}
	}
	function callMultipleProcedure($sql = null,$return = false,$dataReturn = array()){
					$stmt = $mydb->prepare('CALL p_zm_get_profil(:param1, :param2)');
			$stmt->execute(array(':param1' => 77, ':param2' => 0));
			// read first result set
			while ($row = $stmt->fetch()) {
				dump($row,false);
			}
			$stmt->nextRowset();
			// read second result set
			while ($row = $stmt->fetch()) {
				dump($row,false);
			}
			die('dddddddddddddddddd');
	}
	// cette fonction cree un code unique utilise pour eviter de resortir un id d'un base (plus rapide permet de rentrer un id en mode asynchrone)
	// cree un num de 17 chiffres ,il existe 999999 codes aleatoires toutes les milisecondes(tres peux de chance de doublons)

	function give_id_ord(){
		$id_ord = !empty($_COOKIE['id_ord']['id_ord']) ? ($_COOKIE['id_ord']['id_ord']) : null;
		if(empty($id_ord)){
			$_SESSION['id_ord'] = $id_ord;
		}else{
			$y = substr(date('Y'),-1); 											//dernnier chiffre de l'ann?e 2004 -->4
			$j = date('z');
			$t = (date('h')*60*60)+(date('i')*60)+date('s'); 					//ex: t="1234"-->0123400 ou t="1234,5"-->0123450 ou t="1234,02"-->0123402
			if(strpos($t, ",")===false){
				$t = substr("00000".$t."00", -7);		//si il n'y a pas de miliseconde(je ne sais pas si ca arrive?)
			}else{
				$s = explode(",", $t);
				$t = substr("00000".$s[0], -5).substr($s[1]."00", 0,2);		//timer() = nb seconde depuis minuit 3600secondes *24h =86400,00 secondes max  (on suprime la virgule)
				
			}
			// randomize
			$rd = (10000000 * rand());
			$a = substr("000000".$rd,-6);										//nb aleat entre 000000-->999999
			$id_ord = $y.$j.$t.$a;

			/** on stock id dans un cookies
			 * date+360 perime jamais - experted until 01/01/2037
			 * path: "/" permet de voir le cookies de tous les repertoire	response.cookies("id_ord").domain = ".lyad.com"			'fonctionne aussi pour les subdomains
			 */
			setcookie("id_ord['id_ord']",$id_ord,strtotime("01/01/2037"),"/");	
			//response.cookies("id_ord").domain = ".lyad.com"			'fonctionne aussi pour les subdomains
			$_SESSION["id_ord"] = $id_ord;
		}
	}
	function dump($obj,$isExit = true) {
		Debug::d($obj,10,true);
		if ($isExit) exit();	
	}
	function geo(){
		$ip 				= $_SERVER["REMOTE_ADDR"];
		
		$checkLogs = false;
		$files = false;
		if(!empty($_SERVER["REMOTE_ADDR"])){
			$path = dirname(__FILE__)."/geos/{$ip}.txt";
			if($ip == "127.0.0.1"){
				$path = dirname(__FILE__)."/geos/127.0.0.1.txt";
			}
		}else{
			$path = dirname(__FILE__)."/geos/127.0.0.1.txt";
		}
		$files = @file_get_contents($path);
		if(!$files){
		
		}else{
			$arrGeo = @json_decode($files);
			return (object) $arrGeo;
		}
		
		/**
		HOT FIX
		**/
		// $arrGeo['ip']='213.144.202.'.rand(1,255);
		// $arrGeo['loc']='48.7916,2.6475';
		// $arrGeo['latitude']='48.7916';
		// $arrGeo['longtitude']='2.6475';
		// $arrGeo['longitude']='2.6475';
		// $arrGeo['city']='Paris';
		// $arrGeo['country']='FR';
		// $arrGeo['region']='FR';
		// $arrGeo['codepostal']=38100;
		// return (object) $arrGeo;
		
		if(!empty($_SERVER["REMOTE_ADDR"])){
			if($ip == "127.0.0.1"){
				$geo = @file_get_contents('http://geoip-release.toancauxanh.vn/');
			}else{
				$geo = @file_get_contents('http://geoip-release.toancauxanh.vn/?ip='.$ip);
			}
		}else{
			$geo = @file_get_contents('http://geoip-release.toancauxanh.vn/');
		}
		global $checkInternet;
		if(!$geo){
			$checkInternet = false;
		}
		
		if($checkInternet):
			$arrGeo = (array) @json_decode($geo);
			
			$lat = $arrGeo['latitude'];
			$lng = $arrGeo['longitude'];
			
			$postcode = @file_get_contents("http://maps.googleapis.com/maps/api/geocode/json?latlng=$lat,$lng&sensor=false");
			
			$postal_code = 0;
			if(!empty($postcode)){
				$result = (object) array();
				$geo = @json_decode($postcode);
				if(!empty($geo) && !empty($geo->results) && !empty($geo->results[0])){
					$result = $geo->results[0];
					foreach($result->address_components as $key=>$value){
						if(is_array($value->types)){
							foreach($value->types as $k=>$v){
								if($v == "postal_code") $postal_code = $value->short_name;
							}
						
						}
					}
				}
				
				
			}
			$arrGeo['codepostal'] = $postal_code;
			$arrGeo['longtitude'] = $arrGeo['longitude'];
			$arrGeo['country'] = $arrGeo['country_code'];
			$arrGeo['loc'] = $arrGeo['latitude'].", ".$arrGeo['longtitude'];
			$arrGeo['ip'] = !empty($arrGeo['ip']) ? $arrGeo['ip'] : "127.0.0.1";
			
			if(!$files){
				@file_put_contents($path, @json_encode($arrGeo),LOCK_EX);
			}else{
			
			}
		else:
		
			if(!empty($arrGeo['bogon'])){
				$arrGeo = array(
					"ip"=> "113.160.226.120",
					"hostname"=> "No Hostname",
					"city"=> "Hanoi",
					"region"=> "Ha Noi",
					"country"=> "VN",
					"loc"=> "21.033299999999997,105.85000000000002",
					"latitude"=>"21.033299999999997",
					"longtitude"=>"105.85000000000002",
					"org"=> "AS45899 VNPT Corp"
				);
			}
			$arrGeo['ip']='213.144.202.'.rand(1,255);
			$arrGeo['loc']='48.7916,2.6475';
			$arrGeo['latitude']='48.7916';
			$arrGeo['longtitude']='2.6475';
			$arrGeo['longitude']='2.6475';
			$arrGeo['city']='Paris';
			$arrGeo['country']='FR';
			$arrGeo['region']='FR';
			$arrGeo['codepostal']=38100;
			if(!$files){
				@file_put_contents($path, @json_encode($arrGeo),LOCK_EX);
			}else{
			
			}
		endif;
		return (object) $arrGeo;
	}
	
	
	function sHtmMenuNew($sSelected,$lemois,$lejour,$sColor,$sColor2,$lessMenu = false) {
		$assets = new Assets();
		
		$sHtml = "";
		$iNbOnline = !empty($_SESSION["iNbOnline"]) ? $_SESSION["iNbOnline"] : 0;
		$iNbMembre = !empty($_SESSION["iNbMembre"]) ? $_SESSION["iNbMembre"] : 0;
	  $sHtml.="<div class=\"contenu ht3\"><font color=\"".$sColor."\"><b>{$iNbOnline}</b></font> membres dont <font color=\"".$sColor."\"><b>{$iNbMembre}</b></font> en ligne</div>"; // WARNING: assuming sColor is an external function the ASP application object is not supported assuming sColor is an external function the ASP application object is not supported
	  $sHtml.="<ul class=\"mlu\">";
	  
	  if($lessMenu){
		
		$tmuid_inscrit =  !empty($_SESSION['uid_inscrit']) ? $_SESSION['uid_inscrit'] : null;
		$sHtml.= "<li class=mlr><a href='/?uid_inscrit=".$tmuid_inscrit."' class='mll'  " . viIf($sSelected=="decouverte",'style=background:'.$sColor.';color:white;',  '') . " title='Espace découverte'>Zone&nbsp;découverte</a></li>";
		$sHtml.= "<li class=mlr><a href='/?uid_inscrit=&redir=modification' class='mll' " . viIf($sSelected=="profil",'style=background:'.$sColor.';color:white;','') . " title='Modifier mon profil'>&nbsp;Mon&nbsp;profil&nbsp;</a></li>";
		$sHtml.= "<li class=mlr><a href='/rubriques/messenger/' class='mll' " . viIf($sSelected=="messenger", 'style=background:'.$sColor.';color:white;','') . " title='Chat avec " . viIf($_SESSION['appli_skin']==2,"Okeny",viIf($_SESSION['appli_skin']==3,"Okety","Lyad")) . " messenger'>" . viIf($_SESSION['appli_skin']==2,"Okeny",viIf($_SESSION['appli_skin']==3,"Okety","Lyad")) . "&nbsp;Messenger</a></li>";
		$sHtml.= "<li class=mlr><a href='/rubriques/contact/' class='mllc' " . viIf($sSelected=="contact", 'style=background:'.$sColor.';color:white;', '') . " title='contact'>>&nbsp;Contact</a></li>";
		//dump($_SESSION['appli_skin']);
	  }else{
	  
			if($sSelected == "accueil"){
				$sHtml.="<li class=mlr><a href=\"/\" class=\"mll\" style=\"background:".$sColor.";color:white;\" title=\"Chat et rencontres\">Accueil</a></li>";
			  }else{
				$sHtml.="<li class=mlr><a href=\"/\" class=\"mll\"  title=\"Chat et rencontres\">Accueil</a></li>";
			  }
				if($sSelected == "presentation"){
					$sHtml.="<li class=mlr><a href=\"/rubriques/presentation/\" class=\"mll\" style=\"background:".$sColor.";color:white;\" title=\"présentation  du chat\">Présentation</a></li>";
				}else{
					$sHtml.="<li class=mlr><a href=\"/rubriques/presentation/\" class=\"mll\"  title=\"présentation  du chat\">Présentation</a></li>";
				}
				if($sSelected == "inscription"){
					$sHtml.="<li class=mlr><a href=\"/rubriques/inscription/\" class=\"mll\" style=\"background:".$sColor.";color:white;\" title=\"inscription au Tchat\">Inscription</a></li>"; 
				}else{
					$sHtml.="<li class=mlr><a href=\"/rubriques/inscription/\" class=\"mll\"  title=\"inscription au Tchat\">Inscription</a></li>"; 
				}
				if($sSelected == "faq"){
					$sHtml.="<li class=mlr><a href=\"/rubriques/faq/\" class=\"mll\" style=\"background:".$sColor.";color:white;\" title=\"foire aux questions\">&nbsp;FAQ&nbsp;</a></li>";
				}else{
					$sHtml.="<li class=mlr><a href=\"/rubriques/faq/\" class=\"mll\"  title=\"foire aux questions\">&nbsp;FAQ&nbsp;</a></li>";
				}
				if($sSelected == "messenger"){
					$sHtml.="<li class=mlr><a href=\"/rubriques/messenger/\" class=\"mll\" style=\"background:".$sColor.";color:white;\" title=\"Chat avec Lyad  messenger\">Lyad&nbsp;Messenger</a></li>"; 
				}else{
					$sHtml.="<li class=mlr><a href=\"/rubriques/messenger/\" class=\"mll\"  title=\"Chat avec Lyad  messenger\">Lyad&nbsp;Messenger</a></li>"; 
				}
				if($sSelected == "contact"){
					$sHtml.="<li class=mlr><a href=\"/rubriques/contact/\" class=\"mllc\" style=\"background:".$sColor.";color:white;\" title=\"contact\">Contact</a></li>";
				}else{
					$sHtml.="<li class=mlr><a href=\"/rubriques/contact/\" class=\"mllc\"  title=\"contact\">>&nbsp;Contact</a></li>";
				}
	  
	  
	  }
	  
	  
	  
	  
	  
	  
	   // WARNING: assuming sColor is an external function assuming viIf is an external function
	  // WARNING: assuming sColor is an external function assuming viIf is an external function assuming appli_skin is an external function assuming appli_skin is an external function assuming viIf is an external function assuming viIf is an external function assuming appli_skin is an external function assuming appli_skin is an external function assuming viIf is an external function assuming viIf is an external function
	   // WARNING: assuming sColor is an external function assuming viIf is an external function
	  $sHtml.="</ul>";
	 // dump($_SESSION['appli_skin']);
	  
	  $ok = "";
	  $width = "175";

	  if($_SESSION['appli_skin'] == 2){
		  $ok = "-okety";
		  $width = "210";
	  }else{
		if($_SESSION['appli_skin'] == 3){
			$ok = "-okety";
			$width = "210";
		}
		  
	  }
	//dump($ok);
	  if (($lemois == 12 && $lejour > 15) || ($lemois == 1 && $lejour < 10)) {
		$sHtml.="<div class=\"houx\"></div>"; // WARNING: assuming lemois is an external function assuming lejour is an external function assuming lemois is an external function assuming lejour is an external function
		$sHtml.="<div style=\"position:absolute;top:26px;z-index:2;left:5px;width:190px;height:22px;background:url(".$assets->baseUrl."/images/navigation/neige-lyad.gif) no-repeat;\"></div>"; // WARNING: assuming appli_url is an external function
		if ($_SESSION['appli_skin'] < 2) {
		  $sHtml.="<div style=\"position:absolute;top:7px;z-index:3;left:4px;width:175px;height:49px;\"><a href=\"/\" style=\"text-decoration:none;\"><img src=\"".$assets->baseUrl."/images/navigation/logo-noel.gif\"  border=0 width=174 height=55 alt=\"votre site de dialogue\"></a></div>"; // WARNING: assuming appli_skin is an external function assuming appli_url is an external function
		}
		else {
		  $sHtml.="<div class=\"logo\"><a href=\"/\" title=\"site de rencontre et de dialogue\"><img src=\"http://".$ok.".gif\" border=0 width=\"".$width."\" height=49 alt=\"site de rencontre et de dialogue\" title=\"site de rencontre et de dialogue\"></a></div>"; // WARNING: assuming appli_url is an external function assuming appli_skin is an external function assuming appli_skin is an external function assuming viIf is an external function assuming viIf is an external function assuming appli_skin is an external function assuming appli_skin is an external function assuming viIf is an external function assuming viIf is an external function
		}
	  }
	  elseif ($lemois == 1 && $lejour > 4 && $lejour < 20 && $_SESSION['appli_skin'] < 2) {
		$sHtml.="<div style=\"position:absolute;top:7px;z-index:3;left:5px;width:175px;height:49px;\"><a href=\"/\" style=\"text-decoration:none;\"><img src=\"".$assets->baseUrl."/images/navigation/logo-fete.gif\"  border=0 width=174 height=55 alt=\"votre site de dialogue\"></a></div>"; // WARNING: assuming lemois is an external function assuming lejour is an external function assuming lejour is an external function assuming appli_skin is an external function assuming appli_url is an external function
		$sHtml.="<div style=\"position:absolute;top:26px;z-index:2;left:5px;width:190px;height:22px;background:url(".$assets->baseUrl."/images/navigation/neige-lyad.gif) no-repeat;\"></div>"; // WARNING: assuming appli_url is an external function
	  }
	  else {
		  
		$sHtml.="<div class=\"logo\"><a href=\"/\" title=\"site de rencontre et de dialogue\"><img src=\"".$assets->baseUrl."/images/navigation/logo".$ok.".gif\" border=0 width=\"".$width."\" height=49 alt=\"site de rencontre et de dialogue\" title=\"site de rencontre et de dialogue\"></a></div>"; // WARNING: assuming appli_url is an external function assuming appli_skin is an external function assuming appli_skin is an external function assuming viIf is an external function assuming viIf is an external function assuming appli_skin is an external function assuming appli_skin is an external function assuming viIf is an external function assuming viIf is an external function
	  }
	  return $sHtml;
	}


	function htmldump($variable, $height="14em") {
		echo "<pre style=\"border: 1px solid red; height: {$height}; overflow: auto; margin: 0.5em;\">";
			var_dump($variable);
		echo "</pre>\n";
	}
    function sCreateHautBox($url =null , $width =null , $height =null){
        if(!empty($url)){
            $image= "<img border='0' src='$url'  width='$width' height='$height'>";
            return $image;
        }

    }
        function sCreateBasBox(){
        return null;
        }  
		
		
	function otherDiffDate($out_in_array=false,$end=null){
		if($end == null) $end = '2020-06-09 10:30:00';
		
		$end = date("Y-m-d",strtotime($end));
		
        $intervalo = date_diff(date_create(), date_create($end));
        $out = $intervalo->format("Years:%Y,Months:%M,Days:%d,Hours:%H,Minutes:%i,Seconds:%s");
        if(!$out_in_array)
            return $out;
        $a_out = array();
        array_walk(explode(',',$out),
        function($val,$key) use(&$a_out){
            $v=explode(':',$val);
            $a_out[$v[0]] = $v[1];
        });
        return $a_out;
	}
	//rajout marqueur pour stats mise en relation sur index direct
	function insert_marqueur_global($psMarqueur){

		if (!empty($_SESSION[$psMarqueur."_marqueur"]) && $_SESSION[$psMarqueur."_marqueur"]==1){
				return false;
		}
		try{
			
			
			query("CALL p_fe_insert_t_marker_changes(?,?,?)",array(
				array('value'=>$psMarqueur,"type"=>PDO::PARAM_STR,"length"=>255),
				array('value'=>null,"type"=>PDO::PARAM_STR,"length"=>255),
				array('value'=>date("Y-m-d"),"type"=>PDO::PARAM_STR,"length"=>255)
			),false);
		}catch(Exception $e){
			throw new Exception($e->getMessage());
		}

		$_SESSION[$psMarqueur."_marqueur"]=1;

	}
	
	function getMeta(){
		
		
		
	}
?>

<?php
//option explicit

//last modif 28/02/2006 20:12
//http://affiliation.lyad.com/analyses.asp?id=605&ddate=13/12/2005&fdate=13/02/2006
require(dirname(__FILE__). '/global_conn.php');
require (dirname(__FILE__). '/jpgraph/jpgraph.php');
require (dirname(__FILE__). '/jpgraph/jpgraph_line.php');

header("Expires: " . gmdate("D, d M Y H:i:s", time() + (0*60)) . " GMT"); ob_start();
ob_clean();

$id=!empty($_REQUEST["id"])?$_REQUEST["id"]:"";
$ddate=!empty($_REQUEST["ddate"])?$_REQUEST["ddate"]:"";
$fdate=!empty($_REQUEST["fdate"])?$_REQUEST["fdate"]:"";

if(!empty($ddate) && !empty($fdate)){
	$date1 = explode("/",$ddate);
	$date2 = explode("/",$fdate);
	$date2=mktime(0,0,0,$date2[1],$date2[0],$date2[2]);
	$date1=mktime(0,0,0,$date1[1],$date1[0],$date1[2]);
	$d=$date2 - $date1;
	$xdatediff = (int)($date2 - $date1)/(3600*24);
 }else{
 }
//var_dump($diff); exit();
$date1 = date("Y-m-d H:i:s", $date1);
$date2 = date("Y-m-d H:i:s", $date2);
//var_dump($date1);
//var_dump($date2);
//echo date("jS F, Y", strtotime("13/11/10")); 
// $Chart is of type "csDrawGraph.Draw"

///**** Bar Chart Properties (some also apply to line graphs)
if ($xdatediff>90){
  //$Chart->LineWidth=1;
}else{
  //$Chart->LineWidth=2;
} 
$arr_impressions = array();
$arr_transformation = array();

$impressions = !empty($_REQUEST['impressions'])?$_REQUEST['impressions']:0;
if ($impressions=="1"){
	///**** impressions ****/'
	// $sql_impressions = "call grenoble.aff_select_impressions('".$id."','".$date1."','".$date2."')";
	
	
	$rs_impressions = query("CALL grenoble.aff_select_impressions(?,?,?)",array(
			array('value'=>$id,"type"=>PDO::PARAM_STR,"length"=>255),
			array('value'=>$date1,"type"=>PDO::PARAM_STR,"length"=>255),
			array('value'=>$date2,"type"=>PDO::PARAM_STR,"length"=>255)
		),false);
	if(!empty($rs_impressions)){
		foreach($rs_impressions as $item){
			array_push($arr_impressions,$item['ping']);
		}
	}
} 
$transformation = !empty($_REQUEST['transformation'])?$_REQUEST['transformation']:0;
if ($transformation=="1"){
	///**** transformation ****/'
	
	
	$rs_transformation = query("CALL grenoble.aff_select_impressions(?,?,?)",array(
			array('value'=>$id,"type"=>PDO::PARAM_STR,"length"=>255),
			array('value'=>$date1,"type"=>PDO::PARAM_STR,"length"=>255),
			array('value'=>$date2,"type"=>PDO::PARAM_STR,"length"=>255)
		),false);
	
	// dump($rs_transformation);
	if(!empty($rs_transformation)){
		foreach($rs_transformation as $item){
			array_push($arr_transformation,$item['ping']);
		}
	}
} 
$Title = "Analyses ces ".$xdatediff." derniers jours // du ".$ddate." au ".$fdate;
// Setup the graph
$graph = new Graph(520,230);
if(empty($arr_impressions) && empty($arr_transformation)){
	$graph->SetScale('int',0,1,0,1);
}else{
	//$graph->SetScale("textlin");
	$graph->SetScale("int");
	$graph->yaxis->scale->SetAutoMin(0);
}
$graph->img->SetAntiAliasing(false);
$graph->title->Set($Title);
$graph->title->SetColor("#000000");
$graph->title->SetFont(FF_FONT1); 
$graph->SetBox(false);

//$graph->xaxis->SetTextLabelInterval(5);
$graph->ygrid->SetLineStyle("dotted");
$graph->ygrid->SetColor('#464637');

if(!empty($arr_impressions)){
	//Create the impressions line
	if(count($arr_transformation) <= 1){
		array_unshift($arr_transformation, 0);
	}
	$p0 = new LinePlot($arr_impressions);
	$graph->Add($p0);
	$p0->SetWeight(2); 
	$p0->SetColor("#8000a0");
	$p0->SetLegend("Impressions");
	$p0->SetStyle("solid"); 
}
if(!empty($arr_transformation)){
	// scale ticks
	if(count($arr_transformation) >= 365)
			$graph->xaxis->SetTextTickInterval(25,-1);
        elseif(count($arr_transformation) >= 150)
            $graph->xaxis->SetTextTickInterval(20,-1);
        elseif(count($arr_transformation) >= 120)
            $graph->xaxis->SetTextTickInterval(15,-1);
        elseif(count($arr_transformation) >= 90)
			$graph->xaxis->SetTextTickInterval(10,-1);
        elseif(count($arr_transformation) >= 60)
           $graph->xaxis->SetTextTickInterval(7,-1);
        elseif(count($arr_transformation) >= 30)
            $graph->xaxis->SetTextTickInterval(5,-1);
	
	//Create the transformation line
	if(count($arr_transformation) <= 1){
		array_unshift($arr_transformation, 0);
	}
	$p1 = new LinePlot($arr_transformation);
	$graph->Add($p1);
	$p1->SetWeight(2); 
	$p1->SetColor("#600080");
	$p1->SetLegend("Transformation");
	$p1->SetStyle("solid"); 
}
//echo count($arr_transformation);
$graph->legend->SetFrameWeight(1);
// Output line
$graph->Stroke(); 
?>


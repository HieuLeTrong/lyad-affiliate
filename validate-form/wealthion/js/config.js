

var headersHttp = {
	getHeaders: function(data){
		
		return {
			"Content-Type": "application/json",
			'Accept':'application/json'
		};
		
	},
	postHeaders: function(){
		var c = YouLookCookie.actions.getCookie("token");
		if(c != "") c = decodeURIComponent(c);
		c = c.replace(/"/g, '');
		return {
			'Accept':'application/json',
			"Access-Token": c,
			'Content-Type': 'application/x-www-form-urlencoded'
		};
	},
	putHeaders: function(){
		var c = YouLookCookie.actions.getCookie("token");
		if(c != "") c = decodeURIComponent(c);
		c = c.replace(/"/g, '');
		return {
			"Access-Token": c,
			'Content-Type': 'application/json'
		};
	},
	deleteHeaders: function(){
		var c = YouLookCookie.actions.getCookie("token");
		if(c != "") c = decodeURIComponent(c);
		c = c.replace(/"/g, '');
		return {
			'Accept':'application/json',
			"Access-Token": c
		};
	}
}
/** 
	YouLook: init project
**/
var Demo = angular.module('Demo', [
	'ui.router',
	'ngCookies',
	'ngSanitize',
	'ngDialog',
	'ngResource'
]);


/**
	Router for YouLook site.
	The section use for rewrite url
**/

Demo.config(function($stateProvider, $urlRouterProvider,$locationProvider) {		
	$locationProvider.html5Mode(false).hashPrefix('!');
	$urlRouterProvider.otherwise('/');
	$stateProvider
		.state('home', {
			url: '/test',
			templateUrl: 'templates/homepage.html'
		});

		
});
Demo.controller('MainController', function($scope, $rootScope,$http,$state,$stateParams,ngDialog) {
	$scope.items = ['What was the name of your elementary school?','What was the name of your favorite childhood friend?','What was the name of your childhood pet?'];
	$scope.states = ["VN","FR"];
	$scope.items1 = ["What street did you live on in third grade?","What is your oldest sibling's birth month?","In what city did your mother and father meet?"];
	$scope.items2 = ["Employed (full-time)","Employed (part-time)","Self-employed","Student","Retired","Homemaker","Not employed"];
	
	$root.option1 = "n";
	$scope.changeOption1 = function(){
		if($scope.option1=="y"){
			ngDialog.open({
				template: 'popupTmpl.html',
				controller:'MainController'
			});
		}
	}
	$scope.setOption1 = function(n){
		$scope.option1 = n;
		
		ngDialog.close();
	}
	$scope.register = function(){
		console.log($scope)
		
	}
	
	
});


Demo.directive("faQuestion", function(){
  return {
   restrict: "A",
	link: function(scope,el,attrs){
		setTimeout(function(){
			
			
			

			$('[data-toggle="tooltip"]').tooltip()


			
				
			
			
			
			
			
		},1000);
		
       
	}
  };
});
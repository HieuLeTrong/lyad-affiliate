! function() {
    "use strict";
    var t = angular.module("adaptive.youtube", []);

    t.controller("YoutubeCtrl", ["$scope", function(t) {
        t.updateStyle = function(e) {
            var o = "http://img.youtube.com/vi/" + e + "/0.jpg",
                r = "/img/v1.0/front/btn-play-video.png";
				/** r = YouLookServer.Youlook.HOST + "/img/v1.0/front/btn-play-video.png"; **/
			t.style = {
                display: "block",
                cursor: "pointer",
                "background-image": "url('" + r + "'), url('" + o + "')",
                "background-repeat": "no-repeat, no-repeat",
                "-webkit-background-size": "auto, cover",
                "-moz-background-size": "auto, cover",
                "-o-background-size": "auto, cover",
                "-ms-background-size": "auto, cover",
                "background-size": "auto, cover",
                "background-position": "center center, center center"
            }
        }, t.updateStyle(t.video)
    }]), t.directive("youtube", [function() {
        return {
            template: '<a ng-style="style" ng-href="{{VIDEO_HREF}}" target="_blank"></a>',
            replace: !0,
            restrict: "E",
            controller: "YoutubeCtrl",
            scope: {
                video: "="
            },
            link: function(t, e) {
                if (void 0 === t.video) throw new Error("The `video` attribute is required.");
                t.videoLoaded = !1, e.bind("click", function(o) {
                    o.preventDefault(), t.videoLoaded = !0;
                    var r = e[0].offsetHeight;		
                    e[0].innerHTML = '<iframe type="text/html" width="100%" height="' + r + '" src="http://www.youtube.com/embed/' + t.video + '?autoplay=1" frameborder="0"/>'
					$(".time").hide();
			   }), t.$watch("video", function() {
					
                    t.updateStyle(t.video), e[0].innerHTML = ""
                })
            }
        }
    }])
}();
/* ng-infinite-scroll - v1.0.0 - 2013-02-23 */
var mod;

mod = angular.module('infinite-scroll', []);

mod.directive('infiniteScroll', [
	'$rootScope', '$window', '$timeout', function($rootScope, $window, $timeout) {
	return {
		link: function(scope, elem, attrs) {
			var checkWhenEnabled, handler, scrollDistance, scrollEnabled, elemscroll, elemContent, newHeightContent;
			elemscroll = angular.element(elem); /**/
			scrollDistance = 0;
			if (attrs.infiniteScrollDistance != null) {
				scope.$watch(attrs.infiniteScrollDistance, function(value) {
					return scrollDistance = parseInt(value, 10);
				});
			}
			scrollEnabled = true;
			checkWhenEnabled = false;

			if (attrs.infiniteScrollDisabled != null) {
				scope.$watch(attrs.infiniteScrollDisabled, function(value) {					
					scrollEnabled = !value;
					if (scrollEnabled && checkWhenEnabled) {
						checkWhenEnabled = false;
						return handler();
					}
				});
			}
			if (attrs.infiniteElementHeight != null) {
				elemContent = attrs.infiniteElementHeight;					
			}			
			newHeightContent = 0;
			handler = function() {
				var elementBottom, remaining, shouldScroll, windowBottom;
				windowBottom = elemscroll.height() + elemscroll.scrollTop();
				elementBottom = elem.offset().top + elem.height();
				remaining = elementBottom - windowBottom;
				shouldScroll = remaining <= elemscroll.height() * scrollDistance;
				shouldScroll = false;
				
				heightScroll = elemscroll.scrollTop() +elemscroll.height();
				heightContent = angular.element(elemContent).height();
				
				if (heightScroll > heightContent && heightContent!=newHeightContent) {
					shouldScroll = true;
					newHeightContent = heightContent;
				}								
								
				if (shouldScroll && scrollEnabled) {
					if ($rootScope.$$phase) {
						return scope.$eval(attrs.infiniteScroll);
					} else {
						return scope.$apply(attrs.infiniteScroll);
					}
				} else if (shouldScroll) {
					return checkWhenEnabled = true;
				}
			};
			elemscroll.on('scroll', handler);
			scope.$on('$destroy', function() {
				return elemscroll.off('scroll', handler);
			});
			return $timeout((function() {
				if (attrs.infiniteScrollImmediateCheck) {
					if (scope.$eval(attrs.infiniteScrollImmediateCheck)) {
						return handler();
					}
				} else {
					return handler();
				}
			}), 2000);
		}
	};
}]);
/**
 * @license ng-bs-daterangepicker v0.0.1
 * (c) 2013 Luis Farzati http://github.com/luisfarzati/ng-bs-daterangepicker
 * License: MIT
 */
(function (angular) {
'use strict';

angular.module('ngBootstrap', []).directive('input', function ($compile, $parse) {
	return {
		restrict: 'E',
		require: '?ngModel',
		link: function ($scope, $element, $attributes, ngModel,$rootScope) {
			
			if ($attributes.type !== 'daterange' || ngModel === null ) return;

			var options = {};
			options.format = $attributes.format || 'YYYY-MM-DD';
			options.separator = $attributes.separator || ' - ';
			options.minDate = $attributes.minDate && moment($attributes.minDate);
			options.maxDate = $attributes.maxDate && moment($attributes.maxDate);
			options.dateLimit = $attributes.limit && moment.duration.apply(this, $attributes.limit.split(' ').map(function (elem, index) { return index === 0 && parseInt(elem, 10) || elem; }) );
			options.ranges = $attributes.ranges && $parse($attributes.ranges)($scope);
			options.locale = $attributes.locale && $parse($attributes.locale)($scope);
			options.opens = $attributes.opens && $parse($attributes.opens)($scope);
			
			function format(date) {
				return date.format(options.format);
			}

			function formatted(dates) {
				
				return [format(dates.startDate), format(dates.endDate)].join(" "+options.separator+" ");
			}

			ngModel.$formatters.unshift(function (modelValue) {
				if (!modelValue) return '';
				return modelValue;
			});

			ngModel.$parsers.unshift(function (viewValue) {
				return viewValue;
			});

			ngModel.$render = function () {
				if (!ngModel.$viewValue || !ngModel.$viewValue.startDate) return;
				$element.val(formatted(ngModel.$viewValue));
			};

			$scope.$watch($attributes.ngModel, function (modelValue) {
				
				if (typeof modelValue !="undefined" ) {
					
					if(!modelValue.startDate && !modelValue.endDate){
						
						
						var o = { startDate: moment($scope.date_to).format("YYYY-MM-DD"), endDate: moment($scope.date_from).format("YYYY-MM-DD") };
						ngModel.$setViewValue(o);
						
						
						$element.data('daterangepicker').startDate = moment($scope.date_to);
						$element.data('daterangepicker').endDate = moment($scope.date_from);
						$element.data('daterangepicker').updateView();
						$element.data('daterangepicker').updateCalendars();
						$element.data('daterangepicker').updateInputText();
					}
					
				}
				
			});
			
			
			$element.daterangepicker(options, function(start, end) {
				var o = { startDate: moment(start).format("YYYY-MM-DD"), endDate: moment(end).format("YYYY-MM-DD") };
				
				var text = o.endDate+" to "+ o.startDate;
				
				$scope.$apply(function () {

					$scope.filterDate(o.startDate,o.endDate);
					
					ngModel.$setViewValue(text);
					ngModel.$render();
					
					
				});
			});			
		}
	};
});

})(angular);
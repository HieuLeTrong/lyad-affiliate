

var headersHttp = {
	getHeaders: function(data){
		
		return {
			"Content-Type": "application/json",
			'Accept':'application/json'
		};
		
	},
	postHeaders: function(){
		var c = YouLookCookie.actions.getCookie("token");
		if(c != "") c = decodeURIComponent(c);
		c = c.replace(/"/g, '');
		return {
			'Accept':'application/json',
			"Access-Token": c,
			'Content-Type': 'application/x-www-form-urlencoded'
		};
	},
	putHeaders: function(){
		var c = YouLookCookie.actions.getCookie("token");
		if(c != "") c = decodeURIComponent(c);
		c = c.replace(/"/g, '');
		return {
			"Access-Token": c,
			'Content-Type': 'application/json'
		};
	},
	deleteHeaders: function(){
		var c = YouLookCookie.actions.getCookie("token");
		if(c != "") c = decodeURIComponent(c);
		c = c.replace(/"/g, '');
		return {
			'Accept':'application/json',
			"Access-Token": c
		};
	}
}
/** 
	YouLook: init project
**/
var Demo = angular.module('Demo', [
	'ui.router',
	'ngCookies',
	'ngSanitize',
	'ngResource'
]);


/**
	Router for YouLook site.
	The section use for rewrite url
**/

Demo.config(function($stateProvider, $urlRouterProvider,$locationProvider) {		
	$locationProvider.html5Mode(false).hashPrefix('!');
	$urlRouterProvider.otherwise('/');
	$stateProvider
		.state('home', {
			url: '/test',
			templateUrl: 'templates/homepage.html'
		});

		
});
Demo.controller('MainController', function($scope, $rootScope,$http,$state,$stateParams) {
	$scope.items = ['What was the name of your elementary school?','What was the name of your favorite childhood friend?','What was the name of your childhood pet?'];
	$scope.register = function(){
		console.log($scope)
		
	}
});
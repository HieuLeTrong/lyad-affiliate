<%@ language=vbscript %>
<%

  Response.Expires = 0
  Response.Buffer = true
  Response.Clear
  
  ddate = request("ddate")
  fdate = request("fdate")
  xdatediff =  datediff("d", ddate, fdate) 
  
  wcount = xdatediff
  
  if request("recul") = "" then wcount = 60 end if
	
  Set Chart = Server.CreateObject("csDrawGraph.Draw")

'/**** Bar Chart Properties (some also apply to line graphs)

  Chart.OriginX = 40
  Chart.OriginY = 200
  Chart.MaxX = 500
  Chart.MaxY = 170
	    
  'Chart.OriginX = 40
  'Chart.OriginY = 200
  'Chart.MaxX = 420
  'Chart.MaxY = 170
  Chart.BarWidth = 0
  Chart.BarGap = 0
  Chart.YTop = 0
  Chart.YGrad = 0
  Chart.XAxisText = ""
  Chart.YAxisText = ""
  Chart.AxisTextFont = "Arial"
  Chart.AxisTextSize = 8
  Chart.AxisTextBold = false
  Chart.AxisTextBGColor = "ffffff"
  Chart.AxisTextColor = "000000"
  Chart.LabelVertical = false
  Chart.ShowBarTotal = false
  Chart.VerticalBars = true
  Chart.XValuesVertical = true

'/**** Line Graph Properties

  Chart.XTop = 0
  Chart.XGrad = 0
  
  if xdatediff > 120 then
  
  	Chart.LineWidth = 1
  else
  	Chart.LineWidth = 2
  end if
  Chart.PointStyle = 0
  Chart.PointSize = 1
  Chart.UseXAxisLabels = false
  Chart.UseYAxisLabels = false
  Chart.XAxisNegative = 0
  Chart.YAxisNegative = 0
  Chart.XOffset = 0
  Chart.YOffset = 0
  Chart.XMarkSize = 4
  Chart.YMarkSize = 4
  
  Chart.ShowGrid = true
  Chart.GridStyle = 3
  Chart.GridColor = "999999"
  Chart.HideVGrid = true
  Chart.HideHGrid = false

'/**** Line Graph Text Properties
 
  Chart.LineGraphTextFont = "Arial"
  Chart.LineGraphTextBGColor = "ffffff"
  Chart.LineGraphTextColor = "000000"
  Chart.LineGraphTextSize = 7
  Chart.LineGraphTextBold = true
  Chart.LineGraphTextAlign = 0
  Chart.LineGraphTextX = 0
  Chart.LineGraphTextY = 0
  Chart.LineGraphTextBorder = false
  Chart.LineGraphTextLeader = true
  Chart.LineGraphBorderColor = "000000"
  Chart.LineGraphLeaderColor = "000000"

'/**** General Properties

  Chart.Width = 600
  Chart.Height = 230
  Chart.BGColor = "ffffff"
  Chart.PlotAreaColor = "ffffff"
  Chart.ShowPlotBorder = true
  Chart.GraphPen = "000000"
  Chart.LegendX = 350
  Chart.LegendY = 0
  Chart.Square = 8
  Chart.Padding = 5
  Chart.ShowLegend = true
  Chart.ShowLegendBox = false
  Chart.LegendTextSize = 8
  Chart.LegendFont = "Verdana"
  Chart.LegendBGColor = "ffffff"
  Chart.LegendColor = "000000"
  Chart.ShowNumbers = true
  Chart.ShowLabel = true
  Chart.LabelFont = "Arial"
  Chart.LabelSize = 8
  Chart.LabelBold = false
  Chart.LabelBGColor = "ffffff"
  Chart.LabelColor = "000000"
  Chart.Title = date &" "& time
  Chart.TitleX = 50
  Chart.TitleY = 5
  Chart.TitleFont = "Verdana"
  Chart.TitleSize = 8
  Chart.TitleBold = false
  Chart.TitleBGColor = "ffffff"
  Chart.TitleColor = "000000"
  Chart.TitleTextAlign = 0
  Chart.JpegQuality = 100
  Chart.RNDColor = 0
  Chart.Decimals = 0
  Chart.ShowSeparator = false
  Chart.Prefix = ""
  Chart.Suffix = ""
  Chart.Transparent = true
  Chart.TransColor = "ffffff"
  Chart.ShowPlotBorder = false

'/****  bipbip

	Set oConn = Server.CreateObject("ADODB.Connection")
	oconn.open "database=compteurs;dsn=compteurs;uid=sa;pwd=sa;" 

'/**** selection ****/'

	xname = "Visiteurs unique"
	color = "FF9600"
	CommandText = "EXEC full_visiteurs"

'/**** clics ****/'

	ycount = 1
	
	set RS = oConn.Execute(CommandText)
	
	count = wcount
	xcount = wcount+ycount
	
	do while not RS.EOF
	
		if xcount > 1 then
			Chart.AddPoint xcount, rs("ping"), color, xname
		end if
	
		if xcount=wcount+ycount then
		      Chart.AddLineGraphText rs("ping"), xcount+1, rs("ping")+5, 0
		end if	
	
	RS.MoveNext
	count = count - 1
	xcount = xcount - 1	
	loop
	
	
  Response.ContentType = "Image/Gif"
  Response.BinaryWrite Chart.GIFLine

  Response.End
  %>
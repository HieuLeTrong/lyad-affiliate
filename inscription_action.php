<?php
session_start();
require_once(dirname(__FILE__). '/global_conn.php');


$id_affilie = !empty($_SESSION['id_affilie']) ? $_SESSION['id_affilie'] : null;

$login=!empty($_POST["login"])?$_POST["login"]:"";
$pass=!empty($_POST["pass"])?$_POST["pass"]:"";
$prenom=!empty($_POST["prenom"])?$_POST["prenom"]:"";
$nom=!empty($_POST["nom"])?$_POST["nom"]:"";
$mail=!empty($_POST["mail"])?$_POST["mail"]:"";
$tel=!empty($_POST["tel"])?$_POST["tel"]:"";
$url=!empty($_POST["url"])?$_POST["url"]:"";
$adresse=!empty($_POST["adresse"])?$_POST["adresse"]:"";
$ville=!empty($_POST["ville"])?$_POST["ville"]:"";
$codepostal=!empty($_POST["codepostal"])?$_POST["codepostal"]:"";
$fax=!empty($_POST["fax"])?$_POST["fax"]:"";
// dump($_POST);
// setcookie("login", $login, time()+3600);
// setcookie("prenom", $prenom, time()+3600);
// setcookie("nom", $nom, time()+3600);
// setcookie("mail", $mail, time()+3600);
// setcookie("login", $login, time()+3600);
// setcookie("tel", $tel, time()+3600);
// setcookie("url", $url, time()+3600);
// setcookie("adresse", $adresse, time()+3600);
// setcookie("ville", $ville, time()+3600);
// setcookie("codepostal", $codepostal, time()+3600);
// setcookie("fax", $fax, time()+3600);

//---------------------------
function cnumeric($v){
  // extract($GLOBALS);
  if (is_numeric($v)){
    $function_ret=$v;
  }else{
    $function_ret=null;
  } 

  return $function_ret;
} 
//---------------------------

// function insert_base(){
	// extract($GLOBALS);
	$id = !empty($_SESSION['id_affilie_login_indispo']) ? cnumeric($_SESSION['id_affilie_login_indispo']):"";
	
	
	$rs = query("CALL p_af_add_new_affilie(@id_affilie,?,?,?,?,?,?,?,?,?,?,?,@SWP_Ret_Value)",array(
		array('value'=>$login, "type"=>PDO::PARAM_STR, "length"=>25),
		array('value'=>$pass, "type"=>PDO::PARAM_STR, "length"=>25),
		array('value'=>$prenom, "type"=>PDO::PARAM_STR, "length"=>25),
		array('value'=>$nom, "type"=>PDO::PARAM_STR, "length"=>25),
		array('value'=>$mail, "type"=>PDO::PARAM_STR, "length"=>50),
		array('value'=>$tel, "type"=>PDO::PARAM_STR, "length"=>30),
		array('value'=>$url, "type"=>PDO::PARAM_STR, "length"=>100),
		array('value'=>$adresse, "type"=>PDO::PARAM_STR, "length"=>200),
		array('value'=>$ville, "type"=>PDO::PARAM_STR, "length"=>30),
		array('value'=>$codepostal, "type"=>PDO::PARAM_STR, "length"=>25),		
		array('value'=>$fax, "type"=>PDO::PARAM_STR, "length"=>25)		
	),false,'@id_affilie,@SWP_Ret_Value');
	
	// dump($rs);
	$id_affilie = $rs["@id_affilie"];
	if($rs['@SWP_Ret_Value'] == 741){
		$_SESSION['id_affilie_login_indispo']=$id_affilie;
		require(dirname(__FILE__)."/inscriptions.php");
	}else{
		// send email
		$to = $mail;
		$subject = _t("Votre Inscription sur")." ".HOST;
		$body = _t("Cher")." ".$prenom.", <br><br>".

			_t("Votre demande de partenariat avec Lyad.com à bien été prise en compte")." <br><br>".

			_t("Vous pouvez dès à présent vous rendre sur l'espace membres du site").",<br>".
			_t("celui-ci est réservé aux partenaires.")."<br><br>".

			_t("Disponible sur cet espace :")."<br><br>".

			_t("- La mise à jour de vos données")." <br>".
			_t("- Les scripts pour les bandeaux")." <br>".
			_t("- Vos statistiques détaillées")." <br>".
			_t("- Des outils marketings")." <br>".
			_t("- L'état de vos gains")." <br><br>.".

			_t("L'adresse de l'espace membre")." :<br>
			".$assets->domain." <br>
			login : {$login} <br>
			password : {$pass} <br><br><br>".
			_t("Cordialement").", <br>
			Eric  <br>".
			 _t("Gestion Clientèle")."<br>
			<a href='".$assets->domain."'>".HOST."</a> <br><br>".

			_t("pour modifier ou rectifier les données vous concernant veuillez vous rendre sur l'espace membre du site.")." <br>".
			_t("pour vous désinscrire merci de nous ecrire en répondant à ce courrier.");

		
		mailer($to,$subject,$body,'',NAMEAPP);
				
		if($id_affilie!=""){
			$_SESSION['id_affilie']=$id_affilie;
			$_SESSION['id_affilie_login_indispo']="";
			$_SESSION['demo']="";
			$_SESSION['login']=$login;
			require_once(dirname(__FILE__)."/ok.php");
		}else{
			require_once(dirname(__FILE__)."/inscriptions.php");
		}
		
	}
	
	
	
	
	
//}
//-------------------------------------------------------------------------
// insert_base();
?>



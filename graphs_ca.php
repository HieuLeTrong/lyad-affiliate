<?php
  session_start();
  require(dirname(__FILE__). '/global_conn.php');
  require (dirname(__FILE__). '/jpgraph/jpgraph.php');
  require (dirname(__FILE__). '/jpgraph/jpgraph_line.php');
?>
<?php 
header("Expires: " . gmdate("D, d M Y H:i:s", time() + (0*60)) . " GMT"); ob_start();
ob_clean();
?>
<?php 
$id=!empty($_REQUEST["id"])?$_REQUEST["id"]:"";
if ($id==""){
  $id=$_SESSION['id'];
} 
$ddate=!empty($_REQUEST["ddate"])?$_REQUEST["ddate"]:"";
$fdate=!empty($_REQUEST["fdate"])?$_REQUEST["fdate"]:"";
 if(!empty($ddate) && !empty($fdate)){
	$date1 = explode("/",$ddate);
	$date2 = explode("/",$fdate);
	$date2=mktime(0,0,0,$date2[1],$date2[0],$date2[2]);
	$date1=mktime(0,0,0,$date1[1],$date1[0],$date1[2]);
	$d=$date2 - $date1;
	$xdatediff = round((int)($date2 - $date1)/(3600*24));
	$wcount = $xdatediff;
 }else{
	 $wcount=0;
 }
if (empty($_REQUEST["recul"])){
  $wcount=30;
}
$Title = "CA par semaine ces " . $xdatediff . " derniers jours / du " . $ddate . " et " . $fdate;

$rs = query("CALL p_af_eric_select_ca_new(?,?)",array(
			array('value'=>$id,"type"=>PDO::PARAM_STR,"length"=>255),
			array('value'=>$xdatediff+30,"type"=>PDO::PARAM_STR,"length"=>255)
		),false);
// Setup the graph
		$graph = new Graph(520,230);
		if(!empty($rs)){
			//$graph->SetScale("textlin");
			$graph->SetScale("int");
			$graph->yaxis->scale->SetAutoMin(0);
		}else{
			$graph->SetScale('int',0,1,0,1);
		}
				
		$graph->img->SetAntiAliasing(false);
		// $graph->SetMargin(0,0,0,0);		
		$graph->title->Set($Title);
		$graph->title->SetColor("#000000");
		$graph->title->SetFont(FF_ARIAL,FS_NORMAL,8);
		$graph->title->SetPos('left');
		$graph->SetBox(false);
		
		//$graph->xaxis->SetTextLabelInterval(3);
		//$graph->xaxis->SetTextTickInterval(3,-1);
		$graph->ygrid->SetLineStyle("dotted");
		$graph->ygrid->SetColor('#464637');
		$graph->xaxis->scale->ticks->SetSize(8,3);
		$graph->xaxis->SetLabelAngle(90);
		
		
 $ca =  array();
 $xnew =  array();
 $re =  array();
if(!empty($rs)){
	 foreach($rs as $item){
		if ($_REQUEST["ca"] == 1){
			//array_push($ca,($item["montant_paiement"]+$item["montant_paiement_auto"]));
			array_unshift($ca,($item["montant_paiement"]+$item["montant_paiement_auto"]));
		}	

		if  ($_REQUEST["new"] == 1){
			//array_push($xnew, $item["montant_paiement"]);
			array_unshift($xnew, $item["montant_paiement"]);
		}

		if  ($_REQUEST["re"] == 1){
			//array_push($re, $item["montant_paiement_auto"]);
			array_unshift($re, $item["montant_paiement_auto"]);
		}
	 }
 }
if(!empty($ca)){
	// scale ticks
	if(count($ca) >= 365)
			$graph->xaxis->SetTextTickInterval(20,-1);
        elseif(count($ca) >= 150)
            $graph->xaxis->SetTextTickInterval(15,-1);
        elseif(count($ca) >= 120)
            $graph->xaxis->SetTextTickInterval(12,-1);
        elseif(count($ca) >= 90)
			$graph->xaxis->SetTextTickInterval(9,-1);
        elseif(count($ca) >= 60)
           $graph->xaxis->SetTextTickInterval(6,-1);
        elseif(count($ca) >= 30)
            $graph->xaxis->SetTextTickInterval(3,-1);
	//Create the ca line
	if(count($ca) <= 1){
		array_unshift($ca, 0);
	}
	$p1 = new LinePlot($ca);
	$graph->Add($p1);
	$p1->SetWeight(2); 
	$p1->SetColor("#8000a0");
	$p1->SetLegend("CA TTC");
	$p1->SetStyle("solid"); 
}
if(!empty($xnew)){
	// scale ticks
	if(count($xnew) >= 365)
			$graph->xaxis->SetTextTickInterval(20,-1);
        elseif(count($xnew) >= 150)
            $graph->xaxis->SetTextTickInterval(15,-1);
        elseif(count($xnew) >= 120)
            $graph->xaxis->SetTextTickInterval(12,-1);
        elseif(count($xnew) >= 90)
			$graph->xaxis->SetTextTickInterval(9,-1);
        elseif(count($xnew) >= 60)
           $graph->xaxis->SetTextTickInterval(6,-1);
        elseif(count($xnew) >= 30)
            $graph->xaxis->SetTextTickInterval(3,-1);
	//Create the xnew line
	if(count($xnew) <= 1){
		array_unshift($xnew, 0);
	}
	$p2 = new LinePlot($xnew);
	$graph->Add($p2);
	$p2->SetWeight(2); 
	$p2->SetColor("#D900D9");
	$p2->SetLegend("Nouveaux");
	$p2->SetStyle("solid"); 
}
if(!empty($re)){
	// scale ticks
	if(count($re) >= 365)
			$graph->xaxis->SetTextTickInterval(20,-1);
        elseif(count($re) >= 150)
            $graph->xaxis->SetTextTickInterval(15,-1);
        elseif(count($re) >= 120)
            $graph->xaxis->SetTextTickInterval(12,-1);
        elseif(count($re) >= 90)
			$graph->xaxis->SetTextTickInterval(9,-1);
        elseif(count($re) >= 60)
           $graph->xaxis->SetTextTickInterval(6,-1);
        elseif(count($re) >= 30)
            $graph->xaxis->SetTextTickInterval(3,-1);
	//Create the re line
	if(count($re) <= 1){
		array_unshift($re, 0);
	}
	$p3 = new LinePlot($re);
	$graph->Add($p3);
	$p3->SetWeight(2); 
	$p3->SetColor("#999999");
	$p3->SetLegend("Rebills");
	$p3->SetStyle("solid"); 
}
// $graph->legend->SetFrameWeight(1);
$graph->legend->Pos( 0,0,"right","top");
$graph->legend->SetLineWeight(10);
$graph->legend->SetLayout(LEGEND_VERT);
$graph->legend->SetLineSpacing(3);
// Output line
$graph->Stroke(); 

?>


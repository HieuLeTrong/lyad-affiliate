<?php
if(!isset($_SESSION)){
    session_start();
	
}
require_once(dirname(__FILE__). '/global_conn.php');

?>
<?php 


$parrain=!empty($_POST["affiliation"]["parrain"]) ? $_POST["affiliation"]["parrain"] : "";

$login=!empty($_POST["login"]) ? $_POST["login"] : "";
$pass=!empty($_POST["pass"])?$_POST["pass"]:"";
$prenom=!empty($_POST["prenom"])?$_POST["prenom"]:"";
$nom=!empty($_POST["nom"])?$_POST["nom"]:"";
$mail=!empty($_POST["mail"])?$_POST["mail"]:"";
$tel=!empty($_POST["tel"])?$_POST["tel"]:"";
$url=!empty($_POST["url"])?$_POST["url"]:"";
$adresse=!empty($_POST["adresse"])?$_POST["adresse"]:"";
$ville=!empty($_POST["ville"])?$_POST["ville"]:"";
$codepostal=!empty($_POST["codepostal"])?$_POST["codepostal"]:"";
$conditions=!empty($_POST["conditions"])?$_POST["conditions"]:"";
$id_affilie_login_indispo = !empty($_SESSION['id_affilie_login_indispo'])?$_SESSION['id_affilie_login_indispo']:"";
$msg_err = "";
if ($id_affilie_login_indispo!="" && $login!=""){
 $msg_err="<b>"._t("Désolé !")."</b><br>"._t("Veuillez")." <b>"._t("saisir un nouveau login")."</b> "._t("celui-ci n'est pas disponible.");
} 
//var_dump($_SESSION);
?>

<html>
<head>
<title><?php echo _t("Lyad.com // L'affiliation"); ?></title>
<META http-equiv=Page-Enter content=blendTrans(Duration=1.0)>


<script language="JavaScript">
function stopError() {return true;}
//window.onerror = stopError;

function IsValidEmail(strEmail){
	var emailfilter  = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9])+$/; 
	return emailfilter.test(strEmail)
}

function check_and_submit(){
  
	var f=document.theform;
	var msg_err="";
	var nb_er=0;

	if (f.conditions.checked==false){
			nb_er++;
	      		msg_err = '<?php echo _t("- Cochez la case pour accepter les <b>conditions</b>"); ?>';
	      		f.conditions.focus();
	      		}

	if (f.url.value == ""){
		nb_er++;
      		msg_err = '<?php echo _t("- Champ <b>url</b> vide"); ?>';
      		f.url.focus();
      		}

	if (f.tel.value == ""){
		nb_er++;
      		msg_err = '<?php echo _t("- Champ <b>téléphone</b> vide<br>"); ?>'+msg_err;
      		f.tel.focus();
      		}

	else if (f.tel.value!=""){
			var tel_car = new String("0123456789-. ");
			var tel_pas_bon;
			for(i=0; i<f.tel.value.length; i++){
					if( tel_car.indexOf(f.tel.value.charAt(i)) == -1 ){tel_pas_bon = 1;}
					}
			if(tel_pas_bon == 1||f.tel.value.length < 4){
					nb_er++;
					msg_err = '<?php echo _t("- le <b>téléphone</b> est incorrecte<br>"); ?>'+msg_err;
					f.tel.focus();
					}
	}
	
	if (f.mail.value == ""){nb_er++; msg_err = '<?php echo _t("- Champ <b>email</b> vide<br>"); ?>'+msg_err;f.mail.focus();}
		
	else if(f.mail.value !=""){
					if (!IsValidEmail(f.mail.value))
							{nb_er++; msg_err = '<?php echo _t("- <b>email</b> incorrecte<br>"); ?>'+msg_err;f.mail.focus();}
				}
	
	if (f.nom.value == ""){
			nb_er++;
	      		msg_err = '<?php echo _t("- Champ <b>Nom</b> vide<br>"); ?>'+msg_err;
	      		f.nom.focus();
	      		}
	else if (f.nom.value!=""){
					if(f.nom.value.length < 3){
					nb_er++;
					msg_err = '<?php echo _t("- le <b>nom</b> est incorrecte<br>"); ?>'+msg_err;
					f.nom.focus();
					}
		}
	
	if (f.prenom.value == ""){
			nb_er++;
	      		msg_err = '<?php echo _t("- Champ <b>prénom</b> vide<br>"); ?>'+msg_err;
	      		f.prenom.focus();
	      		}
	else if (f.prenom.value!=""){
						if(f.prenom.value.length < 3){
						nb_er++;
						msg_err = '<?php echo _t("- le <b>prénom</b> est incorrecte<br>"); ?>'+msg_err;
						f.prenom.focus();
						}
		}
	
	if (f.pass.value == ""){
			nb_er++;
	      		msg_err = '<?php echo _t("- Champ <b>pass</b> vide.<br>"); ?>'+msg_err;
	      		f.pass.focus();
	      		}
	else if (f.pass.value!=""){
						if(f.pass.value.length < 3){
							nb_er++;
							msg_err = '<?php echo _t("- le <b>pass</b> est trop court<br>"); ?>'+msg_err;
							f.pass.focus();
							}
						else if (f.pass.value != f.repass.value){
							nb_er++;
							msg_err = '<?php echo _t("- les 2 <b>pass</b> saisis ne sont pas identiques<br>"); ?>'+msg_err;
							f.pass.focus();
							}
	}
	
	if (f.login.value == ""){
						nb_er++;
	      		msg_err = '<?php echo _t("- Champ <b>login</b> vide<br>"); ?>'+msg_err;
	      		f.login.focus();
	      		}
	
	else if(f.login.value != ""){

				if(f.login.value.length < 3){
				nb_er++;
				msg_err = '<?php echo _t("- le <b>login</b> est trop court<br>"); ?>'+msg_err;
				f.login.focus();
				}

				var carp = new String("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_");
						var err="";
						for(i=0; i< f.login.value.length; i++){
							if( carp.indexOf(f.login.value.charAt(i)) == -1 ){
								err = err + ""+ f.login.value.charAt(i);				
								}
						}
					if(err!=""){nb_er++;msg_err =  '<?php echo _t("- Carractere(s) interdit <b>"); ?>' +err+'<?php echo _t("</b> dans le <b>login</b><br>"); ?>'+msg_err;}
					//if(err!=""){nb_er++;msg_err =  "- Carractere(s) interdit  \"<b>" +err+ "</b>\" dans le <b>login</b><br>";}
		}

	if (nb_er!=0&&nb_er<8){
			document.getElementById("div_err").innerHTML = '<?php echo _t("<b>ERREUR !</b><br>"); ?>'+msg_err;
			document.getElementById("snd").src = "snd_erreur.wav";
			return (false);
			}
	else if (nb_er!=0){
			document.getElementById("div_err").innerHTML = '<?php echo _t("<b>ERREUR !</b><br> Renseignez tous les champs avec <b>*</b>"); ?>';
			document.getElementById("snd").src = "snd_erreur.wav";
			return (false);
			}
	else{
			document.getElementById("div_err").innerHTML = "<font class=clerreur ><b>"+'<?php echo _t("Enregistrement en cours ..."); ?>'+"</b></font>";;
			f.submit.disabled= true;
			//document.forms.theform.submit();
			return (true);
			//alert("ok");
			//return (false);
			}
}

function press(e) {
	if (document.all){
		if (event.keyCode == 13) {check_and_submit();} 	//entre
		}
}
//document.onkeydown=press;

function open_cond(){
	//window.open('conditions.asp','conditions','width=600,height=500,resizable=yes,scrollbars=yes,left=10,top=10,screenX=10,screenY=10');
	window.open('conditions.php','','width=600,height=500,resizable=yes,scrollbars=yes,left=10,top=10,screenX=10,screenY=10');
}

</script>
<link href="/affiliation.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--

.Style1 {color: #666666}

-->
</style>
</head>

<body bgcolor="#FFFFFF" onLoad="theform.login.focus();" topmargin=7 leftmargin=5 class="body1" oncontextmenu="return false">

<bgsound id="snd" src="" volume=0>

<img src="images/inscriptions<?php if($lang!='fr') echo '_'.$lang  ?>.jpg" width="545" height="20">

<br>
<table width="545" height="96%" border="0" cellpadding="0" cellspacing="0" align="center">
<tr>
<td height="10%">&nbsp;</td>
</tr>
<tr><td align="center" valign="top">

   
      <form name="theform" method="POST" action="/inscription_action.php" onSubmit="return check_and_submit();" >
        <table width="326" border="0" cellspacing="0" cellpadding="0" align="center">
          <tr valign="top">
            <td colspan="3" height="42">
              <div align="right"><img src="images/coordonnees<?php if($lang!='fr') echo '_'.$lang  ?>.gif" width="460" height="24"><br>
            </div></td>
          </tr>
          <tr>
            <td width="163" class="texte1">
              <div align="right"><strong><?php echo _t("Login"); ?></strong><font color="RED">*</font></div></td>
            <td width="19"></td>
            <td width="144">
              <input type="text" name="login" value="<?php echo !empty($_POST["login"])?$_POST["login"]:"";?>" MAXLENGTH="25" SIZE="17">
            </td>
          </tr>
          <tr>
            <td width="163" class="texte1">
              <div align="right"><strong><?php echo _t("Password"); ?></strong><font color="RED">*</font></div></td>
            <td width="19"></td>
            <td width="144">
              <input name="pass" type="password" value="" MAXLENGTH="25" SIZE="17" >
            </td>
          </tr>
          <tr>
            <td width="163" class="texte1">
              <div align="right"><strong><?php echo _t("Retapez-le"); ?></strong><font color="RED">*</font></div></td>
            <td width="19"></td>
            <td width="144">
              <input name="repass" type="password" value="" MAXLENGTH="25" SIZE="17" >
            </td>
          </tr>
          <tr>
            <td width="163" class="texte1">
              <div align="right"><strong><?php echo _t("Prénom"); ?></strong><font color="RED">*</font></div></td>
            <td width="19"></td>
            <td width="144">
              <input name="prenom" type="text" value="<?php echo !empty($_POST["prenom"])?$_POST["prenom"]:"";?>" MAXLENGTH="25" SIZE="17">
            </td>
          </tr>
          <tr>
            <td width="163" class="texte1">
              <div align="right"><strong><?php echo _t("Nom"); ?></strong><font color="RED">*</font></div></td>
            <td width="19"></td>
            <td width="144">
              <input name="nom" type="text" value="<?php echo !empty($_POST["nom"])?$_POST["nom"]:"";?>" MAXLENGTH="25" SIZE="17">
            </td>
          </tr>
          <tr>
            <td width="163" class="texte1">
              <div align="right"><strong><?php echo _t("E-Mail (valide)"); ?></strong><font color="RED">*</font></div></td>
            <td width="19"></td>
            <td width="144">
              <input type="text" name="mail" value="<?php echo !empty($_POST["mail"])?$_POST["mail"]:"";?>" MAXLENGTH="50" SIZE="30">
            </td>
          </tr>
          <tr>
            <td width="163" class="texte1">
              <div align="right"><strong><?php echo _t("Téléphone"); ?></strong><font color="RED">*</font></div></td>
            <td width="19"></td>
            <td width="144">
              <input name="tel" type="text" value="<?php echo !empty($_POST["tel"])?$_POST["tel"]:"";?>" MAXLENGTH="30" SIZE="17">
            </td>
          </tr>
          <tr>
            <td width="163" class="texte1">
              <div align="right"><strong><?php echo _t("Url de votre site"); ?></strong><font color="RED">*</font></div></td>
            <td width="19">&nbsp;</td>
            <td width="144">
              <input type="text" name="url" value="<?php echo !empty($_POST["url"])?$_POST["url"]:"";?>" MAXLENGTH="100" SIZE="30">
            </td>
          </tr>
          <tr>
            <td width="163" height="2" class="texte1">
              <div align="right"><?php echo _t("Adresse"); ?></div></td>
            <td width="19" height="2">&nbsp;</td>
            <td width="144" height="2">
              <input type="text" name="adresse" value="<?php echo !empty($_POST["adresse"])?$_POST["adresse"]:"";?>" MAXLENGTH="100" SIZE="30" >
            </td>
          </tr>
          <tr>
            <td width="163" class="texte1">
              <div align="right"><?php echo _t("Ville"); ?></div></td>
            <td width="19">&nbsp;</td>
            <td width="144">
              <input type="text" name="ville" value="<?php echo !empty($_POST["ville"])?$_POST["ville"]:"";?>" MAXLENGTH="30" SIZE="17">
            </td>
          </tr>
          <tr>
            <td width="163" class="texte1">
              <div align="right"><?php echo _t("Code Postal"); ?></div></td>
            <td width="19">&nbsp;</td>
            <td width="144">
              <input name="codepostal" type="text" value="<?php echo !empty($_POST["codepostal"])?$_POST["codepostal"]:"";?>" MAXLENGTH="25" SIZE="17">
            </td>
          </tr>
          <tr>
            <td width="163" class="texte1">
              <div align="right"><?php echo _t("Fax"); ?></div></td>
            <td width="19">&nbsp;</td>
            <td width="144">
              <input type="text" name="fax" value="<?php echo !empty($_POST["fax"])?$_POST["fax"]:"";?>" MAXLENGTH="25" SIZE="17">
            </td>
          </tr>
          <tr>
            <td colspan="3" class="texte1" height="3"></td>
          </tr>
          <tr>
            <td colspan="3" class="texte1">
              <div align="center">          <br>
          <table width="460" border="0" align="center" cellpadding="0" cellspacing="0" background="images/acceptation<?php if($lang!='fr') echo '_'.$lang  ?>.gif">
            <tr>
              <td width="460" height="23"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="8%">&nbsp;</td>
                    <td width="26%"><input type="checkbox" name="conditions" value="checked" <?php echo $conditions;?> ></td>
                    <td width="51%">
			<a href="/conditions.php" target="blank" onClick="open_cond();return(false);" ><img src="images/tr.gif" width="98%" height="16" border="0"></a>
		    </td>
                    <td width="15%"></td>
                  </tr>
              </table></td>
            </tr>
          </table>
            </div></td>
          </tr>
          <tr>
            <td colspan="3" class="texte1" height="6"><div ID="div_err" class="clerreur" =<?php echo !empty($msg_err)?$msg_err:"";?></DIV> </td>
          </tr>
        </table>
        <div align="center">	
		  <table width="460" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
              <td width="460" height="23" background="images/validation<?php if($lang!='fr') echo '_'.$lang  ?>.gif"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="5%">&nbsp;</td>
                  <!--<td width="55%"><a href="#" onClick= "check_and_submit();"><img src="images/tr.gif" width="100%" height="16" border="0"></a></td>-->
                  
                  <td width="55%"><input type="image" src="images/tr.gif" width="100%" height="16" value="bt_submit"></td>
                  
                  
                  
                  <td width="40%">&nbsp;</td>
                </tr>
              </table></td>
            </tr>
          </table>
</div>
      </form>
      <br>
      <div class="copyright">
        <div align="center"><font color="#000000"><span class="Style1"><?php echo _t("Lyad&reg;&#8482; All Rights Reserved / Tous Droits Résevés &copy;"); ?></span> </font><font color="RED">&nbsp;&nbsp;</font><font color="#000000"><span class="Style1"> </span>  // </font><font color="RED">&nbsp;&nbsp;*</font> <font color="#000000">Champs Obligatoires</font></div>
    </div></td>
  </tr>
</table>

</body>
</html>



<?php
  session_start();require(dirname(__FILE__). '/global_conn.php');
  //session_register("parrain_session");
?>
<?php 
//option explicit

print "<!--ddqq75-->";

$login= !empty($_COOKIE["visit"]["login"])?$_COOKIE["visit"]["login"]:"";

if ($login!=""){
	//response.redirect("membres.asp")
	header("Location: membres.php"); exit();

} 
if(!empty($_REQUEST["id"])){
	$_SESSION['parrain'] = $_REQUEST["id"];
}

if (!empty($_SERVER["HTTP_REFERER"])&&substr($_SERVER["HTTP_REFERER"],0,26)=="http://fr.search.yahoo.com")
{

	//response.write "<iframe src=http://www.lyad.com/rubriques/membres/vitrine_rv.asp width=620 height=70 border=0 MARGINWIDTH=0 MARGINHEIGHT=0 cellpadding=0 cellspacing=0 scrolling=no></iframe>"
	print "- Pour acceder à Lyad.com :<br><a href=http://www.lyad.com target=_blank><img src=images/lyad_deroul.jpg width=600 border=0></a><br><br>";
} 

//response.write request.ServerVariables("HTTP_REFERER")
?>

<html>
<head>
<title><?php echo _t("Lyad.com // L'Affiliation Rencontres"); ?></title>
<META http-equiv=Page-Enter content=blendTrans(Duration=1.0)>
<STYLE TYPE="text/css"> A {color:#ffffff;} A:hover {color:#cccccc} </STYLE>

<link href="affiliation.css" rel="stylesheet" type="text/css">

<style type="text/css">
<!--

.Style3 {color: #000000}

-->
</style>
</head>

<body bgcolor="#FFFFFF" TOPMARGIN=7 leftmargin=5 MARGINWIDTH=0 MARGINHEIGHT=0 class="body1">
<a name="top"></a>
<div align="left"><span class="faq"></span><img src="images/conditions_affiliation.gif" width="545" height="20">    <br>
</div>
<table width="95%" border="0" align="center"  cellpadding="0" cellspacing="0">
<tr>
<td height="10%"><p class="faq">    <strong><br>
  - <a href="#identification" class="Style3"><?php echo _t("Identification des parties"); ?>  </a></strong><br>
<strong>- <a href="#eligibilite" class="Style3"><?php echo _t("Eligibilité"); ?>  </a></strong><br>
<strong>- <a href="#demande" class="Style3"><?php echo _t("Demande d'inscription"); ?>  </a></strong><br>
<strong>- <a href="#donnees" class="Style3"><?php echo _t("Donnees personnelles"); ?>  </a></strong><br>
<strong>- <a href="#conditions" class="Style3"><?php echo _t("Conditions de rémunération"); ?>  </a></strong><br>
<strong>- <a href="#facturation" class="Style3"><?php echo _t("Facturation"); ?>  </a></strong><br>
<strong>- <a href="#paiement" class="Style3"><?php echo _t("Conditions de paiement"); ?>  </a></strong><br>
<strong>- <a href="#commissions" class="Style3"><?php echo _t("Commissions"); ?> </a></strong><br>
<strong>- <a href="#parrainage" class="Style3"><?php echo _t("Parrainages"); ?> </a></strong><br>
<strong>- <a href="#fraudes" class="Style3"><?php echo _t("Fraudes et charge back"); ?> </a></strong><br>
<strong>- <a href="#duree" class="Style3"><?php echo _t("Durée du contrat et résiliation"); ?>  </a></strong><br>
<strong>- <a href="#desinscription" class="Style3"><?php echo _t("Desinscription"); ?> </a></strong><br>
<strong>- <a href="#propriete" class="Style3"><?php echo _t("Propriete intellectuelle"); ?> </a></strong><br>
<strong>- <a href="#modifications" class="Style3"><?php echo _t("Modification du present contrat"); ?> </a></strong><br>
<strong>- <a href="#litiges" class="Style3"><?php echo _t("Litiges"); ?> </a></strong><br>
<strong>- <a href="#contact" class="Style3"><?php echo _t("Nous contacter"); ?> </a></strong></p>
    <hr>
    <p class="faq"><?php echo _t("Le présent contrat, représente le contrat d'adhésion au programme d'affiliation que lyad.com met à votre disposition. Votre adhésion au programme d'affiliation suppose votre acceptation des présentes conditions générales d'affiliation. Celui-ci est non exclusif et de durée indéterminée."); ?> </p>    
    <p class="faq"><strong><strong><strong><a name="identification" id="identification"></a></strong></strong><?php echo _t("Article"); ?> 1 </strong>//        <strong><?php echo _t("IDENTIFICATION DES PARTIES"); ?> <strong><strong><strong><a href="#top"><img src="images/top.jpg" width="11" height="9" border="0"></a></strong></strong></strong></strong></p>
        <blockquote>
          <p class="faq"><?php echo _t("Le présent protocole d'affiliation est établi entre "); ?>: <br>
            <br>
            - <?php echo _t("Lyad Affiliation, une marque de Internet SARL, au capital de 7 700 euros immatriculée au registre du commerce et des sociétés de Grenoble sous le numéro 437 639 214 00026 / 722 Z."); ?> <br>
            <br>
            - <?php echo _t("L'Affilié : la société, l'association mentionné dans le formulaire d'inscription, qui est le gestionnaire d'un site Internet."); ?> </p>
        </blockquote>      
        <p class="faq"><strong><strong><strong></strong></strong><strong><strong><strong><strong><strong><a name="eligibilite" id="eligibilite"></a></strong></strong></strong></strong></strong><?php echo _t("Article"); ?> 2 </strong>//        <strong><?php echo _t("ELIGIBILITE"); ?> <strong><strong><strong><a href="#top"><img src="images/top.jpg" width="11" height="9" border="0"></a></strong></strong></strong></strong></p>
        <blockquote>
          <p class="faq"><?php echo _t("Toutes les entreprises, associations et autres structures sont admises à notre programme d'affiliation à condition de respecter les présentes conditions. Les entreprises étrangères sont également acceptées, cependant le mode de facturation est différent et devra s'effectuer en Euros Hors Taxes."); ?> </p>
          <p class="faq"><?php echo _t("En revanche, les particuliers ne peuvent pas adhérer à notre programme."); ?> </p>
        </blockquote>      
        <p class="faq"><strong><strong><strong></strong></strong><strong><strong><strong><strong><a name="demande" id="demande"></a></strong></strong></strong></strong><?php echo _t("Article"); ?> 3 </strong>//        <strong><?php echo _t("DEMANDE D'INSCRIPTION"); ?> <strong><strong><strong><a href="#top"><img src="images/top.jpg" width="11" height="9" border="0"></a></strong></strong></strong></strong></p>
        <blockquote>
          <p class="faq"><?php echo _t("Les demandes d'inscription se font en remplissant le formulaire d'inscription disponible sur le site http://affiliation.lyad.com"); ?> <br>
            <br>
            <?php echo _t("La validation électronique des Conditions Générales d'Affiliation ainsi que du formulaire d'inscription entra&icirc;ne l'acceptation sans réserve des conditions générales d'affiliation."); ?> <br>
            <br>
            <?php echo _t("Chaque demande sera évaluée par nos services, Lyad.com se donnant le droit de refuser un site, si celui-ci ne correspondait pas à notre éthique"); ?> : <br>
            - <?php echo _t("Site à caractère illégal"); ?>, <br>
            - <?php echo _t("Site à caractère pornographique"); ?>, <br>
            - <?php echo _t("Site discriminatoire, tant sur la race, le sexe, la religion, la nationalité, une incapacité ou un handicap quelconque, l'orientation sexuelle, ou l'âge"); ?>, <br>
            - <?php echo _t("Site nuisant à l'image de Lyad.com"); ?>, <br>
            - <?php echo _t("Site non conforme aux lois et règlements en vigueur et aux droits des tiers"); ?>, </p>
          <p class="faq"><?php echo _t("ne peuvent pas adhérer au programme d'affiliation"); ?>. <br>
            <br>
          <?php echo _t("Si vous êtes accepté par le programme, nous nous réservons le droit d'annuler votre adhésion à partir du moment où votre site cesserait de respecter les conditions ci-dessus."); ?></p>
      </blockquote>      
        <p class="faq"><strong><strong><strong></strong></strong><strong><strong><strong><strong><a name="donnees" id="donnees"></a></strong></strong></strong></strong><?php echo _t("Article"); ?> 4 </strong>//        <strong><?php echo _t("DONNEES PERSONNELLES"); ?> <strong><strong><strong><a href="#top"><img src="images/top.jpg" width="11" height="9" border="0"></a></strong></strong></strong></strong></p>
      <blockquote>
        <p class="faq"><?php echo _t("Lors de son inscription au programme Lyad Affiliation, l'affilié est amené à renseigner des données personnelles, telles que ses noms et prénoms, son adresse postale et son téléphone."); ?> </p>
        <p class="faq"><?php echo _t("Les données nominatives demandées à l'affilié pour l'ouverture de son compte&nbsp;sont destinées exclusivement à Lyad Affiliation et, en aucun cas, ne seront transmises à des tiers à quelque fin que ce soit. Le traitement de ces données par Lyad Affiliation est soumis à la loi n°78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés. En application des dispositions de l'article 34 de cette loi, l'affilié peut à tout moment accéder aux données qui le concernent pour les consulter et, le cas échéant, les rectifier."); ?></p>
        <p class="faq"><?php echo _t("Lyad Affiliation s'engage à n'utiliser les données à caractère personnel de l'affilié que pour les strictes finalités du programme d'affiliation. Lyad Affiliation s'engage expressément à ne pas procéder à des traitements, de manière incompatible avec les finalités du programme, à ne pas publier, divulguer ou transmettre d'informations concernant l'affilié, sans son accord préalable."); ?></p>
        <p class="faq"><?php echo _t("L'affilié est bénéficiaire d'un compte membre accessible grâce à un login et un password."); ?> </p>
        <p class="faq"><?php echo _t("Ces paramètres d'accès au compte sont personnels, confidentiels et non transmissibles. Par conséquent, le titulaire du compte est tenu de préserver la confidentialité de ces informations, et s'engage à ne pas céder, ni communiquer à un tiers ses paramètres d'accès et demeure seul responsable de l'utilisation de ces derniers. . L'utilisation de son login et de son password à travers Internet se fait aux risques et périls de l'affilié. Il appartient à celui-ci de prendre toutes les dispositions nécessaires permettant de protéger ses propres données contre toute atteinte."); ?> </p>
        <p class="faq"><?php echo _t("Lyad Affiliation ne saurait être tenue responsable de l'utilisation faite par un tiers en possession des paramètres d'accès de l'affilié."); ?> <br>
          <?php echo _t("L'affilié s'engage à informer immédiatement Lyad Affiliation de toute utilisation non autorisée de ses paramètres d'accès."); ?> </p>
        <p class="faq"><?php echo _t("En cas de litige, ou usage frauduleux ou détournement litigieux au profit de tiers du programme fait au nom du titulaire du compte Lyad Affiliation, Lyad Affiliation se réserve le droit de suspendre le compte de l'affilié après en avoir informé le titulaire."); ?> </p>
      </blockquote>      
      <p class="faq"><strong><strong><strong><strong><a name="conditions" id="conditions"></a></strong></strong></strong><?php echo _t("Article"); ?> 5 </strong>//        <strong><?php echo _t("CONDITIONS DE R&Eacute;MUN&Eacute;RATION"); ?> <strong><strong><strong><a href="#top"><img src="images/top.jpg" width="11" height="9" border="0"></a></strong></strong></strong></strong></p>
      <blockquote>
        <p class="faq"><?php echo _t("La rémunération de l'affilié se fait selon un pourcentage sur le chiffre d'affaires réalisé grâce à ses visiteurs. Le chiffre d'affaires correspond essentiellement aux inscriptions à Lyad.com effectivement transformées sur les 2 années suivant l'inscription d'un membre."); ?></p>
        <p class="faq"><?php echo _t("Pour définir votre taux de rémunération et votre taux de rétrocession, contactez nous.<br>A savoir que le taux de commissionnement est de 50% par défaut."); ?><br><br>
        <?php echo _t("Les conditions de reversement exceptionnel(supérieur à 50%) attribué à un affilié peuvent être revu à tout moment mais la modification pourra être effective uniquement à partir du début du mois suivant, la plateforme devra au préalable prévenir l'affilié d'une telle modification par email(email d'inscription de l'affilié) et devra en expliquer les raisons(les raisons courantes peuvent être par exemple l'arrêt d'envoi de traffic ou une diminution substentielle de celui-ci sur le support Lyad.com ou tout autres raisons raisonnablement justifié)."); ?><br><br>
          <?php echo _t("Si, à n'importe quel moment, vous désapprouvez ou ne voulez plus être lié par cet accord, votre seul recours est de mettre fin à votre participation au programme."); ?><br>
          <br>
          <?php echo _t("Lyad Affiliation donne accès à chaque affilié via son &quot;Espace Membres&quot; aux outils promotionnels (Liens) et au reporting sur le trafic et les inscriptions que vous générez (ceux-ci sont aisément identifiables par la présence d'une donnée d'appel à votre nom (cookies) dans l'adressage vers affiliation.lyad.com)."); ?><br><br>
      <?php echo _t("En cas de conflit en rapport avec la rémunération de l'affilié se reporter à l'article 15 des présentes conditions."); ?></p>
      </blockquote>      
      <p class="faq"><strong><strong><strong><strong><a name="facturation" id="facturation"></a></strong></strong></strong><?php echo _t("Article"); ?> 6 </strong>//        <strong><?php echo _t("FACTURATION"); ?> <strong><strong><strong><a href="#top"><img src="images/top.jpg" width="11" height="9" border="0"></a></strong></strong></strong></strong></p>
      <blockquote>
        <p class="faq"><?php echo _t("Des appels à factures sont disponibles sur la zone membre de affiliation.lyad.com. Le 05 de chaque mois, ceux-ci sont disponibles directement dans l'onglet facturation de la zone membre de l'affilié."); ?> </p>
        <p class="faq"><?php echo _t("L'affilié devra faire parvenir ses factures à l'adresse suivante&nbsp;: Lyad affiliation, 4 Rue Galvani 75017 Paris France."); ?> </p>
      </blockquote>      
      <p class="faq"><strong><strong><strong><strong><a name="paiement" id="paiement"></a></strong></strong></strong><?php echo _t("Article"); ?> 7 </strong>//        <strong><?php echo _t("CONDITIONS DE PAIEMENT"); ?> <strong><strong><strong><a href="#top"><img src="images/top.jpg" width="11" height="9" border="0"></a></strong></strong></strong></strong></p>
      <blockquote>
        <p class="faq"><?php echo _t("Les sommes dues seront réglées à 60 jours fin de mois, après réception de votre facture."); ?> </p>
        <p class="faq"><?php echo _t("Le paiement est réalisé soit par virement bancaire, soit par chèque selon le choix de l'affilié. Si l'affilié désire être réglé par virement bancaire, il suffit qu'il remplisse les champs prévus à cet effet dans sa zone membre. Le cas échéant, son règlement se fera par chèque."); ?> </p>
        <p class="faq">Les paiements par chèque seront envoyés à la personne et à l'adresse indiquées lors de votre inscription. </p>
        <p class="faq"><?php echo _t("Au regard de la fiscalité, les sommes perçues peuvent être déclarées en bénéfices non commerciaux (BNC)."); ?> </p>
      </blockquote>      
      <p class="faq"><strong><strong><strong><strong><a name="commissions" id="commissions"></a></strong></strong></strong><?php echo _t("Article"); ?> 8 </strong>//        <strong><?php echo _t("COMMISSIONS"); ?> <strong><strong><strong><a href="#top"><img src="images/top.jpg" width="11" height="9" border="0"></a></strong></strong></strong></strong></p>
      <blockquote>
        <p class="faq"><?php echo _t("Les commissions aux affiliés son payées à partir de 300 &euro;, si cette somme n'est pas atteinte en fin de mois, les commissions perçues sont renvoyées sur le mois suivant et ainsi de suite jusqu'à ce que le montant de 300 &euro; soit atteint."); ?> </p>
      </blockquote>      
      <p class="faq"><strong><strong><strong><strong><a name="parrainage" id="parrainage"></a></strong></strong></strong><?php echo _t("Article"); ?> 9 </strong>//        <strong><?php echo _t("PARRAINAGES"); ?> <strong><strong><strong><a href="#top"><img src="images/top.jpg" width="11" height="9" border="0"></a></strong></strong></strong></strong></p>
      <blockquote>
        <p class="faq"><?php echo _t("L'affilié peut parrainer tous les webmasters de son choix, à condition que ceux-ci respectent les présentes conditions générales d'affiliation. Dans le cas où le webmaster parrainé déroge aux présentes conditions, l'affilié pourra voir sa responsabilité engagée et son compte pourra être supprimé."); ?> </p>
        <p class="faq"><?php echo _t("Par ailleurs, le parrainage n'est possible qu'au premier niveau de profondeur, signifiant ainsi que seuls les webmasters directement parrainés par l'affilié Lyad seront autorisés."); ?> <br>
          <br>
          <?php echo _t("Il est strictement interdit de se parrainer soit même, toute entorse à cette règle se verrait sanctionnée par une annulation immédiate du compte de l'affilié tél que prévu à l'artcle 11 dans le cadre d'un manquement à vos engagements."); ?></p>
      </blockquote>      
      <p class="faq"><strong><strong><strong><strong><a name="fraudes" id="fraudes"></a></strong></strong></strong><?php echo _t("Article 10"); ?> </strong>//        <strong><?php echo _t("FRAUDES ET CHARGE BACK"); ?> <strong><strong><strong><a href="#top"><img src="images/top.jpg" width="11" height="9" border="0"></a></strong></strong></strong></strong></p>
      <blockquote>
        <p class="faq"><?php echo _t("Toute fraude à l'encontre de Lyad.com et de Lyad Affiliation qui pourra être commise par un affilié, exposera cet affilié à des poursuites judiciaires."); ?> </p>
        <p class="faq"><?php echo _t("Le charge back désigne la conséquence d'une plainte d'un client auprès de sa banque, qui s'oppose et refuse de régler les sommes relatives à Lyad, qui lui ont été notifiées sur son relevé. Cette plainte déclenche un ponctionnement des comptes de Lyad."); ?> </p>
        <p class="faq"><?php echo _t("Le charge back fait partie des frais couramment retenus à l'affilié, puisqu'il entre dans les frais inhérents au paiement électronique."); ?> </p>
        <p class="faq"><?php echo _t("Le charge back est directement déduit du chiffre d'affaires généré par votre compte."); ?> </p>
        <p class="faq"><?php echo _t("Si par ailleurs, nous détectons un taux de charge back supérieur à 6% sur les transactions effectuées sur la clientèle envoyée, nous nous donnons le droit de supprimer votre compte sans que vos commissions ne soient réglées. Un taux de charge back supérieur à 6% traduirait une situation anormale et porterait à croire à des manipulations frauduleuses."); ?> </p>
      </blockquote>      
      <p class="faq"><strong><strong><strong><strong><a name="duree" id="duree"></a></strong></strong></strong><?php echo _t("Article"); ?> 11 </strong>//        <strong><?php echo _t("DUR&Eacute;E DU CONTRAT ET R&Eacute;SILIATION"); ?> <strong><strong><strong><a href="#top"><img src="images/top.jpg" width="11" height="9" border="0"></a></strong></strong></strong></strong></p>
      <blockquote>
        <p class="faq"><?php echo _t("Le partenariat entre Lyad Affiliation et le site Affilié prendra effet dès la signification à l'affilié de son agrément."); ?> <br>
          <?php echo _t("Le présent contrat d'affiliation est conclu de façon non exclusive entre Lyad Affiliation et ses affiliés, pour une durée indéterminée. Chacune des parties pourra y mettre fin à tous moments par l'envoi d'un email le signifiant."); ?> <br>
          <br>
          <?php echo _t("En cas de résiliation par l'une des parties, l'affilié percevra un solde de tout compte calculé sur la base du montant qui lui est d&ucirc;, et ce dans un délai de deux mois."); ?> 
          <?php echo _t("Au cas où le contrat serait annulé Lyad Affiliation pour manquement à vos engagements, les montants accumulés et non payés seront annulés."); ?> </p>
      </blockquote>      
      <p class="faq"><strong><strong><strong><strong><a name="desinscription" id="desinscription"></a></strong></strong></strong><?php echo _t("Article"); ?> 12 </strong>//        <strong><?php echo _t("DESINSCRIPTION"); ?> <strong><strong><strong><a href="#top"><img src="images/top.jpg" width="11" height="9" border="0"></a></strong></strong></strong></strong></p>
      <blockquote>
        <p class="faq"><?php echo _t("Chaque affilié a la possibilité de se désinscrire quand il le souhaite. Pour se désinscrire, il doit envoyer un e-mail à unsuscribe@lyad.com, en précisant son login, password, ainsi que le motif de sa désinscription."); ?> </p>
        <p class="faq"><?php echo _t("La désinscription implique la perte de tous les gains disponibles à la fin du mois à la date de désinscription de l'affilié en question."); ?></p>
      </blockquote>      
      <p class="faq"><strong><strong><strong><strong><a name="propriete" id="propriete"></a></strong></strong></strong><?php echo _t("Article"); ?> 13 </strong>//        <strong><?php echo _t("PROPRIETE INTELLECTUELLE"); ?> <strong><strong><strong><a href="#top"><img src="images/top.jpg" width="11" height="9" border="0"></a></strong></strong></strong></strong></p>
      <blockquote>
        <p class="faq"><?php echo _t("L'affilié n'est pas autorisé à afficher ou créer d'autres bandeaux, utilisant le nom de Lyad.com, que ceux qui lui sont proposés par Lyad, sans autorisation préalable de Lyad."); ?> </p>
        <p class="faq"><?php echo _t("L'appellation Lyad.com et Lyad sont la propriété intellectuelle de Lyad et ne peuvent être reproduits. Les graphismes, logos, photographies, et tout autre contenu de Lyad et son site sont protégés par des marques et des droits d'auteur et toute utilisation par autrui non autorisée constituerait un acte de contrefaçon punissable légalement."); ?> </p>
      </blockquote>      
      <p class="faq"><strong><strong><strong><strong><a name="modifications" id="modifications"></a></strong></strong></strong><?php echo _t("Article"); ?> 14 </strong>//        <strong><?php echo _t("MODIFICATION DU PRESENT CONTRAT"); ?> <strong><strong><strong><a href="#top"><img src="images/top.jpg" width="11" height="9" border="0"></a></strong></strong></strong></strong></p>
      <blockquote>
        <p class="faq"><?php echo _t("L'affilié doit consulter régulièrement les conditions générales d'affiliation, celle-ci pouvant être modifié à tout moment. La prise en vigeur étant immédiate pour tous les affiliés du réseau. à charge de chacun de les consulter régulièrement."); ?><br>
<?php echo _t("Si un affilié refuse les termes des présentes conditions il doit nous tenir informé par mail à affiliation@lyad.com de son souhait de désinscription, la désinscription entraine la perte des gains à partir de la fin du mois de la date de résiliation, date à laquel la plateforme d'affiliation procédera au versement du solde de tout compte pour cet affilié."); ?></p>
      </blockquote>      
      <p class="faq"><strong><strong><strong><strong><a name="litiges" id="litiges"></a></strong></strong></strong><?php echo _t("Article"); ?> 15 </strong>//        <strong><?php echo _t("LITIGES"); ?> <strong><strong><strong><a href="#top"><img src="images/top.jpg" width="11" height="9" border="0"></a></strong></strong></strong></strong></p>
      <blockquote>
        <p class="faq"><?php echo _t("Tous les litiges relatifs aux présentes seront de la compétence exclusive des tribunaux de Paris (France)."); ?> <br>
          <?php echo _t("Cependant nous nous engageons à tenter de régler à l'amiable tout litige qui pourrait survenir."); ?> </p>
      </blockquote>      
      <p class="faq"><strong><strong><strong><strong><a name="contact" id="contact"></a></strong></strong></strong><?php echo _t("Article"); ?> 16 </strong>//        <strong><?php echo _t("NOUS CONTACTER"); ?> <strong><strong><strong><a href="#top"><img src="images/top.jpg" width="11" height="9" border="0"></a></strong></strong></strong></strong></p>
      <blockquote>
        <p class="faq"><?php echo _t("Adresse&nbsp;: Internet // Lyad Affiliation 4 Rue Galvani 75017 Paris France"); ?> <br>
          <?php echo _t("E-Mail&nbsp;: affiliation@lyad.com"); ?><br>
          <?php echo _t("Fax&nbsp;: (+33) 04.76.25.02.19"); ?></p>
    </blockquote>      <p>&nbsp; </p></td>
</tr>
<tr><td height="23" align="center" valign="top"><table width="460" height="23"  border="0" cellpadding="0" cellspacing="0" background="images/nav_02.jpg">
    <tr>
    <td width="26">&nbsp;</td>
    <td width="105"><a href="presentations.php" target="ecran"><img src="images/tr.gif" width="100%" height="16" border="0"></a></td>
    <td width="111"><a href="reversements.php" target="ecran"><img src="images/tr.gif" width="100%" height="16" border="0"></a></td>
    <td width="98"><a href="inscriptions.php" target="ecran"><img src="images/tr.gif" width="100%" height="16" border="0"></a></td>
    <td width="120"><a href="demo.php" target="ecran"><img src="images/tr.gif" width="48%" height="16" border="0"></a></td>
    </tr>
    </table>


</td>
  </tr>
</table>

<div align="center"></div>
</body>
</html>


